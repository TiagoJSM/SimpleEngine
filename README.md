# Simple Engine

This repository is intended to produce code to develop my C++ skills by creating a game engine / framework step by step to create a game prototype, it is in no way ready to create a production product.

## Third party libraries used:

* OpenGL - Graphics rendering.
* Box3D - 2D physics
* Boost - Utility libraries
* SDL - Event and operating system handling

## Game Engine Architecture

The architecture is heavily inspired in Unity3D API.
Every object in the game scene is called a GameObject and the behaviour is coded in Components, that are attached to the GameObject.