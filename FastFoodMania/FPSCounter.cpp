#include "FPSCounter.h"

namespace FFM {
	void FPSCounter::Update(Timer timer) {
		_deltaTime += (timer.deltaTime - _deltaTime) * 0.1f;

		float msec = _deltaTime * 1000.0f;
		float fps = 1.0f / _deltaTime;

		Logger::LogStream() << fps << std::endl;
	}
}