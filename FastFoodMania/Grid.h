#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "boost/multi_array.hpp"

#include "Restaurant.h"

using namespace SimpleEngine;

namespace FFM {
	class Grid : public Component {
	public:
		typedef boost::multi_array<GameObject, 2> GridArrayType;

		void SetGridSize(int width, int height, int cellSize);
		int GetWidth();
		int GetHeight();
		int GetCellSize();
		void Update(Timer timer) override;
		void AddToGridAt(GameObject go, int x, int y);
		GameObject GetGridObject(int x, int y);
	private:
		int _width;
		int _height;
		int _cellSize;
		GridArrayType _gridItems;
		ComponentHandle<FFM::Restaurant> _lastSelectedRestaurant;
	};
}