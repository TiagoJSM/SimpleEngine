 
in vec4 ex_Color;
in vec2 texCoord;

uniform Texture tex;
uniform int highlight;
 
 const vec4 GREEN = vec4( 0.0, 1.0, 0.0, 1.0 );

void main(void) {
	gl_FragColor = texture(tex, texCoord);
	if(highlight > 0) {
		gl_FragColor = mix(gl_FragColor, GREEN, 0.1);
	}
}