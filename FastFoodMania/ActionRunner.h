#pragma once

#include "SimpleEngine\SimpleEngine.h"

using namespace SimpleEngine;

namespace FFM {
	class IAction;

	class ActionRunner : public Component {
	public:
		void RunAction(IAction& action);
	};
}