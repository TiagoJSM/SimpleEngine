 
in vec2 texCoord;

uniform Texture tex;
uniform float colorMultiplier;
uniform vec4 _color;
 
void main(void) {
	gl_FragColor = texture(tex, texCoord) * (_color * colorMultiplier);
}