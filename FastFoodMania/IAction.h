#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "SimpleEngine\Box3.h"

using namespace SimpleEngine;

namespace FFM {
	class IAction {
	public:
		virtual bool IsValid() = 0;
		virtual void Execute() = 0;
	};
}