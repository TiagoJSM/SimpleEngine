#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "RestaurantOwner.h"
#include "UiEvents.h"
#include "AppEvents.h"
#include "UiStore.h"

using namespace SimpleEngine;

namespace FFM {
	class PlayerOwner : public Component {
	public:
		PlayerOwner();
		void Initialize() override;
		void Update(Timer timer) override;
	private:
		UiStore _uiStore;
		ComponentHandle<RestaurantOwner> _owner;
		Event<BuyRestaurantButtonEvent>::EventConnection<BuyRestaurantButtonEvent> _buyRestaurantButtonEvt;

		void BuyRestaurantButtonEventHandler(const BuyRestaurantButtonEvent& data);
	};
}