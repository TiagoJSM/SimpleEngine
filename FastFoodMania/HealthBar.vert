 
// We output the ex_Color variable to the next shader in the chain
out vec4 ex_Color;

void main(void) {
    // Since we are using flat lines, our input only had two points: x and y.
    // Set the Z coordinate to 0 and W coordinate to 1
    gl_Position = MVP * vec4(position.x, position.y, position.z, 1.0);
 
    // Pass the color on to the fragment shader
    ex_Color = color;
}