#include "Restaurant.h"

namespace FFM {
	GameObject MakeHouse(GameWorld& world) {
		string materialName = "houseMaterial";
		auto material = GraphicResourcesManager::Instance()->GetMaterial(materialName);

		string meshName = "Meshes/cube.obj";
		auto mesh = GraphicResourcesManager::Instance()->GetMesh(meshName);

		string vertex = "Standard.vert", fragment = "Standard.frag";
		auto stdShader = GraphicResourcesManager::Instance()->GetShader(vertex, fragment);

		string textureName = "./Textures/tank.bmp";
		auto houseTex = GraphicResourcesManager::Instance()->LoadTextureResource(textureName);
		material->SetShader(stdShader);
		map<std::string, TextureResource> textures = { { "tex", houseTex } };
		material->SetTextures(textures);

		auto go = world.CreateObject();
		go->transform.translation = glm::vec3(0.f, 0.f, 0.f);
		go->transform.scale = glm::vec3(0.5f, 0.5f, 0.5f);
		auto restaurantMeshComp = go->AddComponent<MeshComponent>();
		restaurantMeshComp->mesh = mesh;
		restaurantMeshComp->material = material;

		return go;
	}
}