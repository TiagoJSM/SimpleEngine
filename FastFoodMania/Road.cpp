#include "Road.h"

namespace FFM {
	GameObject MakeRoad(GameWorld& world) {
		float _tankWidth = 1.f;
		float _tankHeight = 1.f;

		string materialName = "roadMaterial";
		auto material = GraphicResourcesManager::Instance()->GetMaterial(materialName);

		string meshName = "Meshes/road.obj";
		auto mesh = GraphicResourcesManager::Instance()->GetMesh(meshName);

		string vertex = "Standard.vert", fragment = "Standard.frag";
		auto stdShader = GraphicResourcesManager::Instance()->GetShader(vertex, fragment);

		string texture = "./Textures/road.bmp";
		auto roadTex = GraphicResourcesManager::Instance()->LoadTextureResource(texture);
		material->SetShader(stdShader);
		map<std::string, TextureResource> textures = { { "tex", roadTex } };
		material->SetTextures(textures);

		auto go = world.CreateObject();
		go->transform.translation = glm::vec3(0.f, 0.f, 0.f);
		auto roadMeshComp = go->AddComponent<MeshComponent>();
		roadMeshComp->mesh = mesh;
		roadMeshComp->material = material;

		return go;
	}
}