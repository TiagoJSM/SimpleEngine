#pragma once

#include <vector>
#include <functional>
#include <stdlib.h>
#include <algorithm>
#include <boost/optional.hpp>
#include "Street.h"
#include "SimpleEngine\MathUtils.h"
#include "SimpleEngine\Logger.h"
#include "SimpleEngine\GlmMathExtensions.h"

using namespace SimpleEngine;

namespace FFM {
	struct CityInfo {
	public:
		glm::uvec2 cityCenter;
		std::vector<glm::uvec2> interestPoints;
		std::vector<Street> streets;
		unsigned int width;
		unsigned int height;
	};
	class CityGenerator {
	public:
		static CityInfo Generate(
			unsigned int interestPointsCount,
			unsigned int width,
			unsigned int height);
	private:
		static const float CityCenterMarginRatio;

		static CityInfo BuildCityInfo(
			unsigned int width, 
			unsigned int height, 
			unsigned int interestPointsCount);

		static glm::uvec2 GetCityCenter(CityInfo& cityInfo);
		static glm::uvec2 GetMostPopulatedZone(CityInfo& cityInfo);
		static std::vector<glm::uvec2> GetStreetPointsBetween(
			CityInfo& cityInfo, glm::uvec2 p1, glm::uvec2 p2);
		static bool ContainsPoint(CityInfo& cityInfo, glm::uvec2 point);
		static std::vector<Street> MakeStreetsFromPoints(std::vector<glm::uvec2>& points);
		static boost::optional<glm::uvec2> RightSideConnection(glm::uvec2 origin, const std::vector<glm::uvec2>& points);
		static boost::optional<glm::uvec2> TopSideConnection(glm::uvec2 origin, const std::vector<glm::uvec2>& points);
		static boost::optional<glm::uvec2> SearchAdjacentClosestPoint(
			glm::uvec2 origin, 
			const std::vector<glm::uvec2>& points,
			std::function<bool(glm::uvec2 origin, glm::uvec2 testPoint)> predicate,
			std::function<unsigned int(glm::uvec2 p1, glm::uvec2 p2)> distanceCalculator);
		static void GetCrossingPointsBetweenStreets(
			std::vector<glm::uvec2>& streetPoints,
			std::vector<Street>& streets);
		static void GetCrossingPointsBetweenStreets(
			std::vector<glm::uvec2>& streetPoints,
			Street& testStreet,
			std::vector<Street>& streets);
		static void MoveCloserToOrigin(CityInfo& cityInfo);
		static bool IsPointTooClose(glm::uvec2 testPoint, std::vector<glm::uvec2>& points, float maxDistance);
	};
}