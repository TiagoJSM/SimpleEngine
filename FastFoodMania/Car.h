#pragma once

#include <boost\optional.hpp>
#include <boost\signals2.hpp>
#include "SimpleEngine\SimpleEngine.h"
#include "CityGenerator.h"

using namespace SimpleEngine;

namespace FFM {
	class Car;
	//typedef boost::signals2::signal<void(Car*)> OnArrivedToTarget;
	using OnArrivedToTarget = Event<Car*>;

	class Car : public Component {
	public:
		CityInfo* cityInfo;
		OnArrivedToTarget onArrivedToTarget;

		void Update(Timer timer) override;
		void Move(glm::ivec2 target);
	private:
		boost::optional<glm::ivec2> _target;
	};
}