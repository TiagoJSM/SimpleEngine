//#include "../SimpleEngine/Renderer.h"
#include "../box2d/box2d.h"

#include "../simpleengine/simpleengine.h"

#include "../simpleengine/main.cpp"

//#include "testscene.h"
#include "cityscene.h"
//#include "fullrestaurantsscene.h"

int main(int argc, char *argv[])
{
	//SimpleEngine::TemplateComponentFactory<FFM::Grid> f;
	//vec.push_back(make_unique<B>());
	//FFM::FullRestaurantsScene scene;
	FFM::CityScene scene;
	//FFM::TestScene scene;
	return Run::run(argc, argv, scene);
	return 0;
}
