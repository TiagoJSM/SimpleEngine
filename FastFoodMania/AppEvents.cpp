#include "AppEvents.h"

namespace FFM {
	SimpleEngine::Event<FFM::BuyRestaurantButtonEvent> buyRestaurantButton;

	SimpleEngine::Event<FFM::BuyRestaurantButtonEvent>& GetBuyRestaurantButton() {
		return buyRestaurantButton;
	}
}