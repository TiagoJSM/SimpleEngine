#pragma once

#include "SimpleEngine\SimpleEngine.h"

#include "City.h"
#include "Grid.h"
#include "Restaurant.h"
#include "Road.h"
#include "OverCityCamera.h"
#include "CityGenerator.h"
#include "SimpleEngine\DefaultGraphicObjects.h"
#include "FPSCounter.h"

using namespace SimpleEngine;
using namespace SimpleEngine::UI;

namespace FFM {
	class FullRestaurantsScene : public IScene {
	public:
		void BuildScene(GameWorld& world) override {
			auto screenSize = Screen::Size();
			_cam.fieldOfView = 90.0f;
			_cam.aspectRatio = (float)screenSize.x / (float)screenSize.y;
			_cam.nearClipping = 0.001f;
			_cam.farClipping = 100.0f;
			_cam.translation = glm::vec3(0.f, 0.f, 0);

			Camera::currentCamera = &_cam;

			auto gridGO = world.CreateObject();
			gridGO->transform.translation = glm::vec3(0, 0, 5);
			auto grid = gridGO->AddComponent<Grid>();
			grid->SetGridSize(10, 10, 1);

			AddBuildings(world, grid);

			auto cameraGO = world.CreateObject();
			cameraGO->AddComponent<OverCityCamera>();
		}

		void AddBuildings(GameWorld& world, Grid* grid) {
			for (auto x = 0; x < grid->GetWidth(); x++) {
				for (auto y = 0; y < grid->GetHeight(); y++) {
					auto restaurant = MakeRestaurant(world);
					grid->AddToGridAt(restaurant->gameObject, x, y);
				}
			}
		}
	private:
		Camera _cam;
	};
}