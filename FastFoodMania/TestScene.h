#pragma once

#include "SimpleEngine/IScene.h"
#include "SimpleEngine/SimpleEngine.h"

using namespace SimpleEngine;

namespace FFM {
	class TestScene : public IScene {
	public:
		void BuildScene(GameWorld& world) override;
	};
}