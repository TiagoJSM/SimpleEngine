#include "ActionRunner.h"
#include "IAction.h"

namespace FFM {
	void ActionRunner::RunAction(IAction& action) {
		if (action.IsValid()) {
			action.Execute();
		}
	}
}