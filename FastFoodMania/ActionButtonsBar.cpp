#include "ActionButtonsBar.h"
#include "BuyRestaurantButton.h"

namespace FFM {
	GameObject MakeActionButtonsBar(GameWorld& world) {
		auto actionButtonsGO = world.CreateObject();
		auto actionButtonsUiTransform = actionButtonsGO->AddComponent<UiTransform>();
		actionButtonsUiTransform->anchors = { glm::vec2(0.0f, 0.0f), glm::vec2(0.75f, 0.2f) };
		auto stack = actionButtonsGO->AddComponent<StackPanel>();
		stack->direction = Direction::Horizontal;

		auto button1 = world.CreateObject();
		button1->AddComponent<Image>();
		button1->AddComponent<Button>();
		auto button1UiTransform = button1->AddComponent<UiTransform>();
		button1->AddComponent<BuyRestaurantButton>();
		button1UiTransform->height = 100;
		button1UiTransform->width = 100;

		auto button2 = world.CreateObject();
		button2->AddComponent<Image>();
		button2->AddComponent<Button>();
		auto button2UiTransform = button2->AddComponent<UiTransform>();
		//button2->AddComponent<BuyRestaurantButton>();
		button2UiTransform->height = 100;
		button2UiTransform->width = 100;

		actionButtonsGO->AddChildren(button1);
		actionButtonsGO->AddChildren(button2);

		return actionButtonsGO;
	}
}