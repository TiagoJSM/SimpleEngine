#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "SimpleEngine\Box3.h"

using namespace SimpleEngine;

namespace FFM {
	class RestaurantOwner;
	class Restaurant : public Component {
	public:
		unsigned int price;
		unsigned int width;
		unsigned int height;
		ComponentHandle<RestaurantOwner> owner;

		void Initialize() override;
		Box3 GetBounds();
		void Render(IRenderer& renderer);
		void Highlight(bool highlight);
	private:
		ComponentHandle<MeshComponent> _meshComp;
	};

	ComponentHandle<Restaurant> MakeRestaurant(GameWorld& world);
}