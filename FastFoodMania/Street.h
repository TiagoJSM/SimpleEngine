#pragma once

#include <algorithm>
#include <glm\glm.hpp>
#include <boost/optional.hpp>

namespace FFM {
	enum class StreetOrientation {
		Horizontal,
		Vertical,
	};

	struct Street {
	public:
		glm::ivec2 p1, p2;

		StreetOrientation GetOrientation() const {
			if (p1.x == p2.x) {
				return StreetOrientation::Vertical;
			}
			return StreetOrientation::Horizontal;
		}

		bool Overlaps(const Street& street) const {
			if (street.GetOrientation() != GetOrientation()) {
				return false;
			}

			if (this->operator==(street)) {
				return true;
			}

			return
				street.IsPointContained(p1) || street.IsPointContained(p2) ||
				IsPointContained(street.p1) || IsPointContained(street.p2);
		}

		boost::optional<glm::uvec2> IntersectionPoint(const Street& street) {
			if (street.GetOrientation() == GetOrientation()) {
				return boost::optional<glm::uvec2>();
			}

			auto horizontalStreet =
				street.GetOrientation() == StreetOrientation::Horizontal
				? street
				: *this;

			auto verticalStreet =
				street.GetOrientation() == StreetOrientation::Vertical
				? street
				: *this;

			auto hxMin = std::min(horizontalStreet.p1.x, horizontalStreet.p2.x);
			auto hxMax = std::max(horizontalStreet.p1.x, horizontalStreet.p2.x);

			auto vyMin = std::min(verticalStreet.p1.y, verticalStreet.p2.y);
			auto vyMax = std::max(verticalStreet.p1.y, verticalStreet.p2.y);

			if (verticalStreet.p1.x > hxMin && verticalStreet.p1.x < hxMax &&
				horizontalStreet.p1.y > vyMin && horizontalStreet.p1.y < vyMax) {

				return glm::uvec2(verticalStreet.p1.x, horizontalStreet.p1.y);
			}
			return boost::optional<glm::uvec2>();
		}

		bool IsPointContained(glm::ivec2 point, bool considerEndings = false) const {
			if (considerEndings) {
				if (p1 == point || p2 == point) {
					return true;
				}
			}

			auto orientation = GetOrientation();

			// on same axis between points
			if (orientation == StreetOrientation::Horizontal) {
				auto min = std::min(p1.x, p2.x);
				auto max = std::max(p1.x, p2.x);
				return point.y == p1.y && min < point.x && point.x < max;
			}
			// on same axis between points
			auto min = std::min(p1.y, p2.y);
			auto max = std::max(p1.y, p2.y);
			return point.x == p1.x && min < point.y && point.y < max;
		}

		unsigned int Distance() {
			auto orientation = GetOrientation();

			// on same axis between points
			if (orientation == StreetOrientation::Horizontal) {
				return abs(p2.x - p1.x);
			}
			else {
				return abs(p2.y - p1.y);
			}
		}

		bool operator==(const Street& street) const {
			return
				street.p1 == p1 && street.p2 == p2 ||
				street.p1 == p2 && street.p2 == p1;
		}

		bool operator==(const Street* street) const {
			return
				street->p1 == p1 && street->p2 == p2 ||
				street->p1 == p2 && street->p2 == p1;
		}
	};
}