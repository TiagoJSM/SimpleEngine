#include "Restaurant.h"
#include "RestaurantOwner.h"

namespace FFM {
	void Restaurant::Initialize() {
		_meshComp = gameObject->GetComponent<MeshComponent>();
		_meshComp->renderData.integers.push_back({ "highlight", 0 });
	}

	Box3 Restaurant::GetBounds() {
		auto position = gameObject->GetPosition();
		return Box3(
			glm::vec3(-0.5f + position.x, -0.5f + position.y, position.z - 0.05f), 
			glm::vec3(0.5f + position.x, 0.5f + position.y, position.z + 0.05f));
	}

	void Restaurant::Render(IRenderer& renderer) {
		//Debug::DrawBox3(GetBounds());
	}

	void Restaurant::Highlight(bool highlight) {
		_meshComp->renderData.integers.clear();
		_meshComp->renderData.integers.push_back({ "highlight", highlight ? 1 : 0 });
	}

	ComponentHandle<Restaurant> MakeRestaurant(GameWorld& world) {
		float _tankWidth = 1.f;
		float _tankHeight = 1.f;

		string restaurantMaterialName = "restaurantMaterial";
		auto restaurantMaterial = GraphicResourcesManager::Instance()->GetMaterial(restaurantMaterialName);
		//TANK
		string restaurant = "Meshes/cube.obj";
		auto restaurantMesh = GraphicResourcesManager::Instance()->GetMesh(restaurant);

		string vertex = "SelectableBuilding.vert", fragment = "SelectableBuilding.frag";
		auto stdShader = GraphicResourcesManager::Instance()->GetShader(vertex, fragment);

		string tankName = "./Textures/tank.bmp";
		auto tankTex = GraphicResourcesManager::Instance()->LoadTextureResource(tankName);
		restaurantMaterial->SetShader(stdShader);
		map<std::string, TextureResource> textures = { { "tex", tankTex } };
		restaurantMaterial->SetTextures(textures);

		auto go = world.CreateObject();
		go->transform.translation = glm::vec3(0.f, 0.f, 0.f);
		go->transform.scale = glm::vec3(0.5f, 0.5f, 0.5f);
		auto restaurantMeshComp = go->AddComponent<MeshComponent>();
		restaurantMeshComp->mesh = restaurantMesh;
		restaurantMeshComp->material = restaurantMaterial;

		return go->AddComponent<Restaurant>();
	}
}