#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "CityGenerator.h"

using namespace SimpleEngine;

namespace FFM {
	class CityMap : public Component, public UI::IMouseClick {
	public:
		CityInfo* cityInfo;
		ComponentHandle<UI::Image> img;

		void Initialize() override;
		void Click() override;
	};
	ComponentHandle<CityMap> MakeCityMap(GameWorld& world, CityInfo* cityInfo);
}