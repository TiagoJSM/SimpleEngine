#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "SimpleEngine\Box3.h"

#include "RestaurantOwner.h"
#include "Restaurant.h"
#include "GameDataStore.h"
#include "IAction.h"

using namespace SimpleEngine;

namespace FFM {
	class BuyRestaurantAction : public IAction {
	public:
		BuyRestaurantAction(GameDataStore& store, ComponentHandle<Restaurant> restaurant);
		bool IsValid() override;
		void Execute() override;
	private:
		GameDataStore& _store;
		ComponentHandle<Restaurant> _restaurant;
	};
}