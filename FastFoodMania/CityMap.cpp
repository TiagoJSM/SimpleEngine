#include "CityMap.h"

namespace FFM {
	void CityMap::Initialize() {
		auto width = 256, height = 256;
		auto tex = GraphicResourcesManager::Instance()->GetTextureResource(width, height);
		const auto len = width * height;
		std::vector<Color> colors(len);

		for (auto idx = 0; idx < len; idx++) {
			colors[idx] = Color(0.2f, 1.f, 0.2f, 1.f);
		}

		for (auto street : cityInfo->streets) {
			auto orientation = street.GetOrientation();
			if (orientation == StreetOrientation::Horizontal) {
				auto min = std::min(street.p2.x, (int)street.p1.x);
				auto max = std::max(street.p2.x, (int)street.p1.x);
				auto texY = height - 1 - (street.p1.y * height / cityInfo->height);
				min = min * width / cityInfo->width;
				max = max * width / cityInfo->width;

				for (; min <= max; min++) {
					colors[width * texY + min] = Color(0.2f, 0.2f, 0.2f, 1.f);
				}
			}
			else {
				auto min = std::min(street.p2.y, (int)street.p1.y);
				auto max = std::max(street.p2.y, (int)street.p1.y);
				auto texX = street.p1.x * width / cityInfo->width;
				min = min * height / cityInfo->height;
				max = max * height / cityInfo->height;

				for (; min <= max; min++) {
					colors[width * (height - min - 1) + texX] = Color(0.2f, 0.2f, 0.2f, 1.f);
				}
			}
		}

		tex->SetPixels(colors);
		img->SetTexture(tex);
	}

	void CityMap::Click() {
		auto uiTransform = img->GetUiTransform();
		auto rect = uiTransform->GetRect();
		auto coordRatio = (glm::vec2(Input::mousePointer) - rect.p1) / glm::vec2(rect.width, rect.height);
		auto x = cityInfo->width * coordRatio.x;
		auto y = cityInfo->height * coordRatio.y;

		auto cam = Camera::currentCamera;
		cam->translation = glm::vec3(x, y, cam->translation.z);
	}

	ComponentHandle<CityMap> MakeCityMap(GameWorld& world, CityInfo* cityInfo) {
		auto cityMapGO = world.CreateObject();
		auto cityMapUiTransform = cityMapGO->AddComponent<UiTransform>();
		cityMapUiTransform->anchors = { glm::vec2(0.75f, 0.0f), glm::vec2(1.0f, 0.25f) };
		auto image = cityMapGO->AddComponent<Image>();
		return cityMapGO->AddComponent<CityMap>([&](CityMap* cityMap) {
			cityMap->cityInfo = cityInfo;
			cityMap->img = image;
		});
	}
}