#include "TestScene.h"

using namespace SimpleEngine;

Camera cam;

float _tankWidth = 1.f;
float _tankHeight = 1.f;

float _markerWidth = .2f;
float _markerHeight = 1.f;

namespace FFM {
	void TestScene::BuildScene(GameWorld& world) {
		/*cam.view = View::Ortho;
		cam.bottom = 0;
		cam.top = 10;
		cam.left = 0;
		cam.right = 10;

		Camera::currentCamera = &cam;

		string skyMaterialName = "skyMaterial";
		auto skyMaterial = GraphicResourcesManager::Instance()->GetMaterial(skyMaterialName);
		string backgroundMaterialName = "backgroundMaterial";
		auto backgroundMaterial = GraphicResourcesManager::Instance()->GetMaterial(backgroundMaterialName);
		string tankMaterialName = "tankMaterial";
		auto tankMaterial = GraphicResourcesManager::Instance()->GetMaterial(tankMaterialName);
		string bulletMaterialName = "bulletMaterial";
		auto bulletMaterial = GraphicResourcesManager::Instance()->GetMaterial(bulletMaterialName);
		string markerMaterialName = "markerMaterial";
		auto markerMaterial = GraphicResourcesManager::Instance()->GetMaterial(markerMaterialName);
		string healthBarMatName = "healthBarMat";
		auto healthBarMat = GraphicResourcesManager::Instance()->GetMaterial(healthBarMatName);

		//TANK
		string tank = "tank";
		auto tankMesh = GraphicResourcesManager::Instance()->GetMesh(tank);
		tankMesh->SetVertices(std::vector<glm::vec3> {
			{ -_tankWidth / 2, _tankHeight / 2, .0f }, // Top left
			{ _tankWidth / 2,  _tankHeight / 2, .0f }, // Top right
			{ _tankWidth / 2, -_tankHeight / 2, .0f }, // Bottom right 
			{ -_tankWidth / 2, -_tankHeight / 2, .0f }, // Bottom left
		});
		tankMesh->SetColors(std::vector<Color> {
			Color(1.0f, 0.0f, 0.0f, 1.0f), // Top left
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Top right
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Bottom right 
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Bottom left
		});
		tankMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		tankMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		tankMesh->ApplyDataToMesh();

		string terrainName = "./Textures/field.png";
		auto terrainTex = GraphicResourcesManager::Instance()->LoadTextureResource(terrainName);
		string tankName = "./Textures/tank.bmp";
		auto tankTex = GraphicResourcesManager::Instance()->LoadTextureResource(tankName);
		string bulletName = "./Textures/bullet.bmp";
		auto bulletTex = GraphicResourcesManager::Instance()->LoadTextureResource(bulletName);
		string skyName = "./Textures/Sky.bmp";
		auto skyTex = GraphicResourcesManager::Instance()->LoadTextureResource(skyName);

		//SHOOT MARKER
		string marker = "marker";
		auto markerMesh = GraphicResourcesManager::Instance()->GetMesh(marker);
		markerMesh->SetVertices(std::vector<glm::vec3> {
			{ -_markerWidth / 2, _markerHeight, .0f }, // Top left
			{ _markerWidth / 2,  _markerHeight, .0f }, // Top right
			{ _markerWidth / 2, 0, .0f }, // Bottom right 
			{ -_markerWidth / 2, 0, .0f }, // Bottom left
		});
		markerMesh->SetColors(std::vector<Color> {
			Color(1.0f, 0.0f, 0.0f, 1.0f), // Top left
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Top right
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Bottom right 
				Color(1.0f, 0.0f, 0.0f, 1.0f), // Bottom left
		});
		markerMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		markerMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		markerMesh->ApplyDataToMesh();

		//BACKGROUND
		string bg = "background";
		auto bgMesh = GraphicResourcesManager::Instance()->GetMesh(bg);
		bgMesh->SetVertices(std::vector<glm::vec3> {
			{ 0.f, 1.f, .0f }, // Top left
			{ 1.f, 1.f, .0f }, // Top right
			{ 1.f, 0.f, .0f }, // Bottom right 
			{ 0.f, 0.f, .0f }, // Bottom left
		});
		bgMesh->SetColors(std::vector<Color> {
			Color(1.0f, 1.0f, 1.0f, 1.0f), // Top left
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Top right
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom right 
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom left
		});
		bgMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		bgMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		bgMesh->ApplyDataToMesh();

		//SKY
		string sky = "sky";
		auto skyMesh = GraphicResourcesManager::Instance()->GetMesh(sky);
		skyMesh->SetVertices(std::vector<glm::vec3> {
			{ 0.f, 1.f, .0f }, // Top left
			{ 1.f, 1.f, .0f }, // Top right
			{ 1.f, 0.f, .0f }, // Bottom right 
			{ 0.f, 0.f, .0f }, // Bottom left
		});
		skyMesh->SetColors(std::vector<Color> {
			Color(1.0f, 1.0f, 1.0f, 1.0f), // Top left
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Top right
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom right 
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom left
		});
		skyMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		skyMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		skyMesh->ApplyDataToMesh();

		//BULLET
		string bullet = "bullet";
		auto bulletMesh = GraphicResourcesManager::Instance()->GetMesh(bullet);
		bulletMesh->SetVertices(std::vector<glm::vec3> {
			{ -0.5f, 0.5f, .0f }, // Top left
			{ 0.5f, 0.5f, .0f }, // Top right
			{ 0.5f, -0.5f, .0f }, // Bottom right 
			{ -0.5f, -0.5f, .0f }, // Bottom left
		});
		bulletMesh->SetColors(std::vector<Color> {
			Color(1.0f, 1.0f, 1.0f, 1.0f), // Top left
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Top right
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom right 
				Color(1.0f, 1.0f, 1.0f, 1.0f), // Bottom left
		});
		bulletMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		bulletMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		bulletMesh->ApplyDataToMesh();

		//HEALTH BAR
		string hb = "healthBar";
		auto hbMesh = GraphicResourcesManager::Instance()->GetMesh(hb);
		hbMesh->SetVertices(std::vector<glm::vec3> {
			{ 0.f, 0.25f, .0f }, // Top left
			{ 1.f, 0.25f, .0f }, // Top right
			{ 1.f, 0.f, .0f }, // Bottom right 
			{ 0.f, 0.f, .0f }, // Bottom left
		});
		hbMesh->SetColors(std::vector<Color> {
			Color(0.0f, 1.0f, 0.0f, 1.0f), // Top left
				Color(0.0f, 1.0f, 0.0f, 1.0f), // Top right
				Color(0.0f, 1.0f, 0.0f, 1.0f), // Bottom right 
				Color(0.0f, 1.0f, 0.0f, 1.0f), // Bottom left
		});
		hbMesh->SetUVs(std::vector<glm::vec2> {
			{ .0f, .0f }, // Top left
			{ 1.f, 0.f }, // Top right
			{ 1.f, 1.f }, // Bottom right 
			{ 0.f, 1.f }, // Bottom left
		});
		hbMesh->SetIndices(std::vector<unsigned int> {
			0, 1, 2, 0, 2, 3
		});
		hbMesh->ApplyDataToMesh();

		string markerTexName = "player1Tex";
		auto markerTex = GraphicResourcesManager::Instance()->GetTextureResource(markerTexName, 1, 1);
		markerTex->SetPixels(std::vector<Color> {
			Color(1.0f, 0.0f, 0.0f, 1.0f),
				Color(1.0f, 0.0f, 0.0f, 1.0f),
				Color(1.0f, 0.0f, 0.0f, 1.0f),
				Color(1.0f, 0.0f, 0.0f, 1.0f),
		});

		string vertex = "Standard.vert", fragment = "Standard.frag";
		auto stdShader = GraphicResourcesManager::Instance()->GetShader(vertex, fragment);
		string hbVertex = "HealthBar.vert", hbFragment = "HealthBar.frag";
		auto hbShader = GraphicResourcesManager::Instance()->GetShader(hbVertex, hbFragment);

		tankMaterial->shader = stdShader;
		tankMaterial->SetTexture("tex", tankTex);
		tankMesh->material = tankMaterial;

		skyMaterial->shader = stdShader;
		skyMaterial->SetTexture("tex", skyTex);
		skyMesh->material = skyMaterial;

		auto skyGO = world.CreateObject();
		skyGO->transform.scale = glm::vec3(10.f, 10.f, 1.f);
		auto skyMeshComp = skyGO->AddComponent<MeshComponent>();
		skyMeshComp->mesh = skyMesh;

		backgroundMaterial->shader = stdShader;
		backgroundMaterial->SetTexture("tex", terrainTex);
		bgMesh->material = backgroundMaterial;

		auto background = world.CreateObject();
		background->transform.scale = glm::vec3(10.f, 10.f, 1.f);
		auto backgroundMesh = background->AddComponent<MeshComponent>();
		backgroundMesh->mesh = bgMesh;

		markerMaterial->shader = stdShader;
		markerMaterial->SetTexture("tex", markerTex);
		markerMesh->material = markerMaterial;

		auto marker1GO = world.CreateObject();
		marker1GO->transform.translation = glm::vec3(2.f, 6.f, 0.f);
		marker1GO->transform.rotation = glm::vec3(0, 0, 0);
		auto marker1MeshComp = marker1GO->AddComponent<MeshComponent>();
		marker1MeshComp->mesh = markerMesh;

		auto marker2GO = world.CreateObject();
		marker2GO->transform.translation = glm::vec3(8.f, 6.f, 0.f);
		marker2GO->transform.rotation = glm::vec3(0, 0, 0);
		auto marker2MeshComp = marker2GO->AddComponent<MeshComponent>();
		marker2MeshComp->mesh = markerMesh;
		marker2GO->SetEnabled(false);

		auto tank1GO = world.CreateObject();
		tank1GO->transform.translation = glm::vec3(2.f, 6.f, 0.f);
		auto tank1MeshComp = tank1GO->AddComponent<MeshComponent>();
		tank1MeshComp->mesh = tankMesh;

		auto tank2GO = world.CreateObject();
		tank2GO->transform.translation = glm::vec3(8.f, 6.f, 0.f);
		tank2GO->transform.scale = glm::vec3(-1.f, 1.f, 1.f);
		auto tank2MeshComp = tank2GO->AddComponent<MeshComponent>();
		tank2MeshComp->mesh = tankMesh;

		auto bulletGO = world.CreateObject();
		bulletMaterial->shader = stdShader;
		bulletMaterial->SetTexture("tex", bulletTex);
		bulletMesh->material = bulletMaterial;
		bulletGO->transform.translation = glm::vec3(2.f, 2.f, 0.f);
		auto bulletMeshComp = bulletGO->AddComponent<MeshComponent>();
		bulletMeshComp->mesh = bulletMesh;
		auto body = bulletGO->AddComponent<Body2D>();
		body->SetGravityScale(150);
		bulletGO->AddComponent<BoxCollider2D>([=](BoxCollider2D* collider) { collider->body = body; });
		bulletGO->SetEnabled(false);

		auto tank1HealthGO = world.CreateObject();
		healthBarMat->shader = hbShader;
		healthBarMat->SetTexture("tex", bulletTex);
		hbMesh->material = healthBarMat;
		tank1HealthGO->transform.translation = glm::vec3(1.f, 9.f, 0.f);
		auto healthBar1Mesh = tank1HealthGO->AddComponent<MeshComponent>();
		healthBar1Mesh->mesh = hbMesh;*/
	}
}