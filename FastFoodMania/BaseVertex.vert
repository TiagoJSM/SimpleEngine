#version 150

// in_Position was bound to attribute index 0 and in_Color was bound to attribute index 1
attribute vec3 position;
attribute vec4 color;
attribute vec2 textureCoordinates;

uniform mat4 MVP;