#pragma once

#include "Restaurant.h"

namespace FFM {
	struct UiStore {
		ComponentHandle<Restaurant> selectedRestaurant;
	};
}