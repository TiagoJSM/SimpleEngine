#pragma once

#include "SimpleEngine\SimpleEngine.h"

using namespace SimpleEngine;

namespace FFM {
	GameObject MakeActionButtonsBar(GameWorld& world);
}