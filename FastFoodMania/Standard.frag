 
in vec4 ex_Color;
in vec2 texCoord;

uniform Texture tex;
 
void main(void) {
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
	gl_FragColor = texture(tex, texCoord);
}