#include "CityGenerator.h"

namespace FFM {
	const float CityGenerator::CityCenterMarginRatio = 0.2f;

	CityInfo CityGenerator::Generate(
			unsigned int interestPointsCount,
			unsigned int width,
			unsigned int height) {
		return BuildCityInfo(width, height, interestPointsCount);
	}

	CityInfo CityGenerator::BuildCityInfo(
			unsigned int width,
			unsigned int height,
			unsigned int interestPointsCount) {
		CityInfo cityInfo;
		cityInfo.width = width;
		cityInfo.height = height;

		cityInfo.cityCenter = GetCityCenter(cityInfo);

		for (unsigned int pointIdx = 0; pointIdx < interestPointsCount; pointIdx++) {
			cityInfo.interestPoints.push_back(GetMostPopulatedZone(cityInfo));
		}

		/*cityInfo.cityCenter = glm::uvec2(23, 7);
		cityInfo.interestPoints = std::vector<glm::uvec2> {
			glm::uvec2(4, 16),
			glm::uvec2(21, 1),
			glm::uvec2(0, 8),
			glm::uvec2(9, 0),
			glm::uvec2(20, 11),
		};*/
		
		Logger::LogStream() << cityInfo.cityCenter << std::endl;
		for (auto point : cityInfo.interestPoints) {
			Logger::LogStream() << point << std::endl;
		}
		
		MoveCloserToOrigin(cityInfo);

		std::vector<glm::uvec2> streetPoints;
		streetPoints.insert(streetPoints.end(), cityInfo.interestPoints.begin(), cityInfo.interestPoints.end());
		//streetPoints.push_back(cityInfo.cityCenter);

		for (auto point : cityInfo.interestPoints) {
			auto newStreetPoints = GetStreetPointsBetween(cityInfo, point, cityInfo.cityCenter);
			streetPoints.insert(streetPoints.end(), newStreetPoints.begin(), newStreetPoints.end());
		}

		streetPoints.push_back(cityInfo.cityCenter);
		auto streets = MakeStreetsFromPoints(streetPoints);
		//streets must be divided to make sure there are no overlaping streets
		GetCrossingPointsBetweenStreets(streetPoints, streets);
		/*cityInfo.streets = MakeRenderableStreetsFromPoints(streetPoints);*/
		cityInfo.streets = MakeStreetsFromPoints(streetPoints);

		return cityInfo;
	}

	glm::uvec2 CityGenerator::GetCityCenter(CityInfo& cityInfo) {
		unsigned int xMargin = (unsigned int)(CityGenerator::CityCenterMarginRatio * cityInfo.width);
		unsigned int yMargin = (unsigned int)(CityGenerator::CityCenterMarginRatio * cityInfo.height);

		auto x = MathUtils::Random(xMargin, cityInfo.width - xMargin);
		auto y = MathUtils::Random(yMargin, cityInfo.height - yMargin);

		return glm::uvec2(x, y);
	}

	glm::uvec2 CityGenerator::GetMostPopulatedZone(CityInfo& cityInfo) {
		while (true) {
			auto x = MathUtils::Random(0, cityInfo.width);
			auto y = MathUtils::Random(0, cityInfo.height);
			auto point = glm::uvec2(x, y);

			if (point == cityInfo.cityCenter) {
				continue;
			}
			if (std::find(cityInfo.interestPoints.begin(), cityInfo.interestPoints.end(), point) != cityInfo.interestPoints.end()) {
				continue;
			}
			auto size = std::min(cityInfo.height, cityInfo.width);
			if (IsPointTooClose(point, cityInfo.interestPoints, (float)size / 5)) {
				continue;
			}

			return point;
		}
	}

	std::vector<glm::uvec2> CityGenerator::GetStreetPointsBetween(
			CityInfo& cityInfo, 
			glm::uvec2 p1, 
			glm::uvec2 p2) {
		std::vector<glm::uvec2> results;
		auto newP1 = glm::uvec2(p1.x, p2.y);
		if (!ContainsPoint(cityInfo, newP1)) {
			results.push_back(newP1);
		}
		auto newP2 = glm::uvec2(p2.x, p1.y);
		if (!ContainsPoint(cityInfo, newP2) && newP1 != newP2) {
			results.push_back(newP2);
		}

		return results;
	}

	bool CityGenerator::ContainsPoint(CityInfo& cityInfo, glm::uvec2 point) {
		if (point == cityInfo.cityCenter) {
			return true;
		}
		return std::find(cityInfo.interestPoints.begin(), cityInfo.interestPoints.end(), point) != cityInfo.interestPoints.end();
	}

	std::vector<Street> CityGenerator::MakeStreetsFromPoints(std::vector<glm::uvec2>& points) {
		std::vector<Street> result; 
		
		for (auto point : points) {
			auto rightSide = RightSideConnection(point, points);
			if (rightSide) {
				Street street = { point, rightSide.get() };
				if (street.Distance() > 0) {
					result.push_back(street);
				}
			}
			auto topSide = TopSideConnection(point, points);
			if (topSide) {
				Street street = { point, topSide.get() };
				if (street.Distance() > 0) {
					result.push_back(street);
				}
			}
		}

		return result;
	}

	boost::optional<glm::uvec2> CityGenerator::RightSideConnection(glm::uvec2 origin, const std::vector<glm::uvec2>& points) {
		return SearchAdjacentClosestPoint(
			origin,
			points,
			[](glm::uvec2 origin, glm::uvec2 testPoint) { 
				return origin.y == testPoint.y && origin.x < testPoint.x; 
			},
			[](glm::uvec2 p1, glm::uvec2 p2) { return p2.x - p1.x; });
	}

	boost::optional<glm::uvec2> CityGenerator::TopSideConnection(glm::uvec2 origin, const std::vector<glm::uvec2>& points) {
		return SearchAdjacentClosestPoint(
			origin, 
			points, 
			[](glm::uvec2 origin, glm::uvec2 testPoint) { 
				return origin.x == testPoint.x && origin.y < testPoint.y; 
			},
			[](glm::uvec2 p1, glm::uvec2 p2) { return p2.y - p1.y; });
	}

	boost::optional<glm::uvec2> CityGenerator::SearchAdjacentClosestPoint(
			glm::uvec2 origin,
			const std::vector<glm::uvec2>& points,
			std::function<bool(glm::uvec2 origin, glm::uvec2 testPoint)> predicate,
			std::function<unsigned int(glm::uvec2 p1, glm::uvec2 p2)> distanceCalculator) {
		boost::optional<glm::uvec2> result;
		for (auto point : points) {
			if (origin == point) {
				continue;
			}
			if (!predicate(origin, point)) {
				continue;
			}
			if (!result) {
				result = point;
				continue;
			}
			auto distance = distanceCalculator(origin, result.get());
			auto testDistance = distanceCalculator(origin, point);
			if (testDistance < distance) {
				result = point;
			}
		}
		return result;
	}

	void CityGenerator::GetCrossingPointsBetweenStreets(
			std::vector<glm::uvec2>& streetPoints,
			std::vector<Street>& streets) {

		for (auto street : streets) {
			GetCrossingPointsBetweenStreets(streetPoints, street, streets);
		}
	}

	void CityGenerator::GetCrossingPointsBetweenStreets(
			std::vector<glm::uvec2>& streetPoints,
			Street& testStreet,
			std::vector<Street>& streets) {

		for (auto street : streets) {
			if (testStreet.GetOrientation() == street.GetOrientation()) {
				continue;
			}
			auto intersection = testStreet.IntersectionPoint(street);
			if (!intersection) {
				continue;
			}

			auto contains = std::find(streetPoints.begin(), streetPoints.end(), intersection.value()) != streetPoints.end();

			if (!contains) {
				streetPoints.push_back(intersection.value());
			}
		}
	}

	void CityGenerator::MoveCloserToOrigin(CityInfo& cityInfo) {
		std::vector<glm::uvec2>& points = cityInfo.interestPoints;
		auto minX = std::min_element(points.begin(), points.end(), [](glm::vec2 v1, glm::vec2 v2) { return v1.x < v2.x; })->x;
		auto minY = std::min_element(points.begin(), points.end(), [](glm::vec2 v1, glm::vec2 v2) { return v1.y < v2.y; })->y;
		auto sub = glm::uvec2(minX, minY);

		cityInfo.cityCenter -= sub;
		for (auto idx = 0u; idx < points.size(); idx++) {
			points[idx] = points[idx] - sub;
		}
	}

	bool CityGenerator::IsPointTooClose(glm::uvec2 testPoint, std::vector<glm::uvec2>& points, float maxDistance) {
		for (auto point : points) {
			auto fPoint = glm::vec2(point.x, point.y);
			auto fTestPoint = glm::vec2(testPoint.x, testPoint.y);
			auto distance = glm::distance(fTestPoint, fPoint);

			if (distance < maxDistance) {
				return true;
			}
		}
		return false;
	}
}