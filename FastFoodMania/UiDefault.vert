
out vec2 texCoord;

void main(void) {
    // Since we are using flat lines, our input only had two points: x and y.
    // Set the Z coordinate to 0 and W coordinate to 1
    gl_Position = MVP * vec4(position.x, position.y, position.z, 1.0);
 
	texCoord = textureCoordinates;
}