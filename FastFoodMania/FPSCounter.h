#pragma once

#include "SimpleEngine\SimpleEngine.h"

using namespace SimpleEngine;

namespace FFM {
	class FPSCounter : public Component {
	public:
		void Update(Timer timer) override;
	private:
		float _deltaTime = 0.0f;
	};
}