#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "UiEvents.h"

namespace FFM {
	SimpleEngine::Event<FFM::BuyRestaurantButtonEvent>& GetBuyRestaurantButton();
}