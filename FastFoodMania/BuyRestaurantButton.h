#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "BuyRestaurantAction.h"

using namespace SimpleEngine;

namespace FFM {
	class BuyRestaurantButton : public Component{
	public:
		void Initialize() override;
	private:
		void OnClickHandler(const int& i);
		ComponentHandle<Button> _button;
		//BuyRestaurantAction _buyRestaurant;
		Event<int>::EventConnection<int> _conn;
	};
}