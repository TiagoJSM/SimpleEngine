#include "Grid.h"

namespace FFM {
	void Grid::SetGridSize(int width, int height, int cellSize) {
		_width = width;
		_height = height;
		_cellSize = cellSize;

		_gridItems.resize(boost::extents[width][height]);
	}

	int Grid::GetWidth() {
		return _width;
	}

	int Grid::GetHeight() {
		return _height;
	}

	int Grid::GetCellSize() {
		return _cellSize;
	}

	void Grid::Update(Timer timer) {
		auto cam = Camera::currentCamera;
		auto s = Screen::Size();
		if (Input::s) {
			auto restaurants = gameObject->FindComponents<FFM::Restaurant>();

			for (auto restaurant : restaurants) {
				restaurant->gameObject->SetEnabled(!restaurant->gameObject->IsEnabled());
			}
		}
		
		auto ray = cam->ScreenPointToRay(Input::mousePointer);
		auto restaurants = gameObject->FindComponents<FFM::Restaurant>();
		auto intersect = false;
		for (auto restaurant : restaurants) {
			intersect = restaurant->GetBounds().Intersect(ray);
			if (intersect) {
				if (_lastSelectedRestaurant != nullptr) {
					_lastSelectedRestaurant->Highlight(false);
					_lastSelectedRestaurant = nullptr;
				}
				_lastSelectedRestaurant = restaurant;
				_lastSelectedRestaurant->Highlight(true);
				break;
			}
		}

		if (!intersect && _lastSelectedRestaurant != nullptr) {
			_lastSelectedRestaurant->Highlight(false);
			_lastSelectedRestaurant = nullptr;
		}
	}

	void Grid::AddToGridAt(GameObject go, int x, int y) {
		go->transform.translation = glm::vec3(x * _cellSize, y * _cellSize, 0);
		gameObject->AddChildren(go);
		_gridItems[x][y] = go;
	}

	GameObject Grid::GetGridObject(int x, int y) {
		return _gridItems[x][y];
	}
}