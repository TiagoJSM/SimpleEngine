#include "RestaurantOwner.h"
#include "IAction.h"
#include "ActionRunner.h"

namespace FFM {
	void RestaurantOwner::Initialize() {
		_runner = gameObject->FindComponent<ActionRunner>();
	}

	void RestaurantOwner::RunAction(IAction& action) {
		_runner->RunAction(action);
	}

	GameDataStore& RestaurantOwner::GetGameDataStore() {
		return _gameData;
	}
}