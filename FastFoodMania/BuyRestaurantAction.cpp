#include "BuyRestaurantAction.h"

namespace FFM {
	BuyRestaurantAction::BuyRestaurantAction(GameDataStore& store, ComponentHandle<Restaurant> restaurant)
		:_store(store), _restaurant(restaurant) {

	}

	bool BuyRestaurantAction::IsValid() {
		return _restaurant->owner == nullptr && _restaurant->price <= _store.money;
	}

	void BuyRestaurantAction::Execute() {
		//_restaurant->owner = owner;
		_store.money -= _restaurant->price;
	}
}