#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "Car.h"

using namespace SimpleEngine;

namespace FFM {
	class CarAi : public Component {
	public:
		CityInfo* cityInfo;

		void Initialize() override;
	private:
		ComponentHandle<Car> _car;
		Event<Car*>::EventConnection<Car*> _conn;

		void ArriveToTargetHandler(Car* car);
		glm::ivec2 GetNextPosition(glm::vec3 currentPosition);
	};

	ComponentHandle<CarAi> MakeCar(GameWorld& world, CityInfo* cityInfo);
}