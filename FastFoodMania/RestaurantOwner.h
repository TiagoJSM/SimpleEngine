#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "GameDataStore.h"
#include "BuyRestaurantAction.h"

using namespace SimpleEngine;

namespace FFM {
	class IAction;
	class ActionRunner;

	class RestaurantOwner : public Component {
	public:
		void Initialize() override;

		void RunAction(IAction& action);
		GameDataStore& GetGameDataStore();
	private:
		GameDataStore _gameData;
		ComponentHandle<ActionRunner> _runner;
	};
}