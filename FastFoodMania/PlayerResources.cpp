#include "PlayerResources.h"

namespace FFM {
	GameObject MakePlayerResources(GameWorld& world) {
		auto playerResourcesGO = world.CreateObject();
		auto playerResourcesUiTransform = playerResourcesGO->AddComponent<UiTransform>();
		playerResourcesUiTransform->anchors = { glm::vec2(0.7f, 0.95f), glm::vec2(1.0f, 1.0f) };
		playerResourcesGO->AddComponent<Image>();
		return playerResourcesGO;
	}
}