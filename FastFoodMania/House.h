#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "SimpleEngine\Box3.h"

using namespace SimpleEngine;

namespace FFM {
	GameObject MakeHouse(GameWorld& world);
}