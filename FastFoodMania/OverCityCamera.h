#pragma once

#include <glm/glm.hpp>
#include "SimpleEngine\SimpleEngine.h"
#include "Restaurant.h"

using namespace SimpleEngine;

namespace FFM {
	class OverCityCamera : public Component {
	public:
		glm::vec2 speed;
		OverCityCamera();
		void Update(Timer timer) override;
	private:
		glm::vec2 GetMovement();
	};
}