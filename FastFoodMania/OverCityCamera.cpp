#include "OverCityCamera.h"

namespace FFM {
	OverCityCamera::OverCityCamera() 
		: speed(3, 3) { }

	void OverCityCamera::Update(Timer timer) {
		auto movement = GetMovement();
		Camera::currentCamera->translation += glm::vec3(movement * speed * timer.deltaTime, 0);
		auto restaurants = gameObject->FindComponents<FFM::Restaurant>();
	}

	glm::vec2 OverCityCamera::GetMovement() {
		auto screenSize = Screen::Size();
		auto boundsRatio = 0.1f;

		auto leftBound = screenSize.x * boundsRatio;
		auto rightBound = screenSize.x * (1 - boundsRatio);
		auto bottomBound = screenSize.y * boundsRatio;
		auto topBound = screenSize.y * (1 - boundsRatio);

		glm::vec2 movement;

		auto mousePointer = Input::mousePointer;

		if (leftBound > mousePointer.x) {
			movement.x = -1;
		}
		else if (rightBound < mousePointer.x) {
			movement.x = 1;
		}

		if (bottomBound > mousePointer.y) {
			movement.y = -1;
		}
		else if (topBound < mousePointer.y) {
			movement.y = 1;
		}

		return movement;
	}
}