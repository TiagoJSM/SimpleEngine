#pragma once

#include "SimpleEngine\SimpleEngine.h"
#include "Grid.h"
#include "CityGenerator.h"

namespace FFM {
	class City : public SimpleEngine::Component {
	public:
		SimpleEngine::ComponentHandle<Grid> grid;
		CityInfo cityInfo;

		void Update(Timer timer) {
			auto center = cityInfo.cityCenter;
			Debug::DrawLine(glm::vec3(center.x, center.y, 0), glm::vec3(center.x, center.y, 5));

			for (auto point : cityInfo.interestPoints) {
				Debug::DrawLine(glm::vec3(point.x, point.y, 0), glm::vec3(point.x, point.y, 5));
			}
		}

		void Render(IRenderer& renderer) {
			auto position = gameObject->transform.translation;
			for (auto street : cityInfo.streets) {
				auto p1 = street.p1, p2 = street.p2;
				Debug::DrawLine(glm::vec3(p1.x, p1.y, 5), glm::vec3(p2.x, p2.y, 5));
			}
		}
	};
}