#include "Car.h"

namespace FFM {
	void Car::Update(Timer timer) {
		if (_target) {
			auto velocity = 1.f;
			auto transform = &gameObject->transform;
			auto currentPosition = gameObject->GetPosition();
			auto position2 = glm::ivec2(currentPosition.x, currentPosition.y);
			auto direction = _target.get() - position2;
			auto length = glm::length(glm::vec2(direction.x, direction.y));
			if (length == 0) {
				auto translation = transform->translation;
				transform->translation = glm::vec3(_target.get().x, _target.get().y, translation.z);
				_target = boost::none;
				onArrivedToTarget.Trigger(this);
				return;
			}
			auto normalizedDir = glm::normalize(glm::vec2(direction.x, direction.y));

			auto movement = timer.deltaTime * velocity;
			transform->translation += glm::vec3(movement * normalizedDir.x, movement * normalizedDir.y, 0);

			auto distance = glm::distance(
				glm::vec2(transform->translation.x, transform->translation.y),
				glm::vec2(_target.get().x, _target.get().y)
			);

			if (distance < 0.1f) {
				auto translation = transform->translation;
				transform->translation = glm::vec3(_target.get().x, _target.get().y, translation.z);
				_target = boost::none;
				onArrivedToTarget.Trigger(this);
			}
		}
	}

	void Car::Move(glm::ivec2 target) {
		_target = boost::optional<glm::ivec2>(target);
	}
}