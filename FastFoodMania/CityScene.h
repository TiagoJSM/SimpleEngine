#pragma once

#include "SimpleEngine\SimpleEngine.h"

#include "City.h"
#include "Grid.h"
#include "Restaurant.h"
#include "House.h"
#include "Road.h"
#include "CarAi.h"
#include "OverCityCamera.h"
#include "CityGenerator.h"
#include "SimpleEngine\DefaultGraphicObjects.h"
#include "FPSCounter.h"
#include "ActionRunner.h"
#include "PlayerOwner.h"
#include "BuyRestaurantButton.h"
#include "ActionButtonsBar.h"
#include "CityMap.h"
#include "PlayerResources.h"

using namespace SimpleEngine;
using namespace SimpleEngine::UI;

namespace FFM {
	#define RestaurantPerNBuildings	5
	class CityScene : public IScene {
	public:
		void BuildScene(GameWorld& world) override {
			auto screenSize = Screen::Size();
			_cam.fieldOfView = 90.0f;
			_cam.aspectRatio = (float)screenSize.x / (float)screenSize.y;
			_cam.nearClipping = 0.001f;
			_cam.farClipping = 100.0f;
			_cam.translation = glm::vec3(0.f, 0.f, 0);

			Camera::currentCamera = &_cam;

			auto gridGO = world.CreateObject();
			gridGO->transform.translation = glm::vec3(0, 0, 5);
			auto grid = gridGO->AddComponent<Grid>();
			grid->SetGridSize(30, 30, 1);

			cityInfo = CityGenerator::Generate(5, grid->GetWidth(), grid->GetHeight());

			auto city = world.CreateObject();
			city->AddComponent<City>([&](City* city) { 
				city->grid = grid;
				city->cityInfo = cityInfo;
			});
			city->AddComponent<ActionRunner>();
			//city->AddComponent<FPSCounter>();

			AddRoads(world, cityInfo, grid);
			AddBuildings(world, cityInfo, grid);

			auto car = MakeCar(world, &cityInfo);

			auto cameraGO = world.CreateObject();
			cameraGO->AddComponent<OverCityCamera>();

			auto restaurantOwnerGO = world.CreateObject();
			restaurantOwnerGO->AddComponent<ActionRunner>();
			restaurantOwnerGO->AddComponent<RestaurantOwner>();
			restaurantOwnerGO->AddComponent<PlayerOwner>();

			auto canvasGO = world.CreateObject();
			canvasGO->AddComponent<UiTransform>();
			canvasGO->AddComponent<Canvas>();
			
			/*auto button = world.CreateObject();
			button->AddComponent<Image>();
			button->AddComponent<Button>();
			auto buttonUiTransform = button->AddComponent<UiTransform>();
			buttonUiTransform->pivot = glm::vec2(0, 0);
			buttonUiTransform->height = 100;
			buttonUiTransform->width = 100;
			buttonUiTransform->anchors = Anchors::LeftBottom;
			button->AddComponent<BuyRestaurantButton>();
			canvasGO->AddChildren(button);*/

			canvasGO->AddChildren(MakeActionButtonsBar(world));
			canvasGO->AddChildren(MakeCityMap(world, &cityInfo)->gameObject);
			canvasGO->AddChildren(MakePlayerResources(world));
			
			//auto imageGO = world.CreateObject();
			//auto imageComp = imageGO->AddComponent<Image>();
			//imageGO->AddComponent<UiTransform>();
			//auto imageUiTransform = imageComp->GetUiTransform();
			//imageUiTransform->horizontalAlignment = HorizontalAlignment::Stretch;
			//imageUiTransform->verticalAlignment = VerticalAlignment::Bottom;
			//imageUiTransform->top = 100;
			//canvasGO->AddChildren(imageGO);

			/*auto textGO = world.CreateObject();
			auto textComp = textGO->AddComponent<Text>();
			textGO->AddComponent<UiTransform>();
			canvasGO->AddChildren(textGO);*/
		}

		void AddRoads(GameWorld& world, CityInfo& cityInfo, ComponentHandle<Grid> grid) {
			for (auto street : cityInfo.streets) {
				AddRoad(world, street, grid, 5.f);
			}
		}

		void AddBuildings(GameWorld& world, CityInfo& cityInfo, ComponentHandle<Grid> grid) {
			auto count = 0;
			for (auto street : cityInfo.streets) {
				AddBuildingsAroundStreet(world, street, grid, cityInfo);
			}
		}

		void AddRoad(GameWorld& world, Street& street, ComponentHandle<Grid> grid, float z) {
			auto result = GetRenderableStreet(street);

			if (!result) {
				return;
			}

			Street renderableStreet = result.get();

			auto orientation = renderableStreet.GetOrientation();
			auto road = MakeRoad(world);
			auto transform = &road->transform;

			if (orientation == StreetOrientation::Horizontal) {
				auto min = std::min(renderableStreet.p2.x, (int)renderableStreet.p1.x);
				auto max = std::max(renderableStreet.p2.x, (int)renderableStreet.p1.x);
				transform->scale = glm::vec3(max - min + 1, 1, 1);
				transform->translation = glm::vec3(((float)(max - min) / 2) + min, renderableStreet.p1.y, z);
			}
			else {
				auto min = std::min(renderableStreet.p2.y, (int)renderableStreet.p1.y);
				auto max = std::max(renderableStreet.p2.y, (int)renderableStreet.p1.y);
				transform->scale = glm::vec3(1, max - min + 1, 1);
				transform->translation = glm::vec3(renderableStreet.p1.x, ((float)(max - min) / 2) + min, 5.f);
			}
		}

		void AddBuildingsAroundStreet(GameWorld& world, Street street, ComponentHandle<Grid> grid, CityInfo& cityInfo) {
			auto orientation = street.GetOrientation();

			if (orientation == StreetOrientation::Vertical) {
				auto min = std::min(street.p2.y, (int)street.p1.y) + 1;
				auto max = std::max(street.p2.y, (int)street.p1.y);
				auto leftX = street.p1.x - 1;
				auto rightX = street.p1.x + 1;

				for (auto idx = 0; min < max; min++, idx++) {
					auto restaurant = (idx % RestaurantPerNBuildings) == 0;
					if (leftX > 0) {
						AddBuildingIfFree(world, grid, leftX, min, cityInfo, restaurant);
					}
					if (rightX < grid->GetWidth()) {
						AddBuildingIfFree(world, grid, rightX, min, cityInfo, restaurant);
					}
				}
			}
			else {
				auto min = std::min(street.p2.x, (int)street.p1.x) + 1;
				auto max = std::max(street.p2.x, (int)street.p1.x);
				auto bottomY = street.p1.y - 1;
				auto topY = street.p1.y + 1;

				for (auto idx = 0; min < max; min++, idx++) {
					auto restaurant = (idx % RestaurantPerNBuildings) == 0;
					if (bottomY > 0) {
						AddBuildingIfFree(world, grid, min, bottomY, cityInfo, restaurant);
					}
					if (topY < grid->GetHeight()) {
						AddBuildingIfFree(world, grid, min, topY, cityInfo, restaurant);
					}
				}
			}
		}

		void AddBuildingIfFree(
				GameWorld& world, 
				ComponentHandle<Grid> grid,
				unsigned int x, 
				unsigned int y, 
				CityInfo& cityInfo,
				bool restaurant) {
			auto obj = grid->GetGridObject(x, y);
			if (obj != nullptr) {
				return;
			}

			auto point = glm::ivec2(x, y);
			auto isStreetPosition = std::find_if(
				cityInfo.streets.begin(), 
				cityInfo.streets.end(), 
				[&](Street street){
					return street.IsPointContained(point, true);
				});

			if (isStreetPosition != cityInfo.streets.end()) {
				return;
			}

			auto go = restaurant ? MakeRestaurant(world)->gameObject : MakeHouse(world);
			grid->AddToGridAt(go, x, y);			
		}

		boost::optional<Street> GetRenderableStreet(Street& street) {
			Street result;
			if (street.GetOrientation() == StreetOrientation::Horizontal) {
				auto y = street.p1.y;
				auto min = std::min(street.p1.x, street.p2.x);
				auto max = std::max(street.p1.x, street.p2.x);
				result = { glm::uvec2(min, y) + glm::uvec2(1, 0), glm::uvec2(max, y) - glm::uvec2(1, 0) };
			}
			else {
				auto x = street.p1.x;
				auto min = std::min(street.p1.y, street.p2.y);
				auto max = std::max(street.p1.y, street.p2.y);
				result = { glm::uvec2(x, min) + glm::uvec2(0, 1), glm::uvec2(x, max) - glm::uvec2(0, 1) };
			}

			if (street.Distance() > 1) {
				return result;
			}

			return boost::optional<Street>();
		}
	private:
		Camera _cam;
		CityInfo cityInfo;
	};
}