#include "PlayerOwner.h"

namespace FFM {
	PlayerOwner::PlayerOwner() {
		_buyRestaurantButtonEvt = Event<BuyRestaurantButtonEvent>::EventConnection<BuyRestaurantButtonEvent>();
	}
	void PlayerOwner::Initialize() {
		_owner = gameObject->GetComponent<RestaurantOwner>();

		auto evt = GetBuyRestaurantButton().Subscribe([&](const BuyRestaurantButtonEvent& data) {
			BuyRestaurantButtonEventHandler(data);
		});
		_buyRestaurantButtonEvt = std::move(evt);
	}

	void PlayerOwner::Update(Timer timer) {
		if (Input::leftMouse) {
			auto restaurants = gameObject->FindComponents<FFM::Restaurant>();
			auto cam = Camera::currentCamera;
			auto ray = cam->ScreenPointToRay(Input::mousePointer);
			for (auto restaurant : restaurants) {
				auto intersect = restaurant->GetBounds().Intersect(ray);
				if (intersect) {
					_uiStore.selectedRestaurant = restaurant;
					break;
				}
			}
		}
	}

	void PlayerOwner::BuyRestaurantButtonEventHandler(const BuyRestaurantButtonEvent& data) {
		BuyRestaurantAction action(_owner->GetGameDataStore(), _uiStore.selectedRestaurant);
		_owner->RunAction(action);
	}
}