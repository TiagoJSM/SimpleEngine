#include "CarAi.h"

namespace FFM {
	void CarAi::Initialize() {
		_car = gameObject->GetComponent<Car>();
		_conn = _car->onArrivedToTarget.Subscribe(boost::bind(&CarAi::ArriveToTargetHandler, this, _1));

		auto point = cityInfo->interestPoints[0];
		gameObject->transform.translation = glm::vec3(point.x, point.y, 4.5f);

		auto position = GetNextPosition(_car->gameObject->GetPosition());
		_car->Move(position);
	}

	void CarAi::ArriveToTargetHandler(Car* car) {
		auto position = GetNextPosition(car->gameObject->GetPosition());
		car->Move(position);
	}

	glm::ivec2 CarAi::GetNextPosition(glm::vec3 currentPosition) {
		glm::ivec2 current(currentPosition.x, currentPosition.y);
		Street streets[4];
		unsigned int count = 0;
		for (auto street : cityInfo->streets) {
			if (street.p1 == current || street.p2 == current) {
				streets[count] = street;
				count++;

				if (count == 4) {
					break;
				}
			}
		}

		auto idx = MathUtils::Random(0, count);
		auto street = streets[idx];

		return street.p1 == current ? street.p2 : street.p1;
	}

	ComponentHandle<CarAi> MakeCar(GameWorld& world, CityInfo* cityInfo) {
		string materialName = "carMaterial";
		auto material = GraphicResourcesManager::Instance()->GetMaterial(materialName);

		string meshName = "Meshes/cube.obj";
		auto mesh = GraphicResourcesManager::Instance()->GetMesh(meshName);

		string vertex = "Standard.vert", fragment = "Standard.frag";
		auto stdShader = GraphicResourcesManager::Instance()->GetShader(vertex, fragment);

		string texturePath = "./Textures/Car.bmp";
		auto carTex = GraphicResourcesManager::Instance()->LoadTextureResource(texturePath);
		material->SetShader(stdShader);
		map<std::string, TextureResource> textures = { { "tex", carTex } };
		material->SetTextures(textures);

		auto go = world.CreateObject();
		go->transform.translation = glm::vec3(0.f, 0.f, 0.f);
		go->transform.scale = glm::vec3(0.1f, 0.1f, 0.1f);
		auto meshComp = go->AddComponent<MeshComponent>();
		meshComp->mesh = mesh;
		meshComp->material = material;
		go->AddComponent<Car>();
		return go->AddComponent<CarAi>([=](CarAi* carAi) { carAi->cityInfo = cityInfo; });
	}
}