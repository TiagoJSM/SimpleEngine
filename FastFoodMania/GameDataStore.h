#pragma once

#include <vector>
#include "Restaurant.h"

using namespace std;

namespace FFM {
	struct GameDataStore {
		vector<Restaurant*> ownedRestaurants;
		unsigned int money;
	};
}