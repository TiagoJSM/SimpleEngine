uniform vec3 start;
uniform vec3 end;

void main(void) {
	vec3 position = gl_VertexID == 0 ? start : end;
	gl_Position = MVP * vec4(position.x, position.y, position.z, 1.0);
}