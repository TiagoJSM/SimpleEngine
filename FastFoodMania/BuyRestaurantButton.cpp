#include "BuyRestaurantButton.h"
#include "AppEvents.h"

namespace FFM {
	void BuyRestaurantButton::Initialize() {
		_button = gameObject->GetComponent<Button>();
		_conn = _button->onClick.Subscribe(std::bind(&BuyRestaurantButton::OnClickHandler, this, std::placeholders::_1));
	}
	void BuyRestaurantButton::OnClickHandler(const int& i) {
		BuyRestaurantButtonEvent e;
		GetBuyRestaurantButton().Trigger(e);
	}
}