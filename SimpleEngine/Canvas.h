#pragma once

#include "GameObject.h"
#include "Component.h"
#include "Rect.h"
#include "ComponentHandle.h"

namespace SimpleEngine {
	namespace UI {
		class UiTransform;

		class Canvas : public Component {
		public:
			ComponentHandle<UiTransform> GetUiTransform();
			void Initialize() override;
			glm::mat4 ViewProjectionMatrix();
			glm::mat4 ViewMatrix();
			glm::mat4 ProjectionMatrix();
		private:
			ComponentHandle<UiTransform> _uiTransform;
		};
	}
}
