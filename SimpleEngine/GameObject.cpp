#include "GameObject.h"
#include "Component.h"
#include "GameWorld.h"
#include "ComponentManager.h"

namespace SimpleEngine {
	GameObjectData::GameObjectData(GameWorld* const world, ComponentManager* componentManager, unsigned int selfIndex)
		: _world(world), _componentManager(componentManager), _selfIndex(selfIndex), _enabled(true), _hasParent(false) {
		
	}

	GameObjectData::GameObjectData(const GameObjectData& other) {
		_world = other._world;
		_componentManager = other._componentManager;
		_enabled = other._enabled;
		_parentIndex = other._parentIndex;
		_hasParent = other._hasParent;
		_components = other._components;
		_childrenGameObjsIndex = other._childrenGameObjsIndex;
		_selfIndex = other._selfIndex;
		transform.rotation = other.transform.rotation;
		transform.scale = other.transform.scale;
		transform.translation = other.transform.translation;
	}

	GameObjectData& GameObjectData::operator=(const GameObjectData& other) {
		_world = other._world;
		_componentManager = other._componentManager;
		_enabled = other._enabled;
		_parentIndex = other._parentIndex;
		_hasParent = other._hasParent;
		_components = other._components;
		_childrenGameObjsIndex = other._childrenGameObjsIndex;
		_selfIndex = other._selfIndex;
		transform.rotation = other.transform.rotation;
		transform.scale = other.transform.scale;
		transform.translation = other.transform.translation;
		return *this;
	}

	void GameObjectData::Update(Timer timer) {
		for (auto component : _components) {
			if (component && component->enabled) {
				component->Update(timer);
			}
		}
	}

	void GameObjectData::PhysicsUpdate(Timer timer) {
		for (auto component : _components) {
			if (component) {
				component->PhysicsUpdate(timer);
			}
		}
	}

    bool GameObjectData::IsEnabled() {
		auto parent = GetParent();
		auto parentEnabled = parent ? parent->IsEnabled() : true;
        return _enabled && parentEnabled;
    }

    void GameObjectData::SetEnabled(bool enabled) {
        if (_enabled == enabled) {
            return;
        }
        _enabled = enabled;

        for (auto comp : _components) {
			if (comp) {
				if (_enabled) {
					comp->OnEnabled();
				}
				else {
					comp->OnDisabled();
				}
			}
        }
    }

	glm::mat4 GameObjectData::TransformMatrix() const {
		auto selfTransform = transform.TransformMatrix();
		auto parent = GetParent();
		if (!parent) {
			return selfTransform;
		}
		return parent->TransformMatrix() * selfTransform;
	}

	glm::vec3 GameObjectData::GetPosition() const {
		auto transformMat = TransformMatrix();
		return glm::column(transformMat, 3);
	}

	void GameObjectData::AddComponent(ComponentHandle<Component> component) {
		component->gameObject = _world->GetAt(_selfIndex);
		_components.push_back(component);
		component->Initialize();
	}

	void GameObjectData::AddChildren(GameObject children) {
		auto childrenParent = children->GetParent();
		if (childrenParent == this) {
			return;
		}
		if (childrenParent) {
			childrenParent->RemoveChildren(children);
		}
		children->_parentIndex = _selfIndex;
		children->_hasParent = true;
		_childrenGameObjsIndex.push_back(children.Index());
	}

	void GameObjectData::RemoveChildren(GameObject children) {
		if (children->GetParent() == this) {
			_childrenGameObjsIndex.erase(std::remove(_childrenGameObjsIndex.begin(), _childrenGameObjsIndex.end(), children), _childrenGameObjsIndex.end());
			children->_hasParent = false;
		}
	}

	bool GameObjectData::HasChildren() {
		return _childrenGameObjsIndex.size() > 0;
	}

	GameObject GameObjectData::GetParent() const {
		return _hasParent ? _world->GetAt(_parentIndex) : GameObject();
	}

	unsigned int GameObjectData::ChildCount() const {
		return _childrenGameObjsIndex.size();
	}

	GameObject GameObjectData::GetChildAt(unsigned int idx) {
		return _world->GetAt(_childrenGameObjsIndex[idx]);
	}

	Box3 GameObjectData::GetBoundingBox() {
		Box3 result;

		for (auto comp : _components) {
			if (comp) {
				result += comp->GetBoundingBox();
			}
		}

		for (auto idx = 0; idx < _childrenGameObjsIndex.size(); idx++) {
			result += GetChildAt(idx)->GetBoundingBox();
		}

		return result;
	}
}