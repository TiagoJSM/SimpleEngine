#pragma once

#include <vector>

using namespace std;

namespace SimpleEngine {
	template<typename TData>
	struct DataContainer {
		TData data;
		unsigned int entryId;
		bool valid;
	};

	template<class TData>
	using DataCollection = vector<DataContainer<TData>>;

	template<typename TData>
	class Handle {
	public:
		Handle(DataCollection<TData>* dataCollection = nullptr, unsigned int idx = 0, unsigned int entryId = 0)
			: _dataCollection(dataCollection), _idx(idx), _entryId(entryId) {}

		DataContainer<TData>* Get() {
			return _dataCollection->operator[](_idx);
		}
		unsigned int Index() const {
			return _idx;
		}
		TData* operator->() const {
			return this->operator bool() ? &_dataCollection->operator[](_idx).data : nullptr;
		}
		operator bool() const {
			return _dataCollection != nullptr && _dataCollection->at(_idx).valid && _dataCollection->at(_idx).entryId == _entryId;
		}
		bool operator==(const Handle<TData>& other) const {
			return _idx == other._idx && _entryId == other._entryId && _dataCollection == other._dataCollection;
		}
		bool operator!=(const Handle<TData>& other) const {
			return !this->operator==(other);
		}
		bool operator==(const TData* other) const {
			auto valid = this->operator bool();
			if (!valid && other == nullptr) {
				return true;
			}

			if (!valid || other == nullptr) {
				return false;
			}

			return &_dataCollection->at(_idx).data == other;
		}
		bool operator!=(const TData* other) const {
			return !this->operator==(other);
		}
		void operator=(const Handle<TData>* other) {
			if (other == nullptr) {
				_dataCollection = nullptr;
			}
			else {
				_dataCollection = other->_dataCollection;
				_idx = other->_idx;
				_entryId = other->_entryId;
			}
		}
		void operator=(const Handle<TData>& other) {
			_dataCollection = other._dataCollection;
			_idx = other._idx;
			_entryId = other._entryId;
		}
	private:
		DataCollection<TData>* _dataCollection;
		unsigned int _idx;
		unsigned int _entryId;
	};
}