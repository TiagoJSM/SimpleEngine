#include "Rect.h"

namespace SimpleEngine {
	Rect::Rect(): Rect(0, 0, 0, 0) { }
	Rect::Rect(float x, float y, float width, float height)
		: p1(glm::vec2(x, y)), width(width), height(height) { }

	glm::vec2 Rect::P2() const {
		return p1 + glm::vec2(width, height);
	}

	glm::vec2 Rect::Center() const {
		return glm::vec2(p1.x + width / 2, p1.y + height / 2);
	}

	bool Rect::Contains(const Rect other) const {
		auto thisP2 = this->P2();
		auto otherP2 = other.P2();
		return this->p1.x < otherP2.x && thisP2.x > other.p1.x &&
			this->p1.y < otherP2.y && thisP2.y > other.p1.y;
	}

	Rect Rect::GetTranslated(const glm::vec2 translation) const {
		return Rect(p1.x + translation.x, p1.y + translation.y, width, height);
	}
}