#include "Canvas.h"
#include "UiTransform.h"

namespace SimpleEngine {
	namespace UI {
		ComponentHandle<UiTransform> Canvas::GetUiTransform() {
			if (_uiTransform) {
				return _uiTransform;
			}
			_uiTransform = gameObject->GetComponent<UiTransform>();
			return _uiTransform;
		}

		void Canvas::Initialize() {
			auto uiTransform = GetUiTransform();
			auto transform = &gameObject->transform;
			uiTransform->width = 0;
			uiTransform->height = 0;
			transform->translation = glm::vec3();
		}

		glm::mat4 Canvas::ViewProjectionMatrix() {
			return ProjectionMatrix() * ViewMatrix();
		}

		glm::mat4 Canvas::ViewMatrix() {
			return glm::mat4();
		}

		glm::mat4 Canvas::ProjectionMatrix() {
			auto uiTransform = GetUiTransform();
			auto rect = uiTransform->GetRect();
			auto p2 = rect.P2();
			return glm::ortho(rect.p1.x, p2.x, rect.p1.y, p2.y, 0.f, 100.0f);
		}
	}
}