#pragma once

#include <SDL.h>

#define GL3_PROTOTYPES 1
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

#include <tuple>
#include <functional>

#include "IRenderer.h"
#include "Material.h"
#include "OpenGLLine.h"
#include "OpenGLQuad.h"
#include "MathUtils.h"
#include "DefaultGraphicObjects.h"
#include "Font.h"
#include "Frustum.h"
#include "Camera.h"
#include "RendererUtils.h"

using boost::any_cast;

namespace SimpleEngine {
	#define TEXTURE_POSITIONS 8

	class OpenGLRenderer : public IRenderer {
	public:
		bool OnInit(IGraphicResourcesLoader* loader) override;
		void PreRender(glm::mat4 viewProjection) override;
		void Submit() override;
		void PostRender() override;
		void CleanUp() override;

		void Render(MeshResource mesh, MaterialResource material, glm::mat4 model, RenderData* renderData = nullptr) override;
		void SubmitLine(glm::vec3 start, glm::vec3 end) override;
		void DrawQuad(glm::mat4 viewProjection, Rect quad, MaterialResource material, RenderData* renderData = nullptr) override;
		void DrawText(glm::mat4 viewProjection, string& text, MaterialResource material, FontResource font) override;
		Frustum GetFrustum(glm::mat4 viewProjection) override;
        glm::ivec2 GetWindowSize() override;

	private:
		const static unsigned int texturePositions[TEXTURE_POSITIONS];

		SDL_Window* _window;
		SDL_GLContext _mainContext;
		glm::mat4 _viewProjection;
        int _windowWidth = 1280;
        int _windowHeight = 720;
		IGraphicResourcesLoader* _loader;
		std::vector<RenderCommand> _renderCommands;

		OpenGLLine _line;
		OpenGLQuad _quad;
		Mesh* _quadMesh;
		Mesh* _lineMesh;
		ShaderResource _debugLineShader;
		OpenGLMaterial* _debugLineMat;

		void SetOpenGLAttributes();

		void SetMatrix(GLuint shaderProgram, const string& name, glm::mat4 mvp);
		void SetTextures(GLuint shaderProgram, const map<std::string, TextureResource>& textures);
		void SetVec4(GLuint shaderProgram, const std::string& name, glm::vec4 vec);
		void SetVec3(GLuint shaderProgram, const std::string& name, glm::vec3 vec);
		void SetVec2(GLuint shaderProgram, const std::string& name, glm::vec2 vec);
		void SetFloat(GLuint shaderProgram, const std::string& name, float value);
		void SetInt(GLuint shaderProgram, const std::string& name, int value);

		void ApplyRenderData(GLuint program, RenderData* renderData);
		void ActivateTextures(const std::map<std::string, TextureResource>& textures);
		void SubmitRender(GLuint program, RenderCommand& renderCommand, bool useProgram);
		bool IsNewProgram(boost::any& currentProgram, GLuint& program, boost::any& elementMaterialHandler);
	};
}