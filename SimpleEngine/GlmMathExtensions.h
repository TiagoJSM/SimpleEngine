#pragma once

#include <iostream>
#include <glm\glm.hpp>

namespace SimpleEngine {
	inline std::ostream &operator<<(std::ostream &os, glm::uvec2 const &vec) {
		return os << "{ " << vec.x << " " << vec.y << " }";
	}
}