#include "Frustum.h"

namespace SimpleEngine {
	Frustum::Frustum(glm::mat4 viewProj) {
		rightPlane = glm::vec4(viewProj[0][3] - viewProj[0][0],
			viewProj[1][3] - viewProj[1][0],
			viewProj[2][3] - viewProj[2][0],
			viewProj[3][3] - viewProj[3][0]);

		leftPlane = glm::vec4(viewProj[0][3] + viewProj[0][0],
			viewProj[1][3] + viewProj[1][0],
			viewProj[2][3] + viewProj[2][0],
			viewProj[3][3] + viewProj[3][0]);

		bottomPlane = glm::vec4(viewProj[0][3] + viewProj[0][1],
			viewProj[1][3] + viewProj[1][1],
			viewProj[2][3] + viewProj[2][1],
			viewProj[3][3] + viewProj[3][1]);

		topPlane = glm::vec4(viewProj[0][3] - viewProj[0][1],
			viewProj[1][3] - viewProj[1][1],
			viewProj[2][3] - viewProj[2][1],
			viewProj[3][3] - viewProj[3][1]);

		farPlane = glm::vec4(viewProj[0][3] - viewProj[0][2],
			viewProj[1][3] - viewProj[1][2],
			viewProj[2][3] - viewProj[2][2],
			viewProj[3][3] - viewProj[3][2]);

		nearPlane = glm::vec4(viewProj[0][3] + viewProj[0][2],
			viewProj[1][3] + viewProj[1][2],
			viewProj[2][3] + viewProj[2][2],
			viewProj[3][3] + viewProj[3][2]);

		rightPlane = glm::normalize(rightPlane);
		leftPlane = glm::normalize(leftPlane);
		bottomPlane = glm::normalize(bottomPlane);
		topPlane = glm::normalize(topPlane);
		farPlane = glm::normalize(farPlane);
		nearPlane = glm::normalize(nearPlane);
	}

	float Frustum::SphereInFrustum(Sphere sphere) {
		float d;
		auto center = sphere.center;

		glm::vec4 frustums[] = {rightPlane, leftPlane, bottomPlane, topPlane, farPlane, nearPlane};

		for (auto p = 0; p < 6; p++)
		{
			auto frustum = frustums[p];
			d = frustum.x * center.x + frustum.y * center.y + frustum.z * center.z + frustum.w;
			if (d <= -sphere.radius)
				return 0;
		}
		return d + sphere.radius;
	}

	Intersection Frustum::BoxInFrustum(Box3 box) {
		auto result = Intersection::Inside;
		glm::vec4 frustums[] = { rightPlane, leftPlane, bottomPlane, topPlane, farPlane, nearPlane };

		for (auto i = 0; i < 6; i++)
		{
			const float pos = frustums[i].w;
			const auto normal = glm::vec3(frustums[i]);

			auto dot = glm::dot(normal, box.GetPositiveVertex(normal));
			if (glm::dot(normal, box.GetPositiveVertex(normal)) + pos < 0.0f)
			{
				return Intersection::Outside;
			}

			if (glm::dot(normal, box.GetNegativeVertex(normal)) + pos < 0.0f)
			{
				result = Intersection::Intersect;
			}
		}

		return result;
	}
}