#pragma once

#include <glm\glm.hpp>
#include <limits.h>

namespace SimpleEngine {
	struct Color {
	public:
		static const Color White;

		float r, g, b, a;

		Color();
		Color(float r, float g, float b, float a);
		Color(glm::vec4 color);
		Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

		operator glm::vec4() const;

		static Color Lerp(Color a, Color b, float t);
	};
}