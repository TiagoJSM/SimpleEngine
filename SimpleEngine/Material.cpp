#include "Material.h"

namespace SimpleEngine {

	Material::Material(boost::any materialHandler) :_materialHandler(materialHandler) {}

	boost::any Material::GetHandler() const {
		return _materialHandler;
	}

	ShaderResource Material::GetShader() const {
		return _shader;
	}

	void Material::SetShader(ShaderResource shader) {
		auto old = _shader;
		_shader = shader;
		OnSetShader(old, _shader);
	}

	const std::map<std::string, TextureResource>& Material::GetTextures() const {
		return _textures;
	}

	Material::~Material() {
		CleanUp();
	}

	void Material::CleanUp() {
		//_textures.clear();
		_materialHandler = nullptr;
	}
}