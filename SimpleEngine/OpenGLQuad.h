#pragma once

#include <GL/glew.h>
#include <glm\glm.hpp>

namespace SimpleEngine {
	class OpenGLQuad {
	public:
		GLuint _vao;

		OpenGLQuad();
		void Render();
		static OpenGLQuad BuildGLQuad();
	private:
		OpenGLQuad(GLuint vao, GLuint vertexIndex, GLuint uvIndex, GLuint indicesBuffer);
		GLuint _vertexIndex, _uvIndex, _indicesBuffer;
	};
}