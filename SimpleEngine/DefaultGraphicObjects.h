#pragma once

#include "Typedefs.h"

namespace SimpleEngine {
	struct DefaultGraphicObjects {
		static ShaderResource DefaultShader;
		static ShaderResource DefaultUiShader;
		static MaterialResource UiDefaultMaterial;
		static TextureResource DefaultTexture;

		static void Initialize();
		static void CleanUp();
	};
}