#include "OpenGLRenderer.h"
#include "IGraphicResourcesLoader.h"

namespace SimpleEngine {

	const unsigned int OpenGLRenderer::texturePositions[TEXTURE_POSITIONS] = {
		GL_TEXTURE0,
		GL_TEXTURE1,
		GL_TEXTURE2,
		GL_TEXTURE3,
		GL_TEXTURE4,
		GL_TEXTURE5,
		GL_TEXTURE6,
		GL_TEXTURE7
	};

	bool OpenGLRenderer::OnInit(IGraphicResourcesLoader* loader) {

		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			return false;
		}

		if ((_window = SDL_CreateWindow("Hello World!",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            _windowWidth, _windowHeight, SDL_WINDOW_OPENGL)) == nullptr) {
			return false;
		}

		_mainContext = SDL_GL_CreateContext(_window);
		_loader = loader;

		SetOpenGLAttributes();

		// This makes our buffer swap syncronized with the monitor's vertical refresh
		SDL_GL_SetSwapInterval(1);

		// Init GLEW
		// Apparently, this is needed for Apple. Thanks to Ross Vander for letting me know
#ifndef __APPLE__
		glewExperimental = GL_TRUE;
		glewInit();
#endif

		glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		_line = OpenGLLine::BuildGLLine();
		_quad = OpenGLQuad::BuildGLQuad();
		_quadMesh = _loader->GetMesh();

		std::vector<glm::vec3> points =
		{
			glm::vec3(0, 0, 0),
			glm::vec3(0, 1, 0),
			glm::vec3(1, 1, 0),
			glm::vec3(1, 0, 0)
		};
		std::vector<glm::vec2> uvs =
		{
			glm::vec2(0, 1),
			glm::vec2(0, 0),
			glm::vec2(1, 0),
			glm::vec2(1, 1)
		};
		std::vector<unsigned int> indices =
		{
			0, 1, 2, 3
		};

		_quadMesh->SetVertices(points);
		_quadMesh->SetUVs(uvs);
		_quadMesh->SetIndices(indices);
		_quadMesh->ApplyDataToMesh();

		_lineMesh = _loader->GetMesh();

		std::vector<glm::vec3> linePoints =
		{
			glm::vec3(0, 0, 0),
			glm::vec3(0, 1, 0),
		};
		std::vector<glm::vec2> lineUvs =
		{
			glm::vec2(0, 1),
			glm::vec2(0, 0)
		};
		std::vector<unsigned int> lineIndices =
		{
			0, 1
		};

		_lineMesh->SetVertices(linePoints);
		_lineMesh->SetUVs(lineUvs);
		_lineMesh->SetIndices(lineIndices);
		_lineMesh->ApplyDataToMesh();

		auto debugLineVert = "DebugLine.vert", debugLineFrag = "DebugLine.frag";
		_debugLineShader = ShaderResource(_loader->GetShader(debugLineVert, debugLineFrag));
		auto materiaialHandler = _loader->GetMaterialHandler();
		_debugLineMat = new OpenGLMaterial(materiaialHandler);
		_debugLineMat->SetShader(_debugLineShader);
		return true;
	}

	void OpenGLRenderer::PreRender(glm::mat4 viewProjection) {
		_viewProjection = viewProjection * glm::scale(glm::vec3(1, 1, -1));
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_renderCommands.clear();
	}

	void OpenGLRenderer::Submit() {
		sort(_renderCommands.begin(), _renderCommands.end(), MaterialRenderDataComp);
		boost::any currentProgram;
		
		for (auto& element : _renderCommands) {
			GLuint program;
			auto useProgram = IsNewProgram(currentProgram, program, element.material->GetHandler());
			SubmitRender(program, element, useProgram);
		}
	}

	void OpenGLRenderer::PostRender() {
		SDL_GL_SwapWindow(_window);
	}

	void OpenGLRenderer::Render(MeshResource mesh, MaterialResource material, glm::mat4 model, RenderData* renderData) {
		_renderCommands.push_back({ 
			material.get(), 
			mesh.get(), 
			_viewProjection * model, 
			GL_TRIANGLES, 
			(int)mesh->GetMeshResource()->IndicesCount(), 
			GL_UNSIGNED_INT,
			renderData
		});
	}

	void OpenGLRenderer::SubmitLine(glm::vec3 start, glm::vec3 end) {
		auto handler = _debugLineMat->GetHandler();
		auto program = any_cast<GLuint>(handler);

		RenderData renderData;
		renderData.vec3s.clear();

		renderData.vec3s.push_back(std::tuple<string, glm::vec3>("start", start));
		renderData.vec3s.push_back(std::tuple<string, glm::vec3>("end", end));

		RenderCommand command =
		{
			_debugLineMat,
			_lineMesh,
			_viewProjection,
			GL_LINES,
			(int)2,
			GL_UNSIGNED_INT,
			&renderData
		};

		SubmitRender(program, command, true);
	}

	void OpenGLRenderer::DrawQuad(glm::mat4 viewProjection, Rect quad, MaterialResource material, RenderData* renderData) {
		auto model = MathUtils::CreateTransformationMatrix(glm::vec3(quad.p1, 0), glm::vec3(), glm::vec3(quad.width, quad.height, 1));
		_renderCommands.push_back({ 
			material.get(), 
			_quadMesh,
			viewProjection * model,
			GL_QUADS,
			4,
			GL_UNSIGNED_INT,
			renderData
		});
	}

	void OpenGLRenderer::DrawText(glm::mat4 viewProjection, string& text, MaterialResource material, FontResource font) {
		auto pen_x = 0;
		auto pen_y = 0;
		RenderData renderData;
		for (auto idx = 0u; idx < text.size(); idx++) {
			auto letter = text[idx];
			auto info = font->GetFontCharacterInfo(letter);
			auto texture = info.texture;
			auto model = MathUtils::CreateTransformationMatrix(
				glm::vec3(pen_x, pen_y, 0),
				glm::vec3(),
				glm::vec3(texture->GetWidth(), texture->GetHeight(), 1));
			if (renderData.textures.empty()) {
				renderData.textures.push_back(std::tuple<string, TextureResource>("tex", info.texture));
			}
			else {
				renderData.textures[0] = std::tuple<string, TextureResource>("tex", info.texture);
			}

			_renderCommands.push_back({
				material.get(),
				_quadMesh,
				viewProjection * model,
				GL_QUADS,
				4,
				GL_UNSIGNED_INT
			});

			pen_x += info.advance.x >> 6;
			pen_y += info.advance.y >> 6; /* not useful for now */
		}
	}

	void OpenGLRenderer::CleanUp() {
		_renderCommands.clear();

		delete _quadMesh;
		_quadMesh = nullptr;
		delete _lineMesh;
		_lineMesh = nullptr;
		delete _debugLineMat;
		_debugLineMat = nullptr;
		_debugLineShader = nullptr;

		// Delete our OpengL context
		SDL_GL_DeleteContext(_mainContext);

		// Destroy our window
		SDL_DestroyWindow(_window);

		// Shutdown SDL 2
		SDL_Quit();
	}

	Frustum OpenGLRenderer::GetFrustum(glm::mat4 viewProjection) {
		auto matrix = viewProjection * glm::scale(glm::vec3(1, 1, -1));
		return Frustum(matrix);
	}

    glm::ivec2 OpenGLRenderer::GetWindowSize() {
        return glm::ivec2(_windowWidth, _windowHeight);
    }

	void OpenGLRenderer::SetOpenGLAttributes() {
		// Set our OpenGL version.
		// SDL_GL_CONTEXT_CORE gives us only the newer version, deprecated functions are disabled
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		// 3.2 is part of the modern versions of OpenGL, but most video cards whould be able to run it
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

		// Turn on double buffering with a 24bit Z buffer.
		// You may need to change this to 16 or 32 for your system
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	}

	void OpenGLRenderer::SetMatrix(GLuint shaderProgram, const string& name, glm::mat4 mvp) {
		// Get a handle for our "MVP" uniform
		// Only during the initialisation
		auto mvpId = glGetUniformLocation(shaderProgram, name.c_str());
		// Send our transformation to the currently bound shader, in the "MVP" uniform
		// This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
		glUniformMatrix4fv(mvpId, 1, GL_FALSE, &mvp[0][0]);
	}
	void OpenGLRenderer::SetTextures(GLuint shaderProgram, const map<std::string, TextureResource>& textures) {
		auto idx = 0;
		for (auto tex : textures) {
			auto textureName = tex.first;
			auto texLoc = glGetUniformLocation(shaderProgram, (textureName + ".Sampler").c_str());
			glUniform1i(texLoc, idx);
			SetVec2(shaderProgram, textureName + ".Offset", tex.second->GetOffset());
			glActiveTexture(OpenGLRenderer::texturePositions[idx]);
			glBindTexture(GL_TEXTURE_2D, tex.second->GetHandler());

			idx++;

			if (idx == TEXTURE_POSITIONS) {
				break;
			}
		}
	}
	void OpenGLRenderer::SetVec4(GLuint shaderProgram, const std::string& name, glm::vec4 vec) {
		auto id = glGetUniformLocation(shaderProgram, name.c_str());
		glUniform4f(id, vec.x, vec.y, vec.z, vec.w);
	}
	void OpenGLRenderer::SetVec3(GLuint shaderProgram, const std::string& name, glm::vec3 vec) {
		auto id = glGetUniformLocation(shaderProgram, name.c_str());
		glUniform3f(id, vec.x, vec.y, vec.z);
	}
	void OpenGLRenderer::SetVec2(GLuint shaderProgram, const std::string& name, glm::vec2 vec) {
		auto id = glGetUniformLocation(shaderProgram, name.c_str());
		glUniform2f(id, vec.x, vec.y);
	}
	void OpenGLRenderer::SetFloat(GLuint shaderProgram, const std::string& name, float value) {
		auto id = glGetUniformLocation(shaderProgram, name.c_str());
		glUniform1f(id, value);
	}
	void OpenGLRenderer::SetInt(GLuint shaderProgram, const std::string& name, int value) {
		auto id = glGetUniformLocation(shaderProgram, name.c_str());
		glUniform1i(id, value);
	}
	void OpenGLRenderer::ApplyRenderData(GLuint program, RenderData* renderData) {
		if (renderData == nullptr) {
			return;
		}
		map<std::string, TextureResource> textureMap;
		for (auto tex : renderData->textures)
		{
			textureMap.insert(std::pair<string, TextureResource>(std::get<0>(tex), std::get<1>(tex)));
		}
		SetTextures(program, textureMap);

		for (auto vec4 : renderData->vec4s)
		{
			SetVec4(program, std::get<0>(vec4), std::get<1>(vec4));
		}
		for (auto vec3 : renderData->vec3s)
		{
			SetVec3(program, std::get<0>(vec3), std::get<1>(vec3));
		}
		for (auto value : renderData->floats)
		{
			SetFloat(program, std::get<0>(value), std::get<1>(value));
		}
		for (auto integer : renderData->integers)
		{
			SetInt(program, std::get<0>(integer), std::get<1>(integer));
		}
	}

	void OpenGLRenderer::ActivateTextures(const std::map<std::string, TextureResource>& textures) {
		auto idx = 0;
		for (auto tex : textures) {
			glActiveTexture(OpenGLRenderer::texturePositions[idx]);
			glBindTexture(GL_TEXTURE_2D, tex.second->GetHandler());
			idx++;
		}
	}

	void OpenGLRenderer::SubmitRender(GLuint program, RenderCommand& renderCommand, bool useProgram) {
		if (useProgram) {
			glUseProgram(program);
		}
		ActivateTextures(renderCommand.material->GetTextures());
		SetMatrix(program, "MVP", renderCommand.mvp);
		auto meshHandler = *static_cast<GLuint*>(renderCommand.mesh->GetMeshResource()->GetHandler());
		glBindVertexArray(meshHandler);

		ApplyRenderData(program, renderCommand.renderData);

		glDrawElements(
			renderCommand.mode,		// mode
			renderCommand.count,	// count
			renderCommand.type,		// type
			(void*)0				// element array buffer offset
		);
	}

	bool OpenGLRenderer::IsNewProgram(boost::any& currentProgram, GLuint& program, boost::any& elementMaterialHandler) {
		auto useProgram = false;
		if (currentProgram.empty()) {
			auto handler = elementMaterialHandler;
			program = any_cast<GLuint>(handler);
			currentProgram = handler;
			useProgram = true;
		}
		else {
			auto handler = elementMaterialHandler;
			program = any_cast<GLuint>(handler);
			if (any_cast<GLuint>(currentProgram) != program) {
				currentProgram = handler;
				useProgram = true;
			}
		}
		return useProgram;
	}
}