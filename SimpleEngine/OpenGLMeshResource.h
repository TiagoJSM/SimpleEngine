#pragma once

#include <GL/glew.h>

#include "IMeshResource.h"
#include "OpenGLGraphicsResourceLoader.h"

namespace SimpleEngine {
	#define PositionAttributeIndex 0
	#define ColorAttributeIndex 1
	#define UvAttributeIndex 2
	#define IndicesAttributeIndex 3
	#define VboCount 4

	class OpenGLMeshResource : public IMeshResource {
	public:
		OpenGLMeshResource();
		void* GetHandler() override;
		unsigned int IndicesCount() override;
		void BuildGraphicHandlers(
			const vector<glm::vec3>& vertices, 
			const vector<Color>& colors,
			const vector<glm::vec2>& uvs,
			const vector<unsigned int>& indices) override;
		~OpenGLMeshResource() override;
	private:
		GLuint _vbo[VboCount], _vao;
		unsigned int _indicesCount;
	};
}