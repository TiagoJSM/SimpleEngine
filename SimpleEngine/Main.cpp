// Headerphile.com OpenGL Tutorial part 1
// A Hello World in the world of OpenGL ( creating a simple windonw and setting background color )
// Source code is an C++ adaption / simplicication of : https://www.opengl.org/wiki/Tutorial1:_Creating_a_Cross_Platform_OpenGL_3.2_Context_in_SDL_(C_/_SDL)
// Compile : clang++ main.cpp -lGL -lSDL2 -std=c++11 -o Test

// C++ Headers
#include <vector>
#include <iostream>
#include <functional>

#include <glm/glm.hpp>

#include "Runner.h"

#include "Renderer.h"
#include "../Box2D/Box2D.h"

#include "SimpleEngine.h"

using namespace std;

class Run {
public:
	static int run(int argc, char *argv[], SimpleEngine::IScene& startingScene)
	{
		b2Vec2 gravity(0.0f, -10.0f);
		b2World world(gravity);

		SimpleEngine::OpenGLRenderer renderer = SimpleEngine::OpenGLRenderer();
		SimpleEngine::OpenGLGraphicsResourceLoader loader("Common", "BaseVertex.vert", "BaseFragment.frag");

		SimpleEngine::Runner runner(renderer, loader, startingScene);

		return runner.OnExecute();
	}
};
