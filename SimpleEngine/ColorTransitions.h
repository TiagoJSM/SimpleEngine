#pragma once

#include "Color.h"

namespace SimpleEngine {
	namespace UI {
		struct ColorTransitions {
			Color standard;
			Color highlight;
			Color pressed;
			float highlightMultiplier;
			float transitionTime;

			ColorTransitions();
		};
	}
}