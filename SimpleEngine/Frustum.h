#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include "Sphere.h"
#include "Box3.h"

namespace SimpleEngine {

	enum class Intersection {
		Inside,
		Intersect,
		Outside
	};

	//References: 
	// http://www.crownandcutlass.com/features/technicaldetails/frustum.html
	// http://www.lighthouse3d.com/tutorials/view-frustum-culling/geometric-approach-testing-boxes/
	// https://www.reddit.com/r/gamedev/comments/xj47t/does_glm_support_frustum_plane_extraction/
	// https://github.com/MichaelBeeu/OctreeDemo/blob/master/src/Frustum.cpp
	struct Frustum {
		glm::vec4 rightPlane;
		glm::vec4 leftPlane;
		glm::vec4 topPlane;
		glm::vec4 bottomPlane;
		glm::vec4 nearPlane;
		glm::vec4 farPlane;

		Frustum(glm::mat4 viewProj);

		float SphereInFrustum(Sphere sphere);
		Intersection BoxInFrustum(Box3 box);
	};
}