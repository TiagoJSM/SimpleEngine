#pragma once

#include <vector>
#include <tuple>
#include "Typedefs.h"
#include "Material.h"

namespace SimpleEngine {
	struct RenderData {
	public:
		std::vector<std::tuple<std::string, TextureResource>> textures;
		std::vector<std::tuple<std::string, glm::vec3>> vec3s;
		std::vector<std::tuple<std::string, glm::vec4>> vec4s;
		std::vector<std::tuple<std::string, int>> integers;
		std::vector<std::tuple<std::string, float>> floats;

		void SetVector4(std::string name, const glm::vec4 vec);
		void SetFloat(std::string name, const float value);
		void SetTexture(std::string name, const TextureResource value);
		bool TryGetVector4(std::string name, glm::vec4& data);
		bool TryGetFloat(std::string name, float& value);

	private:
		template<typename TData>
		void SetData(std::vector<std::tuple<std::string, TData>>& collection, std::string name, const TData value);
		template<typename TData>
		bool TryGetData(std::vector<std::tuple<std::string, TData>>& collection, std::string name, TData& data);
	};
}