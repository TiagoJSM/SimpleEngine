#pragma once

#include "Collider2D.h"

namespace SimpleEngine {
    class BoxCollider2D : public Collider2D {
    public:
        void Initialize() override;
        void SetSize(float x, float y);
    };
}