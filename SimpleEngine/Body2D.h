#pragma once

#include "../Box2D/Box2D.h"
#include <boost\signals2.hpp>

#include "SimpleEngine.h"

namespace SimpleEngine {
    class Collider2D; 
    
    typedef boost::signals2::signal<void(Collider2D*)> OnColliderEnter2D;
    typedef boost::signals2::signal<void(Collider2D*)> OnColliderExit2D;

	class Body2D : public Component {
	public:
        OnColliderEnter2D onColliderEnter2D;
        OnColliderExit2D onColliderExit2D;

		void Initialize() override;
		void PhysicsUpdate(Timer timer) override;
        glm::vec2 GetPosition();
        void SetPosition(glm::vec2 position);
        glm::vec2 GetVelocity();
        void SetVelocity(glm::vec2 velocity);
        bool IsActive();
        void SetActive(bool active);
        b2Body* GetPhysicsEngineBody();
        float GetMass();
        void SetMass(float mass);
        float GetGravityScale();
        void SetGravityScale(float scale);
        void ApplyForce(glm::vec2 force);
        void ApplyImpulse(glm::vec2 force);
        void RaiseOnColliderEnter(Collider2D* collider);
        void RaiseOnColliderExit(Collider2D* collider);
		~Body2D();
	private:
		b2Body* _body;
	};
}