#pragma once

#include <glm/glm.hpp>
#include "Ray.h"

namespace SimpleEngine {
	struct Sphere final {
		glm::vec3 center;
		float radius;
	};
}