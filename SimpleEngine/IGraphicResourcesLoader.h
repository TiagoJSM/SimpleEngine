#pragma once

#include <boost/any.hpp>

#include "ITextureResource.h"
#include "IMeshResource.h"
#include "Mesh.h"
#include "IShader.h"

using boost::any_cast;

namespace SimpleEngine {
	class IGraphicResourcesLoader {
	public:
		virtual Mesh* GetMesh() = 0;
		virtual ITextureResource* GetTextureResource(unsigned int width, unsigned int height) = 0;
		virtual IShader* GetShader(const string& vertexShaderPath, const string& fragmentShaderPath) = 0;
		virtual boost::any GetMaterialHandler() = 0;
	};
}