#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <math.h>  
#include <time.h>
#include <random>

#include "Sphere.h"
#include "Box3.h"

namespace SimpleEngine {
	class MathUtils {
	public:
		const static float PI;
		const static float Rad2Deg;
		const static float Deg2Rad;
		static glm::mat4 CreateTransformationMatrix(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale);
		static float Angle(glm::vec2 v1, glm::vec2 v2);
		static unsigned int Random(unsigned int min, unsigned int max);
		static int NextPowerOf2(int value);
		template <typename T> 
		static int Sign(T val) {
			return (T(0) < val) - (val < T(0));
		}
		static Sphere CalculateSphere(std::vector<glm::vec3>& points);
		static Box3 CalculateBox(std::vector<glm::vec3>& points);
		static float Clamp01(float value);
		static float Lerp(float a, float b, float t);
	};
}