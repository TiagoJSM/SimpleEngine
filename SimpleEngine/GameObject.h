#pragma once

#include <vector>
#include <functional>
#include <algorithm>
#include <glm/gtc/matrix_access.hpp>

#include "IRenderer.h"
#include "Timer.h"
#include "Input.h"
#include "BaseIncludes.h"
#include "TransformComponent.h"
#include "Handle.h"
#include "ComponentHandle.h"

namespace SimpleEngine {
	class GameWorld;
	class GameObjectData;
	class ComponentManager;

	using GameObjectEntry = DataContainer<GameObjectData>;
	using GameObject = Handle<GameObjectData>;

	class GameObjectData {
	public:
		typedef std::vector<GameObject>::iterator iterator;
		
		TransformComponent transform;

		GameObjectData(GameWorld* const world, ComponentManager* componentManager, unsigned int selfIndex);
		GameObjectData(const GameObjectData& other);
		GameObjectData& operator=(const GameObjectData& other);
		void Update(Timer timer);
		void PhysicsUpdate(Timer timer);
        bool IsEnabled();
        void SetEnabled(bool enabled);
		glm::mat4 TransformMatrix() const;
		glm::vec3 GetPosition() const;
        void AddComponent(ComponentHandle<Component> component);
        template<typename TComponent, typename = std::enable_if_t<std::is_base_of<Component, TComponent>::value>>
		ComponentHandle<TComponent> AddComponent(std::function<void(TComponent*)> initializer = nullptr) {
			auto component = _componentManager->CreateComponent<TComponent>();
			_components.push_back(component.Cast<Component>());
            component->gameObject = _world->GetAt(_selfIndex);
            if (initializer != nullptr) {
                initializer(component.operator->());
            }
            component->Initialize();
			return component;
        }
		void AddChildren(GameObject children);
		void RemoveChildren(GameObject children);
		bool HasChildren();
		GameObject GetParent() const;
		unsigned int ChildCount() const;
		GameObject GetChildAt(unsigned int idx);

		template<typename TComponent>
		vector<ComponentHandle<TComponent>> GetComponents() {
			vector<ComponentHandle<TComponent>> result;
			GetComponents<TComponent>(result);
			return result;
		}
		template<typename TComponent>
		ComponentHandle<TComponent> GetComponent() {
			for (auto comp : _components) {
				auto safeCasted = comp.SafeCast<TComponent>();
				if (safeCasted) {
					return safeCasted;
				}
			}
			return ComponentHandle<TComponent>();
		}
		template<typename TComponent>
		ComponentHandle<TComponent> GetComponentInParent(bool considerSelf = true) {
			if (considerSelf) {
				auto component = GetComponent<TComponent>();
				if (component) {
					return component;
				}
			}
			auto parent = GetParent();
			if (!parent) {
				return nullptr;
			}
			return parent->GetComponentInParent<TComponent>();
		}

		template<typename TComponent>
		vector<ComponentHandle<TComponent>> GetComponentsInChildren(bool considerSelf = true, bool nestedChildren = true) {
			vector<ComponentHandle<TComponent>> components;
			if (considerSelf) {
				auto component = GetComponent<TComponent>();
				if (component) {
					components.push_back(component);
				}
			}
			for (auto idx = 0; idx < ChildCount(); idx++) {
				GetChildAt(idx)->GetComponentsInChildren(components, nestedChildren);
			}
			
			return components;
		}

		template<typename TComponent>
		vector<ComponentHandle<TComponent>> FindComponents() {
			vector<ComponentHandle<TComponent>> result;
			for (auto it = _world->begin(); it != _world->end(); ++it) {
				(*it)->GetComponents<TComponent>(result);
			}
			return result;
		}
		
		template<typename TComponent>
		ComponentHandle<TComponent> FindComponent() {
			for (auto it = _world->begin(); it != _world->end(); ++it) {
				auto component = (*it)->GetComponent<TComponent>();
				if (component) {
					return component;
				}
			}
			return nullptr;
		}
		Box3 GetBoundingBox();
	private:
        bool _enabled;
		unsigned int _parentIndex;
		bool _hasParent;
		std::vector<ComponentHandle<Component>> _components;
		std::vector<unsigned int> _childrenGameObjsIndex;
		GameWorld* _world;
		ComponentManager* _componentManager;
		unsigned int _selfIndex;

		template<typename TComponent>
		void GetComponents(vector<ComponentHandle<TComponent>>& result) {
			for (auto comp : _components) {
				auto safeCasted = comp.SafeCast<TComponent>();
				if (safeCasted) {
					result.push_back(safeCasted);
				}
			}
		}
		template<typename TComponent>
		void GetComponentsInChildren(vector<ComponentHandle<TComponent>>& result, bool searchInChildren = true) {
			vector<ComponentHandle<TComponent>> comps;
			GetComponents<TComponent>(comps);
			for (auto comp : comps) {
				result.push_back(comp);
				if (searchInChildren) {
					comp->gameObject->GetComponentsInChildren(result, searchInChildren);
				}
			}
		}
	};
}
