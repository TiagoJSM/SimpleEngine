#pragma once

#include <Map>
#include <functional>
#include <memory>

namespace SimpleEngine {
	template <typename TResource, typename TKey>
	std::shared_ptr<TResource> GetResource(
			TKey& key,
			std::map<TKey, std::weak_ptr<TResource>>& container,
			std::function<TResource*()> loadResource,
			std::function<void(TResource*)> onDelete,
			bool* isNew = nullptr) {
		auto search = container.find(key);
		if (search != container.end()) {
			std::weak_ptr<TResource> ptr = search->second;
			if (isNew != nullptr) {
				*isNew = false;
			}
			return ptr.lock();
		}
		auto resource = loadResource();
		std::shared_ptr<TResource> result(resource, onDelete);
		std::weak_ptr<TResource> weakRef = result;
		container.insert(
			std::pair<TKey, std::weak_ptr<TResource>>(
				key, weakRef));
		if (isNew != nullptr) {
			*isNew = true;
		}
		return result;
	}
}