#include "UiHandler.h"
#include "UiComponent.h"
#include "IMouseClick.h"
#include "Button.h"

namespace SimpleEngine {
	namespace UI {
		bool UiHandler::_leftMouseDownLastUpdate = false;
		bool UiHandler::_leftMouseUpLastUpdate = false;
		GameObject UiHandler::_mouseLeftDownGo;
		GameObject UiHandler::_mouseTargetGo;

		void UiHandler::Update(GameWorld& world) {
			for (auto go : world) {
				auto canvas = go->GetComponent<Canvas>();
				if (canvas) {
					auto translation = Camera::ScreenToWorld(Input::mousePointer, 0.0f, canvas->ViewMatrix(), canvas->ProjectionMatrix());
					auto ray = Camera::ScreenPointToRay(Input::mousePointer, translation, canvas->ViewMatrix(), canvas->ProjectionMatrix());

					auto target = FindTargetGameObject(go, translation, ray);
					//if (target) {
						HandleUiEventsOnGameObject(target);
					//}
					
					//HandleUiEventsOnCanvas(go, translation, ray, [](GameObject go) {HandleUiEventsOnGameObject(go); });
				}
			}
		}

		void UiHandler::HandleUiEventsOnCanvas(GameObject go, glm::vec3 translation, Ray ray, std::function<void(GameObject)> action) {
			//auto childCount = go->ChildCount();
			//for (auto idx = 0; idx < childCount; idx++) {
			//	auto child = go->GetChildAt(idx);
			//	auto box = child->GetBoundingBox();
			//	if (box && box.Intersect(ray)) {
			//		HandleUiEventsOnGameObject(child);
			//		//return;
			//	}
			//	HandleUiEventsOnCanvas(child, translation, ray);
			//}

			//HandleMousePositionOnGameObject(go, canvas);
		}

		void UiHandler::HandleUiEventsOnGameObject(GameObject go) {
			HandleMousePositionOnGameObject(go);

			if (go) {
				if (Input::leftMouse && !_leftMouseDownLastUpdate) {
					_leftMouseDownLastUpdate = true;
					_mouseLeftDownGo = go;
				}
				if (!Input::leftMouse && _leftMouseDownLastUpdate) {
					_leftMouseDownLastUpdate = false;
					if (_mouseLeftDownGo == go) {
						auto comps = go->GetComponents<Component>();
						for (auto comp : comps) {
							auto mouse = comp.SafeCast<IMouseClick>();
							if (mouse) {
								mouse->Click();
							}
						}
						_mouseLeftDownGo = nullptr;
					}
				}
			}
		}

		void UiHandler::HandleMousePositionOnGameObject(GameObject go) {
			if (go != nullptr) {
				if (_mouseTargetGo != nullptr && go != _mouseTargetGo) {
					auto mouseExit = _mouseTargetGo->GetComponent<IMouseExit>();
					if (mouseExit) {
						mouseExit->Exit();
					}

					auto mouseEnter = go->GetComponent<IMouseEnter>();
					if (mouseEnter) {
						mouseEnter->Enter();
					}
				}
				else if (_mouseTargetGo == nullptr) {
					auto mouseEnter = go->GetComponent<IMouseEnter>();
					if (mouseEnter) {
						mouseEnter->Enter();
					}
				}
			}
			else if (_mouseTargetGo != nullptr) {
				auto mouseExit = _mouseTargetGo->GetComponent<IMouseExit>();
				if (mouseExit) {
					mouseExit->Exit();
				}
			}
			_mouseTargetGo = go;
		}

		GameObject UiHandler::FindTargetGameObject(GameObject go, glm::vec3 translation, Ray ray) {
			auto childCount = go->ChildCount();
			for (auto idx = 0; idx < childCount; idx++) {
				auto child = go->GetChildAt(idx);
				auto box = child->GetBoundingBox();
				if (box && box.Intersect(ray)) {
					auto targetChild = FindTargetGameObject(child, translation, ray);
					return targetChild ? targetChild : child;
				}
				//HandleUiEventsOnCanvas(child, translation, ray);
			}
			return nullptr;
		}
	}
}