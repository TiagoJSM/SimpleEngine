#include "StackPanel.h"

namespace SimpleEngine {
	namespace UI {
		StackPanel::StackPanel() {
			direction = Direction::Horizontal;
		}

		void StackPanel::Update(Timer timer) {
			RearrangeLayout();
		}

		void StackPanel::RearrangeLayout() {
			auto uiElements = gameObject->GetComponentsInChildren<UiTransform>(false, false);

			if (direction == Direction::Horizontal) {
				ArrangeHorizontalLayout(uiElements);
			}
			else {
				ArrangeVerticalLayout(uiElements);
			}
		}

		void StackPanel::ArrangeHorizontalLayout(vector<ComponentHandle<UiTransform>>& elements) {
			auto uiTransform = this->GetUiTransform();
			auto panelRect = uiTransform->GetRect();
			auto elementsWidth = panelRect.width / elements.size();

			auto x = 0.f;
			for (auto comp : elements) {
				comp->anchors = Anchors::LeftBottom;

				auto& transform = comp->gameObject->transform;
				transform.translation = glm::vec3(x, 0, 0);
				auto rect = comp->GetRect();

				if (expandWidth) {
					comp->width = elementsWidth;
				}

				x += rect.width;
			}
		}

		void StackPanel::ArrangeVerticalLayout(vector<ComponentHandle<UiTransform>>& elements) {
			auto uiTransform = this->GetUiTransform();
			auto panelRect = uiTransform->GetRect();
			auto elementsHeight = panelRect.height / elements.size();

			if (elements.size() == 0) {
				return;
			}

			auto y = elements[0]->height;
			for (auto comp : elements) {
				comp->anchors = Anchors::LeftTop;
				//comp->pivot = glm::vec2(0, 1);

				auto& transform = comp->gameObject->transform;
				transform.translation = glm::vec3(0, -y, 0);
				auto rect = comp->GetRect();

				if (expandHeight) {
					comp->height = elementsHeight;
				}

				y += rect.height;
			}
		}
	}
}