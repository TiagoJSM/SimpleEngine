#include "OpenGLGraphicsResourceLoader.h"
#include "OpenGLMeshResource.h"

namespace SimpleEngine {
	OpenGLGraphicsResourceLoader::OpenGLGraphicsResourceLoader(
		const string& common, const string& baseVertexShader, const string& baseFragmentShader)
		:_commonShader(common), _baseVertexShader(baseVertexShader), _baseFragmentShader(baseFragmentShader) { }

	Mesh* OpenGLGraphicsResourceLoader::GetMesh() {
		auto meshResource = new OpenGLMeshResource();
		return new Mesh(meshResource);
	}
	ITextureResource* OpenGLGraphicsResourceLoader::GetTextureResource(
			unsigned int width, unsigned int height) {
		auto texture = new OpenGLTextureResource(width, height);
		texture->GenerateGraphicHandlers();
		return texture;
	}
	IShader* OpenGLGraphicsResourceLoader::GetShader(
		const string& vertexShaderPath, const string& fragmentShaderPath) {
		auto shader = new OpenGLShader();
		shader->CreateShader(_commonShader, _baseVertexShader, _baseFragmentShader, vertexShaderPath, fragmentShaderPath);
		return shader;
	}

	boost::any OpenGLGraphicsResourceLoader::GetMaterialHandler() {
		auto shaderProgram = glCreateProgram();

		glBindAttribLocation(shaderProgram, 0, "position");
		glBindAttribLocation(shaderProgram, 1, "color");
		glBindAttribLocation(shaderProgram, 2, "textureCoordinates");

		return shaderProgram;
	}
}