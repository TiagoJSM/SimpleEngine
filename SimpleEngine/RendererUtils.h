#pragma once

#include <functional>
#include "boost\any.hpp"
#include <GL\glew.h>
#include <glm\glm.hpp>

#include "Material.h"
#include "Mesh.h"
#include "Typedefs.h"

namespace SimpleEngine {
	struct RenderCommand {
		Material* material;
		Mesh* mesh;
		glm::mat4 mvp;
		GLenum mode; 
		GLsizei count; 
		GLenum type;
		RenderData* renderData;
	};

	bool MaterialRenderDataComp(RenderCommand& i, RenderCommand& j);
}