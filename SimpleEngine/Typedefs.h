#pragma once

#include <memory>

#include <ft2build.h>
#include FT_FREETYPE_H

namespace SimpleEngine {
	class Mesh;
	class ITextureResource;
	class IShader;
	class Material;
	class Font;

	typedef std::shared_ptr<Mesh> MeshResource;
	typedef std::shared_ptr<ITextureResource> TextureResource;
	typedef std::shared_ptr<IShader> ShaderResource;
	typedef std::shared_ptr<Material> MaterialResource;
	typedef std::shared_ptr<FT_FaceRec_> FontFaceResource;
	typedef std::shared_ptr<Font> FontResource;
}