#include "OpenGLTextureResource.h"

namespace SimpleEngine {
	OpenGLTextureResource::OpenGLTextureResource(unsigned int width, unsigned int height)
		:_width(width), _height(height), _wrappingMode(WrappingMode::Repeat) {}

	unsigned int OpenGLTextureResource::GetWidth() {
		return _width;
	}
	unsigned int OpenGLTextureResource::GetHeight() {
		return _height;
	}

	unsigned int OpenGLTextureResource::GetHandler() {
		return _texture;
	}

	glm::vec2 OpenGLTextureResource::GetOffset() {
		return _offset;
	}
	void OpenGLTextureResource::SetOffset(glm::vec2 offset) {
		_offset = offset;
	}

	WrappingMode OpenGLTextureResource::GetWrappingMode() {
		return _wrappingMode;
	}

	void OpenGLTextureResource::SetWrappingMode(WrappingMode mode) {
		_wrappingMode = mode;
		AssignWrappingMode(_wrappingMode);
	}

	void OpenGLTextureResource::GenerateGraphicHandlers() {
		glGenTextures(1, &_texture);

		glBindTexture(GL_TEXTURE_2D, _texture);

		AssignWrappingMode(_wrappingMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

    vector<Color> OpenGLTextureResource::GetPixels() {
        auto count = _width * _height;
        auto pixels = new Color[count];
        glBindTexture(GL_TEXTURE_2D, _texture);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, pixels);
        glBindTexture(GL_TEXTURE_2D, 0);
        vector<Color> result(pixels, pixels + count);
        delete[] pixels;
        return result;
    }

	void OpenGLTextureResource::SetPixels(const vector<Color>& colors) {
		glBindTexture(GL_TEXTURE_2D, _texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_FLOAT, colors.data());
		glGenerateMipmap(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	OpenGLTextureResource::~OpenGLTextureResource() {
		glDeleteTextures(1, &_texture);
	}

	void OpenGLTextureResource::AssignWrappingMode(WrappingMode mode) {
		auto glMode = mode == WrappingMode::Repeat ? GL_REPEAT : GL_CLAMP;
		glBindTexture(GL_TEXTURE_2D, _texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glMode);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}