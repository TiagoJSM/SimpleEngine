#include "GraphicResourcesManager.h"

// Due to a linking error this has to be included here, unfortunatelly
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

namespace SimpleEngine{
	void GraphicResourcesManager::Initialize(IGraphicResourcesLoader* loader, IRenderer* renderer) {
		_loader = loader;
        _renderer = renderer;
		_meshDelete = [this](Mesh* mesh) { Remove(mesh, _meshes); };
		_textureDelete = [this](ITextureResource* tex) { Remove(tex, _textures); };
		_shaderDelete = [this](IShader* shader) { Remove<ShaderKey>(shader, _shaders); };
		_materialDelete = [this](Material* material) { Remove(material, _materials); };
	}

	MeshResource GraphicResourcesManager::GetMesh() {
		return MeshResource(_loader->GetMesh(), _meshDelete);
	}

    MeshResource GraphicResourcesManager::GetMesh(string& path) {
		std::function<Mesh*()> loadResource = [&]() { return _loader->GetMesh(); };

		bool isNew;
		auto mesh = GetResource<Mesh, std::string>(path, _meshes, loadResource, _meshDelete, &isNew);

		if (!isNew) {
			return mesh;
		}
		
		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		std::string err;
		bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str());

		if (!err.empty()) { // `err` may contain warning message.
			std::cerr << err << std::endl;
			return nullptr;
		}

		if (!ret) {
			return nullptr;
		}

		vector<glm::vec3> vertices;
		vector<Color> colors;
		vector<glm::vec2> uvs;
		vector<unsigned int> indices;

		// Loop over shapes
		for (size_t s = 0u; s < shapes.size(); s++) {
			// Loop over faces(polygon)
			size_t index_offset = 0u;
			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
				int fv = shapes[s].mesh.num_face_vertices[f];

				// Loop over vertices in the face.
				for (size_t v = 0u; v < fv; v++) {
					// access to vertex
					tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
					tinyobj::real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
					tinyobj::real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
					tinyobj::real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
					tinyobj::real_t nx = attrib.normals[3 * idx.normal_index + 0];
					tinyobj::real_t ny = attrib.normals[3 * idx.normal_index + 1];
					tinyobj::real_t nz = attrib.normals[3 * idx.normal_index + 2];
					tinyobj::real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
					tinyobj::real_t ty = 1.0f - attrib.texcoords[2 * idx.texcoord_index + 1];

					vertices.push_back(glm::vec3(vx, vy, vz));
					colors.push_back(Color());
					uvs.push_back(glm::vec2(tx, ty));
					indices.push_back(index_offset + v);
				}
				index_offset += fv;

				// per-face material
				shapes[s].mesh.material_ids[f];
			}
		}

		mesh->SetVertices(vertices);
		mesh->SetColors(colors);
		mesh->SetUVs(uvs);
		mesh->SetIndices(indices);
		mesh->ApplyDataToMesh();

		return mesh;
	}

	TextureResource GraphicResourcesManager::GetTextureResource(
			unsigned int width, unsigned int height) {
		auto texture = _loader->GetTextureResource(width, height);
		return TextureResource(texture);
	}

	TextureResource GraphicResourcesManager::GetTextureResource(
			string& name, unsigned int width, unsigned int height) {
		std::function<ITextureResource*()> loadResource = 
			[&]() { return _loader->GetTextureResource(width, height); };
		return GetResource(name, _textures, loadResource, _textureDelete);
	}

	MaterialResource GraphicResourcesManager::GetMaterial(string& name) {
		std::function<Material*()> loadResource = [&]() { 
			auto handler = _loader->GetMaterialHandler();
			return new OpenGLMaterial(handler);
		};
		return GetResource(name, _materials, loadResource, _materialDelete);
	}

    TextureResource GraphicResourcesManager::LoadTextureResource(
			string& path) {

		auto search = _textures.find(path);
		if (search != _textures.end()) {
			return search->second.lock();
		}

		int width, height, channels;
		auto ht_map = SOIL_load_image
		(
			path.c_str(),
			&width, &height, &channels,
			SOIL_LOAD_AUTO
		);

		auto colorCount = width * height;
		auto colorPtr = ht_map;
		vector<Color> colors;
		auto idx = 0;
		for (; idx < colorCount; idx++, colorPtr+= channels) {
            unsigned char alpha = channels == 4 ? colorPtr[3] : 255;
			colors.push_back(Color(colorPtr[0], colorPtr[1], colorPtr[2], alpha));
		}

		auto texture = GetTextureResource(path, width, height);
		texture->SetPixels(colors);

		//ToDo convert to colors
		SOIL_free_image_data(ht_map);
		return texture;
	}

	ShaderResource GraphicResourcesManager::GetShader(
			string& vertexShaderPath, string& fragmentShaderPath) {
		std::function<IShader*()> loadResource = [&]() { return _loader->GetShader(vertexShaderPath, fragmentShaderPath); };
		ShaderKey key(vertexShaderPath, fragmentShaderPath);
		return GetResource(key, _shaders, loadResource, _shaderDelete);
	}

    IRenderer* GraphicResourcesManager::GetRenderer() {
        return _renderer;
    }

	template <typename TKey, typename TResource>
	void GraphicResourcesManager::Remove(
			TResource* resource, 
			std::map<TKey, std::weak_ptr<TResource>>& container) {
		for (auto iter = container.begin(), endIter = container.end(); iter != endIter; ) {
			if (iter->second.expired()) {
				container.erase(iter++);
			}
			else {
				++iter;
			}
		}
		delete resource;
	}
}