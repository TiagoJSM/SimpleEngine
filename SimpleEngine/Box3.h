#pragma once

#include <glm/glm.hpp>
#include <algorithm>
#include "Ray.h"

using namespace std;

namespace SimpleEngine {
	class Box3 final {
	public:
		Box3() { }
		Box3(glm::vec3 vmin, glm::vec3 vmax)
		{
			bounds[0] = vmin;
			bounds[1] = vmax;
		}

		bool Intersect(const Ray& r) const
		{
			float tmin, tmax, tymin, tymax, tzmin, tzmax;
			auto rSign = r.Sign();
			auto invdir = glm::vec3(
				1 / r.direction.x, 
				1 / r.direction.y,
				1 / r.direction.z);

			tmin = (bounds[rSign.x].x - r.origin.x) * invdir.x;
			tmax = (bounds[1 - rSign.x].x - r.origin.x) * invdir.x;
			tymin = (bounds[rSign.y].y - r.origin.y) * invdir.y;
			tymax = (bounds[1 - rSign.y].y - r.origin.y) * invdir.y;

			if ((tmin > tymax) || (tymin > tmax))
				return false;
			if (tymin > tmin)
				tmin = tymin;
			if (tymax < tmax)
				tmax = tymax;

			tzmin = (bounds[rSign.z].z - r.origin.z) * invdir.z;
			tzmax = (bounds[1 - rSign.z].z - r.origin.z) * invdir.z;

			if ((tmin > tzmax) || (tzmin > tmax))
				return false;
			if (tzmin > tmin)
				tmin = tzmin;
			if (tzmax < tmax)
				tmax = tzmax;

			return true;
		}

		glm::vec3 GetPositiveVertex(const glm::vec3& normal) const {
			auto minimum = bounds[0];
			auto maximum = bounds[1];

			auto positiveVertex = minimum;

			if (normal.x >= 0.0f) positiveVertex.x = maximum.x;
			if (normal.y >= 0.0f) positiveVertex.y = maximum.y;
			if (normal.z >= 0.0f) positiveVertex.z = maximum.z;

			return positiveVertex;
		}

		glm::vec3 GetNegativeVertex(const glm::vec3& normal) const {
			auto minimum = bounds[0];
			auto maximum = bounds[1];

			auto negativeVertex = maximum;

			if (normal.x >= 0.0f) negativeVertex.x = minimum.x;
			if (normal.y >= 0.0f) negativeVertex.y = minimum.y;
			if (normal.z >= 0.0f) negativeVertex.z = minimum.z;

			return negativeVertex;
		}

		operator bool() const
		{
			return bounds[0] != bounds[1];
		}

		Box3 operator +(Box3 other) const {
			if (!this->operator bool()) {
				return other;
			}
			if (!other.operator bool()) {
				return Box3(bounds[0], bounds[1]);
			}
			auto vmin = bounds[0];
			auto otherVmin = other.bounds[0];

			auto min = glm::vec3(
				std::min(vmin.x, otherVmin.x),
				std::min(vmin.y, otherVmin.y),
				std::min(vmin.z, otherVmin.z)
			);

			auto vmax = bounds[1];
			auto otherVmax = other.bounds[1];

			auto max = glm::vec3(
				std::max(vmax.x, otherVmax.x),
				std::max(vmax.y, otherVmax.y),
				std::max(vmax.z, otherVmax.z)
			);

			return Box3(min, max);
		}

		void operator +=(Box3 other) {
			auto result = this->operator+(other);
			bounds[0] = result.bounds[0];
			bounds[1] = result.bounds[1];
		}

		glm::vec3 bounds[2];
	};

	
}