#pragma once

#include <glm\glm.hpp>

#include <string>
#include <map>

#include "ITextureResource.h"
#include "Typedefs.h"

namespace SimpleEngine {
	class IShader {
	public:
		virtual unsigned int GetVertexShader() = 0;
		virtual unsigned int GetFragmentShader() = 0;
		virtual ~IShader() = 0;
	};
}