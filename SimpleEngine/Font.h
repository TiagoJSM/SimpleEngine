#pragma once

#include <vector>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "Typedefs.h"

namespace SimpleEngine {
	struct FontCharacterInfo {
	public:
		TextureResource texture;
		FT_Int bitmapLeft;
		FT_Int bitmapTop;
		FT_Vector advance;

		operator bool() const
		{
			return texture.operator bool(); // Or false!
		}
	};
	class Font {
	public:
		Font(FontFaceResource fontFace, std::vector<FontCharacterInfo> charInfo);
		FontCharacterInfo GetFontCharacterInfo(char letter) const;
	private:
		FontFaceResource _fontFace;
		std::vector<FontCharacterInfo> _charInfo;
	};
}