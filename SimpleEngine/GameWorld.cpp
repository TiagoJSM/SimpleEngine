#include "GameWorld.h"
#include "ComponentManager.h"

namespace SimpleEngine {
	GameWorld::GameWorld(ComponentManager& componentManager) : _componentManager(componentManager) {}

	//https://stackoverflow.com/questions/5432739/am-i-using-the-copy-if-wrong
	//https://github.com/Microsoft/Range-V3-VS2015
    GameObject GameWorld::CreateObject() {
		unsigned int idx, entryId;
		if (GetFreeEntryIndex(&idx)) {
			auto& existing = _gameObjects[idx]; 
			entryId = existing.entryId + 1u;
			existing = { GameObjectData(this, &_componentManager, idx), entryId, true };
		}
		else {
			entryId = 0;
			idx = _gameObjects.size();
			GameObjectEntry entry { GameObjectData(this, &_componentManager, idx), entryId, true };
			_gameObjects.push_back(entry);
		}
        
        return GameObject(&_gameObjects, idx, entryId);
    }

	GameObject GameWorld::GetAt(unsigned int idx) {
		auto& entry = _gameObjects.at(idx);
		return GameObject(&_gameObjects, idx, entry.entryId);
	}

	GameObjectEntry* GameWorld::GetEntryAt(unsigned int idx) {
		return &_gameObjects.at(idx);
	}

	GameWorld::GameWorldIterator GameWorld::begin() {
        return GameWorld::GameWorldIterator(*this);
    }
	GameWorld::GameWorldIterator GameWorld::end() {
		return GameWorld::GameWorldIterator(*this, _gameObjects.size());
    }

	/*vector<GameObjectEntry*> GameWorld::TopLevelGameObjects() {
		vector<GameObjectEntry*> result;
		boost::copy(
			_gameObjects | boost::adaptors::filtered(IsTopLevelGameObject()),
			std::back_inserter(result));
		return result;
	}*/

    void GameWorld::CleanUp() {
		for (auto go : _gameObjects) {
			//ToDo: delete all components
			//delete go;
		}
        _gameObjects.clear();
    }

	bool GameWorld::GetFreeEntryIndex(unsigned int* index) {
		for (auto idx = 0u; idx < _gameObjects.size(); idx++) {
			if (!_gameObjects[idx].valid) {
				*index = idx;
				return true;
			}
		}
		return false;
	}

	GameWorld::GameWorldIterator::GameWorldIterator(GameWorld& world, unsigned int idx) :_world(world), _idx(idx) {}
	
	GameObject GameWorld::GameWorldIterator::operator*() const {
		auto entry = _world._gameObjects[_idx];
		return GameObject(&_world._gameObjects, _idx, entry.entryId);
	}

	// Pre- and post-incrementable.
	GameWorld::GameWorldIterator& GameWorld::GameWorldIterator::operator++() {
		while (_idx < _world._gameObjects.size()) {
			auto validGO = _world._gameObjects[_idx].valid;
			_idx++;
			if (validGO) {
				break;
			}
		}
		return *this;
	}

	/*GameWorld::GameWorldIterator& GameWorld::GameWorldIterator::operator++(int increment) {
		while (increment > 0) {
			this->operator++();
		}
		return *this;
	}*/

	// Equality / inequality.
	bool GameWorld::GameWorldIterator::operator==(const GameWorldIterator& rhs) {
		return _idx == rhs._idx;
	}

	bool GameWorld::GameWorldIterator::operator!=(const GameWorldIterator& rhs) {
		return _idx != rhs._idx;
	}
}