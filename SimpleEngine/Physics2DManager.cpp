#include "Physics2DManager.h"

namespace SimpleEngine {
	b2Vec2 Physics2DManager::DefaultGravity = b2Vec2(0.0f, -10.0f);

	Physics2DManager::Physics2DManager() :_world(Physics2DManager::DefaultGravity) {

	}

	void Physics2DManager::Initialize(
			float step, 
			int velocityIterations, 
			int positionIterations,
			b2Vec2 gravity) {
		_step = step;
		_velocityIterations = velocityIterations;
		_positionIterations = positionIterations;
        _world.SetGravity(gravity);
        _world.SetContactListener(&_contactListener);
	}

	void Physics2DManager::Update() {
		_world.Step(_step, _velocityIterations, _positionIterations);
	}

	b2Body* Physics2DManager::CreateBody(glm::vec2 position) {
		b2BodyDef bodyDef;
		bodyDef.position.Set(position.x, position.y);
		return _world.CreateBody(&bodyDef);
	}
}