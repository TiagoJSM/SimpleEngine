#include "Text.h"

namespace SimpleEngine {
	namespace UI {
		void Text::Initialize() {
			string fontName = "Fonts/arial_narrow_7.ttf";
			font = FontResourcesManager::Instance()->GetFont(fontName);
			text = "a";
		}

		void Text::Render(glm::mat4 viewProjection, IRenderer& renderer) {
			renderer.DrawText(viewProjection, text, DefaultGraphicObjects::UiDefaultMaterial, font);
		}
	}
}