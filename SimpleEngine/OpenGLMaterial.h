#pragma once

#include <GL\glew.h>
#include "Material.h"
#include "Logger.h"

using boost::any_cast;

namespace SimpleEngine {
	#define TEXTURE_POSITIONS 8
	class OpenGLMaterial : public Material {
	public:
		OpenGLMaterial(boost::any materialHandler);

		void SetMatrix(const std::string& name, glm::mat4 matrix) override;
		void SetTextures(const map<std::string, TextureResource>& textures) override;
		void SetVec3(const std::string& name, glm::vec3 vec) override;
		void SetVec2(const std::string& name, glm::vec2 vec) override;
		void SetInt(const std::string& name, int value) override;

		~OpenGLMaterial();
	protected:
		void OnSetShader(ShaderResource oldShader, ShaderResource newShader) override;
	private:
		const static unsigned int texturePositions[TEXTURE_POSITIONS];

		void PrintShaderLinkingError(GLuint shaderProgram);
	};
}