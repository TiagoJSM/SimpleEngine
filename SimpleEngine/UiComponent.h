#pragma once

#include "Component.h"
#include "UiTransform.h"
#include "Camera.h"

namespace SimpleEngine {
	namespace UI {
		class UiTransform;
		class UiComponent : public Component {
		public:
			ComponentHandle<UiTransform> GetUiTransform();
			void Render(IRenderer& renderer);
		protected:
			virtual void Render(glm::mat4 viewProjection, IRenderer& renderer);
		private:
			ComponentHandle<UiTransform> _uiTransform;
		};
	}
}