#pragma once

#include <GL/glew.h>

#include <vector>

#include "ITextureResource.h"
#include "OpenGLGraphicsResourceLoader.h"
#include "Color.h"
#include "OpenGLShader.h"

using namespace std;

namespace SimpleEngine {
	class OpenGLTextureResource : public ITextureResource {
	public:
		OpenGLTextureResource(unsigned int width, unsigned int height);
		unsigned int GetWidth() override;
		unsigned int GetHeight() override;
		unsigned int GetHandler() override;
		glm::vec2 GetOffset() override;
		void SetOffset(glm::vec2 offset) override;

		WrappingMode GetWrappingMode() override;
		void SetWrappingMode(WrappingMode mode) override;

		void GenerateGraphicHandlers() override;

        vector<Color> GetPixels() override;
		void SetPixels(const vector<Color>& colors) override;

		~OpenGLTextureResource() override;
	private:
		GLuint _texture;
		WrappingMode _wrappingMode;
		glm::vec2 _offset;

		unsigned int _width, _height;

		void AssignWrappingMode(WrappingMode mode);
	};
}