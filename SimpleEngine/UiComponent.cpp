#include "UiComponent.h"

namespace SimpleEngine {
	namespace UI {
		ComponentHandle<UiTransform> UiComponent::GetUiTransform() {
			if (_uiTransform) {
				return _uiTransform;
			}
			_uiTransform = gameObject->GetComponent<UiTransform>();
			return _uiTransform;
		}

		void UiComponent::Render(IRenderer& renderer) {
			auto viewProj = this->GetUiTransform()->GetCanvas()->ViewProjectionMatrix();
			Render(viewProj, renderer);
		}

		void UiComponent::Render(glm::mat4 viewProjection, IRenderer& renderer) { }
	}
}