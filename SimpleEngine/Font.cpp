#include "Font.h"

namespace SimpleEngine {
	Font::Font(FontFaceResource fontFace, std::vector<FontCharacterInfo> charInfo)
		: _fontFace(fontFace), _charInfo(charInfo) {}

	FontCharacterInfo Font::GetFontCharacterInfo(char letter) const {
		auto idx = letter - 'a';
		if (idx < 0 || idx > _charInfo.size()) {
			return FontCharacterInfo();
		}
		return _charInfo[idx];
	}
}