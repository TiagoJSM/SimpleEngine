#pragma once

#include "IRenderer.h"
#include "IGraphicResourcesLoader.h"
#include "IMeshResource.h"
#include "IShader.h"
#include "ITextureResource.h"
#include "Mesh.h"
#include "Material.h"
#include "Color.h"

#include "OpenGLTextureResource.h"
#include "OpenGLShader.h"
#include "OpenGLRenderer.h"
#include "OpenGLMeshResource.h"
#include "OpenGLGraphicsResourceLoader.h"
#include "OpenGLLine.h"
#include "RenderData.h"