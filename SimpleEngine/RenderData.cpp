#include "RenderData.h"

namespace SimpleEngine {
	void RenderData::SetVector4(std::string name, const glm::vec4 vec) {
		SetData(vec4s, name, vec);
	}

	void RenderData::SetFloat(std::string name, const float value) {
		SetData(floats, name, value);
	}
	void RenderData::SetTexture(std::string name, const TextureResource value) {
		SetData(textures, name, value);
	}
	bool RenderData::TryGetVector4(std::string name, glm::vec4& data) {
		return TryGetData(vec4s, name, data);
	}
	bool RenderData::TryGetFloat(std::string name, float& data) {
		return TryGetData(floats, name, data);
	}

	template<typename TData>
	void RenderData::SetData(std::vector<std::tuple<std::string, TData>>& collection, std::string name, const TData value) {
		auto it = std::find_if(
			collection.begin(), 
			collection.end(), 
			[name](const std::tuple<std::string, TData>& v) { return name == std::get<0>(v); });

		if (it != collection.end()) {
			auto& tupple = (*it);
			get<1>(tupple) = value;
		}
		else {
			collection.push_back(std::tuple<string, TData>(name, value));
		}
		
	}
	template<typename TData>
	bool RenderData::TryGetData(std::vector<std::tuple<std::string, TData>>& collection, std::string name, TData& data) {
		auto it = std::find_if(
			collection.begin(),
			collection.end(),
			[name](const std::tuple<std::string, TData>& v) { return name == std::get<0>(v); });

		if (it == collection.end()) {
			return false;
		}

		auto& tupple = (*it);
		data = get<1>(tupple);
		return true;
	}
}