#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

#include "MathUtils.h"
//#include "Component.h"

namespace SimpleEngine {
	class TransformComponent /*: public Component*/ {
	public:
		glm::vec3 translation;
		glm::vec3 rotation;
		glm::vec3 scale;

		TransformComponent();
		TransformComponent(const TransformComponent& other) = delete; // non construction-copyable
		TransformComponent& operator=(const TransformComponent&) = delete; // non copyable
		glm::mat4 TransformMatrix() const;
	};
}