#pragma once

#include <glm\glm.hpp>

namespace SimpleEngine {
	struct Line2D {
	public:
		glm::vec2 p1;
		glm::vec2 p2;
	};
}