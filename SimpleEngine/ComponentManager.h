#pragma once

#include <map>
#include <typeindex>

#include "TemplateComponentPool.h"

using namespace std;

namespace SimpleEngine {
	class ComponentManager {
	public:
		template<typename TComponent, typename = EnableIfBaseOfComponent<TComponent>>
		ComponentHandle<TComponent> CreateComponent() {
			//workaround, since can't do typeid(TComponent)
			TComponent tmp;
			auto& type = typeid(tmp);
			auto it = _pools.find(type);
			IComponentPool* pool;
			if (it == _pools.end()) {
				pool = new TemplateComponentPool<TComponent>();
				_pools[type] = pool;
			}
			else {
				pool = it->second;
			}
			return pool->CreateComponent().Cast<TComponent>();
		}
	private:
		map<type_index, IComponentPool*> _pools;
	};

}