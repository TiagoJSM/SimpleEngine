#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

#include "MathUtils.h"
#include "Screen.h"
#include "Ray.h"

namespace SimpleEngine {
	enum View {
		Perspective,
		Ortho
	};
	class Camera {
	public:
		glm::vec3 translation;
		glm::vec3 rotation;
		glm::vec3 scale;
		View view;

		static Camera* currentCamera;

		float fieldOfView, aspectRatio, nearClipping, farClipping;
		float left, right, bottom, top;

		Camera();

		glm::mat4 ViewMatrix() const;
		glm::mat4 ProjectionMatrix() const;

		glm::vec2 WorldToScreen(glm::vec3 position) const;
        glm::vec3 ScreenToWorld(glm::vec2 screenPosition, float z) const;
		static glm::vec3 ScreenToWorld(glm::vec2 screenPosition, float z, glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
		Ray ScreenPointToRay(glm::vec2 position) const;
		static Ray ScreenPointToRay(glm::vec2 position, glm::vec3 translation, glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
	};
}