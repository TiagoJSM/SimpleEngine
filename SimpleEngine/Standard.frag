 
in vec4 ex_Color;
in vec2 texCoord;

uniform sampler2D Texture;
 
void main(void) {
	gl_FragColor = texture(Texture, texCoord);
}