#pragma once

#include <vector>
#include <functional>

#include "BaseIncludes.h"

using namespace std;

namespace SimpleEngine {
	template<typename TComponent>
	using EnableIfBaseOfComponent = std::enable_if_t<std::is_base_of<Component, TComponent>::value>;

	struct ComponentContainer {
		Component* data;
		unsigned int entryId;
	};

	template<typename TComponent, typename = EnableIfBaseOfComponent<TComponent>>
	struct TComponentContainer {
		TComponent data;
		unsigned int entryId;
		bool valid;
	};

	using ComponentCollection = vector<ComponentContainer>;

	template<typename TComponent, typename = EnableIfBaseOfComponent<TComponent>>
	using TComponentCollection = vector<TComponentContainer<TComponent>>;

	template<typename TComponent>
	class ComponentHandle {
	public:
		ComponentHandle(function<Component*(unsigned int idx, unsigned int entryId)> getter = nullptr, unsigned int idx = 0, unsigned int entryId = 0)
				: _getter(getter), _idx(idx), _entryId(entryId) {
		}

		unsigned int Index() const {
			return _idx;
		}
		TComponent* operator->() const {
			return this->operator bool() ? dynamic_cast<TComponent*>(_getter(_idx, _entryId)) : nullptr;
		}
		operator bool() const {
			return _getter != nullptr && _getter(_idx, _entryId) != nullptr;
		}
		bool operator==(const ComponentHandle<TComponent>& other) const {
			return _idx == other._idx && _entryId == other._entryId && _getter == other._getter;
		}
		bool operator==(const TComponent* other) const {
			auto valid = this->operator bool();
			if (!valid && other == nullptr) {
				return true;
			}

			if (!valid || other == nullptr) {
				return false;
			}

			return _getter(_idx, _entryId) == other;
		}
		bool operator!=(const TComponent* other) const {
			return !this->operator==(other);
		}
		void operator=(const ComponentHandle<TComponent>* other) {
			if (other == nullptr) {
				_getter = nullptr;
			}
			else {
				_getter = other->_getter;
				_idx = other->_idx;
				_entryId = other->_entryId;
			}
		}
		void operator=(const ComponentHandle<TComponent>& other) {
			_getter = other._getter;
			_idx = other._idx;
			_entryId = other._entryId;
		}
		template<typename TCast>
		ComponentHandle<TCast> Cast() {
			return ComponentHandle<TCast>(_getter, _idx, _entryId);
		}
		template<typename TCast>
		ComponentHandle<TCast> SafeCast() {
			if (this->operator bool()) {
				auto comp = dynamic_cast<TCast*>(this->operator->());
				return comp == nullptr ? ComponentHandle<TCast>() : ComponentHandle<TCast>(_getter, _idx, _entryId);
			}
			return ComponentHandle<TCast>();
		}
	private:
		function<Component*(unsigned int idx, unsigned int entryId)> _getter;
		unsigned int _idx;
		unsigned int _entryId;
	};
}