#pragma once

#include <string>
#include <map>

#include <boost/any.hpp>

#include "IShader.h"
#include "Typedefs.h"

namespace SimpleEngine {
	class Material {
	public:
		unsigned int renderQueue;

		Material(boost::any materialHandler);
		boost::any GetHandler() const;
		ShaderResource GetShader() const;
		void SetShader(ShaderResource shader);

		virtual void SetMatrix(const std::string& name, glm::mat4 matrix) = 0;
		virtual void SetTextures(const map<std::string, TextureResource>& textures) = 0;
		virtual void SetVec3(const std::string& name, glm::vec3 vec) = 0;
		virtual void SetVec2(const std::string& name, glm::vec2 vec) = 0;
		virtual void SetInt(const std::string& name, int value) = 0;

		const std::map<std::string, TextureResource>& GetTextures() const;

		virtual ~Material();
	protected:
		virtual void OnSetShader(ShaderResource oldShader, ShaderResource newShader) = 0;

		std::map<std::string, glm::mat4> _matrices;
		std::map<std::string, glm::vec3> _vec3s;
		std::map<std::string, glm::vec2> _vec2s;
		std::map<std::string, TextureResource> _textures;
		std::map<std::string, int> _integers;

		template<typename TData>
		void SetParameter(
			const std::string& name, TData data, std::map<std::string, TData>& storage) {

			auto result = storage.insert(
				std::pair<std::string, TData>(name, data));

			if (!result.second) {
				storage[name] = data;
			}
		}

		template<typename TData>
		void RemoveParameter(
			const std::string& name, std::map<std::string, TData>& storage) {

			storage.erase(name);
		}
	private:

		void ApplyMatrices();
		void ApplyVec3s();
		void ApplyVec2s();
		void ApplyTextures();
		void CleanUp();

		boost::any _materialHandler;
		ShaderResource _shader;
	};
}