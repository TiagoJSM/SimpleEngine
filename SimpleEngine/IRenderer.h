#pragma once

#include <glm/glm.hpp>
#include "IMeshResource.h"
#include "IShader.h"
#include "Rect.h"
#include "RenderData.h"
#include "Frustum.h"
#include "Typedefs.h"

namespace SimpleEngine {
	class IGraphicResourcesLoader;
	class IRenderer {
	public:
		virtual bool OnInit(IGraphicResourcesLoader* loader) = 0;
		virtual void PreRender(glm::mat4 viewProjection) = 0;
		virtual void PostRender() = 0;
		virtual void Submit() = 0;
		virtual void CleanUp() = 0;

		virtual void Render(MeshResource mesh, MaterialResource material, glm::mat4 model, RenderData* renderData = nullptr) = 0;
		virtual void SubmitLine(glm::vec3 start, glm::vec3 end) = 0;
		virtual void DrawQuad(glm::mat4 viewProjection, Rect quad, MaterialResource material, RenderData* renderData = nullptr) = 0;
		virtual void DrawText(glm::mat4 viewProjection, string& text, MaterialResource material, FontResource font) = 0;
		virtual Frustum GetFrustum(glm::mat4 viewProjection) = 0;
        virtual glm::ivec2 GetWindowSize() = 0;
	};
}