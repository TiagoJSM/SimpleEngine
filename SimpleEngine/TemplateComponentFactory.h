#pragma once

#include <vector>
#include "IComponentFactory.h"

using namespace std;

namespace SimpleEngine {
	template<typename TComponent, typename = EnableIfBaseOfComponent<TComponent>>
	class TemplateComponentFactory : public IComponentFactory {
	public:
		ComponentHandle<Component> CreateComponent() override {
			auto func = std::bind(&TemplateComponentFactory<TComponent>::GetComponent, this, std::placeholders::_1, std::placeholders::_2);
			unsigned int idx;
			if (GetFreeEntryIndex(idx)) {
				TComponentContainer<TComponent> container = { TComponent(), ++_components[idx].entryId, true };
				_components[idx] = container;
			}
			else {
				idx = _components.size();
				TComponentContainer<TComponent> container = { TComponent(), 0, true };
				_components.push_back(container);
			}
			
			return ComponentHandle<Component>(func, idx, 0);
		}
	private:
		TComponentCollection<TComponent> _components;

		Component* GetComponent(unsigned int idx, unsigned int entryId) {
			auto& entry = _components.at(idx);
			if (entry.valid && entry.entryId == entryId) {
				return &entry.data;
			}
			return nullptr;
		}

		bool GetFreeEntryIndex(unsigned int& index) {
			for (auto idx = 0u; idx < _components.size(); idx++) {
				if (!_components[idx].valid) {
					index = idx;
					return true;
				}
			}
			return false;
		}
	};
}