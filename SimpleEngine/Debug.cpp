#include "Debug.h"

namespace SimpleEngine {
	IRenderer* Debug::_renderer;
	std::vector<std::function<void()>> Debug::_renderCalls;

	void Debug::Initialize(IRenderer* renderer) {
		_renderer = renderer;
	}

	void Debug::DrawLine(glm::vec3 start, glm::vec3 end) {
		std::function<void()> func = [=]() {
			_renderer->SubmitLine(start, end);
		};
		_renderCalls.push_back(func);
	}

	void Debug::DrawBox3(Box3 box) {
		std::function<void()> func = [=]() {
			auto p0 = box.bounds[0];
			auto p7 = box.bounds[1];
			auto p1 = glm::vec3(p0.x, p7.y, p0.z);
			auto p2 = glm::vec3(p7.x, p0.y, p0.z);
			auto p3 = glm::vec3(p7.x, p7.y, p0.z);
			auto p4 = glm::vec3(p0.x, p7.y, p7.z);
			auto p5 = glm::vec3(p7.x, p0.y, p7.z);
			auto p6 = glm::vec3(p0.x, p0.y, p7.z);

			_renderer->SubmitLine(p0, p2);
			_renderer->SubmitLine(p2, p3);
			_renderer->SubmitLine(p3, p1);
			_renderer->SubmitLine(p1, p0);

			_renderer->SubmitLine(p6, p5);
			_renderer->SubmitLine(p5, p7);
			_renderer->SubmitLine(p7, p4);
			_renderer->SubmitLine(p4, p6);

			_renderer->SubmitLine(p0, p6);
			_renderer->SubmitLine(p1, p4);

			_renderer->SubmitLine(p2, p5);
			_renderer->SubmitLine(p3, p7);
		};

		_renderCalls.push_back(func);
	}

	void Debug::Submit() {
		for (auto call : _renderCalls) {
			call();
		}
		_renderCalls.clear();
	}

	void Debug::Destroy() {
	}
}