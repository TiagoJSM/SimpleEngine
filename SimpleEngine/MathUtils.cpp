#include "MathUtils.h"

#include <algorithm>

namespace SimpleEngine {
	const float MathUtils::PI = 3.14159265358979323846f;
	const float MathUtils::Rad2Deg = 360.f / (PI * 2.f);
	const float MathUtils::Deg2Rad = (PI * 2.f) / 360.f;

	glm::mat4 MathUtils::CreateTransformationMatrix(
			glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale) {
		glm::mat4 orientation;
		orientation = glm::rotate(orientation, rotation.x * Deg2Rad, glm::vec3(1, 0, 0));
		orientation = glm::rotate(orientation, rotation.y * Deg2Rad, glm::vec3(0, 1, 0));
		orientation = glm::rotate(orientation, rotation.z * Deg2Rad, glm::vec3(0, 0, 1));

		return glm::translate(translation) * orientation * glm::scale(scale);
	}

	float MathUtils::Angle(glm::vec2 v1, glm::vec2 v2) {
		auto dot = glm::dot(glm::normalize(v1), glm::normalize(v2));
		return acosf(dot) * 180.0f / PI;
	}

	unsigned int MathUtils::Random(unsigned int min, unsigned int max) {
		std::random_device rd;
		std::default_random_engine generator(rd());
		std::uniform_int_distribution<unsigned int> distribution(min, max);
		return distribution(generator);
	}

	int MathUtils::NextPowerOf2(int value) {
		int rval = 1;
		// rval<<=1 Is A Prettier Way Of Writing rval*=2;
		while (rval<value) rval <<= 1;
		return rval;
	}

	Sphere MathUtils::CalculateSphere(std::vector<glm::vec3>& points) {
		glm::vec3 sum;

		for (auto point : points) {
			sum += point;
		}

		auto size = points.size();
		auto center = glm::vec3(sum.x / size, sum.y / size, sum.z / size);

		auto maxDistance = 0.f;
		for (auto point : points) {
			auto distance = glm::distance(point, center);

			if (distance > maxDistance) {
				maxDistance = distance;
			}
		}
		return { center, maxDistance };
	}

	Box3 MathUtils::CalculateBox(std::vector<glm::vec3>& points) {
		glm::vec3 min, max;

		for (auto point : points) {
			if (point.x < min.x) {
				min.x = point.x;
			}
			if (point.y < min.y) {
				min.y = point.y;
			}
			if (point.z < min.z) {
				min.z = point.z;
			}

			if (point.x > max.x) {
				max.x = point.x;
			}
			if (point.y > max.y) {
				max.y = point.y;
			}
			if (point.z > max.z) {
				max.z = point.z;
			}
		}

		return Box3(min, max);
	}

	float MathUtils::Clamp01(float value) {
		return std::max(0.f, std::min(value, 1.f));
	}

	float MathUtils::Lerp(float a, float b, float t) {
		t = Clamp01(t);
		return (1 - t)*a + t*b;
	}
}