#pragma once

#include <vector>
#include <functional>
#include <algorithm>
#include <utility>

namespace SimpleEngine {
	template<typename TData>
	class Event {
	public:
		template<typename TData>
		class EventConnection {
		public:
			EventConnection()
				:_evt(nullptr), _idx(-1) { }
			EventConnection(Event<TData>* evt, int idx)
				:_evt(evt), _idx(idx) { }

			EventConnection& operator=(EventConnection&& conn) {
				std::swap(_evt, conn._evt);
				std::swap(_idx, conn._idx);
				return *this;
			}
			EventConnection(const EventConnection&) = default;
			EventConnection(EventConnection&&) = default;
			EventConnection& operator=(const EventConnection&) = default;
			//EventConnection& operator=(EventConnection&&) = default;

			void Disconnect() {
				if (_evt != nullptr) {
					_evt->Disconnect(_idx);
					_evt = nullptr;
				}
			}

			~EventConnection() {
				Disconnect();
			}
		private:
			Event<TData>* _evt;
			int _idx;
		};

		//Event() {}
		//Event(const Event&) = default;
		//Event(Event&&) = default;
		//Event& operator=(const Event&) = default;
		//Event& operator=(Event&&) = default;

		EventConnection<TData> Subscribe(std::function<void(const TData&)> func) {
			auto iter =
				std::find_if(
					_callbacks.begin(),
					_callbacks.end(),
					[this](std::function<void(const TData&)> f) -> bool { return !f; });

			int idx;
			if (iter == _callbacks.end()) {
				idx = _callbacks.size();
				_callbacks.push_back(func);
			}
			else {
				idx = std::distance(_callbacks.begin(), iter);
				_callbacks[idx] = func;
			}

			return EventConnection<TData>(this, idx);
		}

		bool Empty() {
			if (_callbacks.size() == 0) {
				return true;
			}

			for (auto& callback : _callbacks) {
				if (callback) {
					return false;
				}
			}

			return true;
		}

		void Trigger(const TData& data) {
			for (auto& callback : _callbacks) {
				if (callback) {
					callback(data);
				}
			}
		}

		~Event() {
			
		}

		private:
			std::vector<std::function<void(const TData&)>> _callbacks;

			void Disconnect(int idx) {
				_callbacks[idx] = nullptr;
			}
	};
}