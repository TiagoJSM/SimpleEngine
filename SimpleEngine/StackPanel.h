#pragma once

#include "UiComponent.h"

namespace SimpleEngine {
	namespace UI {
		enum class Direction {
			Horizontal,
			Vertical
		};

		class StackPanel : public UiComponent {
		public:
			bool expandWidth;
			bool expandHeight;
			Direction direction;

			StackPanel();
			void Update(Timer timer) override;
		private:
			void RearrangeLayout();
			void ArrangeHorizontalLayout(vector<ComponentHandle<UiTransform>>& elements);
			void ArrangeVerticalLayout(vector<ComponentHandle<UiTransform>>& elements);
		};
	}
}