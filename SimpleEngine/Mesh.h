#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "Color.h"
#include "IMeshResource.h"
#include "IRenderer.h"
#include "Material.h"
#include "Sphere.h"
#include "Box3.h"
#include "MathUtils.h"

using namespace std;

namespace SimpleEngine {
	class IRenderer;
	class Mesh {
	public:
		Mesh(IMeshResource* meshResource);
		void ApplyDataToMesh();
		void SetVertices(vector<glm::vec3>& vertices);
		const vector<glm::vec3>& GetVertices();
		void SetColors(vector<Color>& colors);
		const vector<Color>& GetColors();
		void SetUVs(vector<glm::vec2>& uvs);
		const vector<glm::vec2>& GetUVs();
		void SetIndices(vector<unsigned int>& indices);
		const vector<unsigned int>& GetIndices();
		Sphere GetBoundingSphere() const;
		Box3 GetBoundingBox() const;
		IMeshResource* GetMeshResource() const;

		~Mesh();

	private:
		vector<glm::vec3> _vertices;
		vector<Color> _colors;
		vector<glm::vec2> _uvs;
		vector<unsigned int> _indices;
		IMeshResource* _meshResource;
		Sphere _sphere;
		Box3 _box;
	};
}