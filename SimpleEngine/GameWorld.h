#pragma once

#include <algorithm>

#include "GameObject.h"
#include "Handle.h"

using namespace std;

namespace SimpleEngine {

	/*struct GameObjectEntry {
		GameObjectData data;
		unsigned int entryId;
		bool valid;
	};*/

	//using GameObjectEntry = DataContainer<GameObjectData>; // <GameObjectData>;

	class ComponentManager;

    class GameWorld {
    public:
		class GameWorldIterator;

        //typedef GameWorld::GameWorldIterator iterator;
		GameWorld(ComponentManager& componentManager);

		class GameWorldIterator {
		public:
			GameWorldIterator() = default;
			~GameWorldIterator() { }

			GameWorldIterator(GameWorld& world, unsigned int idx = 0);
			GameObject operator*() const;

			// Pre- and post-incrementable.
			GameWorldIterator& operator++();
			//GameWorldIterator& operator++(int increment);

			// Equality / inequality.
			bool operator==(const GameWorldIterator& rhs);
			bool operator!=(const GameWorldIterator& rhs);
		private:
			GameWorld& _world;
			unsigned int _idx;
		};

        GameObject CreateObject();
		GameObject GetAt(unsigned int idx);
		GameObjectEntry* GetEntryAt(unsigned int idx);

		GameWorldIterator begin();
		GameWorldIterator end();

        void CleanUp();
    private:
        std::vector<GameObjectEntry> _gameObjects;
		ComponentManager& _componentManager;

		bool GetFreeEntryIndex(unsigned int* index);
    };
}