#pragma once

#include "BaseIncludes.h"
#include "ComponentHandle.h"

using namespace std;

namespace SimpleEngine {
	class IComponentPool {
	public:
		virtual ComponentHandle<Component> CreateComponent() = 0;
	};
}