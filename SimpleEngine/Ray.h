#pragma once

#include <iostream>
#include <glm/glm.hpp>

namespace SimpleEngine {
	struct Ray {
		glm::vec3 origin;
		glm::vec3 direction;

		glm::ivec3 Sign() const {
			glm::ivec3 result;
			auto invdir = glm::vec3(1 / direction.x, 1 / direction.y, 1 / direction.z);
			result.x = (invdir.x < 0);
			result.y = (invdir.y < 0);
			result.z = (invdir.z < 0);
			return result;
		}
	};

	inline std::ostream &operator<<(std::ostream &os, Ray const &ray) {
		auto origin = ray.origin;
		auto direction = ray.direction;
		return os 
			<< "{ origin: (" 
				<< origin.x << " " << origin.y << " " << origin.z << "), "
			<< "direction: (" 
				<< direction.x << " " << direction.y << " " << direction.z << ") }";
	}
}