#include "OpenGLQuad.h"

namespace SimpleEngine {
	OpenGLQuad::OpenGLQuad() {}

	void OpenGLQuad::Render() {
		glBindVertexArray(_vao);
		glDrawElements(
			GL_QUADS,			// mode
			4,					// count
			GL_UNSIGNED_INT,	// type
			(void*)0);			// element array buffer offset
	}

	OpenGLQuad OpenGLQuad::BuildGLQuad() {
		const auto PositionAttributeIndex = 0;
		const auto UvAttributeIndex = 2;

		glm::vec3 points[] = 
			{ 
				glm::vec3(0, 0, 0), 
				glm::vec3(0, 1, 0), 
				glm::vec3(1, 1, 0), 
				glm::vec3(1, 0, 0)
			};
		glm::vec2 uvs[] =
		{
			glm::vec2(0, 1),
			glm::vec2(0, 0),
			glm::vec2(1, 0),
			glm::vec2(1, 1)
		};

		GLuint vao, vertexIndex, uvIndex, indicesBuffer = 0;
		GLuint vbos[3];
		glGenBuffers(3, vbos);
		vertexIndex = vbos[0];
		uvIndex = vbos[1];
		indicesBuffer = vbos[2];

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vertexIndex);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, &points, GL_STATIC_DRAW);
		glVertexAttribPointer(PositionAttributeIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(PositionAttributeIndex);

		glBindBuffer(GL_ARRAY_BUFFER, uvIndex);
		glBufferData(GL_ARRAY_BUFFER, (4 * sizeof(glm::vec2)), uvs, GL_STATIC_DRAW);
		glVertexAttribPointer(UvAttributeIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(UvAttributeIndex);

		unsigned int indices[] = { 0, 1, 2, 3 };
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(unsigned int), indices, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return OpenGLQuad(vao, vertexIndex, uvIndex, indicesBuffer);
	}

	OpenGLQuad::OpenGLQuad(GLuint vao, GLuint vertexIndex, GLuint uvIndex, GLuint indicesBuffer) {
		_vao = vao;
		_vertexIndex = vertexIndex;
		_uvIndex = uvIndex;
		_indicesBuffer = indicesBuffer;
	}
}