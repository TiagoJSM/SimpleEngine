#include "Body2D.h"

namespace SimpleEngine {
	void Body2D::Initialize() {
		auto position = gameObject->transform.translation;
		_body = Physics2DManager::Instance()->CreateBody(glm::vec2(position.x, position.y));
		_body->SetType(b2_dynamicBody);
        _body->SetUserData(this);
	}

	void Body2D::PhysicsUpdate(Timer timer) {
		auto position = gameObject->transform.translation;
		auto bodyPosition = _body->GetPosition();
		position.x = bodyPosition.x;
		position.y = bodyPosition.y;
		gameObject->transform.translation = position;
	}

    glm::vec2 Body2D::GetPosition() {
        auto position = _body->GetPosition();
        return glm::vec2(position.x, position.y);
    }

    void Body2D::SetPosition(glm::vec2 position) {
        gameObject->transform.translation = glm::vec3(position.x, position.y, 0);
        _body->SetTransform(b2Vec2(position.x, position.y), _body->GetAngle());
    }

    glm::vec2 Body2D::GetVelocity() {
        auto velocity = _body->GetLinearVelocity();
        return glm::vec2(velocity.x, velocity.y);
    }

    void Body2D::SetVelocity(glm::vec2 velocity) {
        _body->SetLinearVelocity(b2Vec2(velocity.x, velocity.y));
    }

    bool Body2D::IsActive() {
        return _body->IsActive();
    }

    void Body2D::SetActive(bool active) {
        _body->SetActive(active);
    }

    b2Body* Body2D::GetPhysicsEngineBody() {
        return _body;
    }

    float Body2D::GetMass() {
        return _body->GetMass();
    }

    void Body2D::SetMass(float mass) {
        b2MassData massData;
        _body->GetMassData(&massData);
        massData.mass = mass;
        _body->SetMassData(&massData);
    }

    float Body2D::GetGravityScale() {
        return _body->GetGravityScale();
    }

    void Body2D::SetGravityScale(float scale) {
        _body->SetGravityScale(scale);
    }

    void Body2D::ApplyForce(glm::vec2 force) {
        _body->ApplyForceToCenter(b2Vec2(force.x, force.y), true);
    }

    void Body2D::ApplyImpulse(glm::vec2 force) {
        _body->ApplyLinearImpulseToCenter(b2Vec2(force.x, force.y), true);
    }

    void Body2D::RaiseOnColliderEnter(Collider2D* collider) {
        onColliderEnter2D(collider);
    }
    void Body2D::RaiseOnColliderExit(Collider2D* collider) {
        onColliderExit2D(collider);
    }

	Body2D::~Body2D() {
		auto world = _body->GetWorld();
		world->DestroyBody(_body);
		_body = nullptr;
	}
}