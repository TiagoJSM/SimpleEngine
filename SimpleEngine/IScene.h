#pragma once

#include "GameWorld.h"

namespace SimpleEngine {
	class IScene {
	public:
		virtual void BuildScene(GameWorld& world) = 0;
	};
}