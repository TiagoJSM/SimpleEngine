#include "Button.h"

namespace SimpleEngine {
	namespace UI {
		void Button::Initialize() {
			_image = gameObject->GetComponent<Image>();
		}
		void Button::Update(Timer timer) {
			_timeSinceLastUpdate = timer.time;
			if (_inTransition) {
				if (colorTransitions.transitionTime == 0) {
					_image->renderData.SetVector4("_color", _targetColor);
					_image->renderData.SetFloat("colorMultiplier", _targetHighlight);
					_inTransition = false;
				}
				else {
					auto t = (timer.time - _startTransitionTime) / colorTransitions.transitionTime;
					_image->renderData.SetVector4("_color", Color::Lerp(_originalColor, _targetColor, t));
					_image->renderData.SetFloat("colorMultiplier", MathUtils::Lerp(_originalHighlight, _targetHighlight, t));

					_inTransition = t < 1;
				}
			}
		}
		void Button::Click() {
			onClick.Trigger(0);
		}
		void Button::Enter() {
			StartTransition(colorTransitions.highlight, colorTransitions.highlightMultiplier);
		}
		void Button::Exit() {
			StartTransition(colorTransitions.standard, 1.0f);
		}
		void Button::StartTransition(Color color, float highlight) {
			_targetColor = color;
			_targetHighlight = highlight;
			_startTransitionTime = _timeSinceLastUpdate;
			_image->renderData.TryGetFloat("colorMultiplier", _originalHighlight);
			glm::vec4 originalColorVec;
			_image->renderData.TryGetVector4("_color", originalColorVec);
			_originalColor = originalColorVec;
			_inTransition = true;
		}
	}
}