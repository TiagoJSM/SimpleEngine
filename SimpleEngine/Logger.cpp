#include "Logger.h"

namespace SimpleEngine {
    void Logger::Log(const std::string& text) {
        std::cout << text << std::endl;
    }

	std::ostream& Logger::LogStream() {
		return std::cout;
	}
}