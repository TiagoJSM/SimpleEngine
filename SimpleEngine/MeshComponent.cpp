#include "MeshComponent.h"
#include "GameObject.h"

namespace SimpleEngine {
	void MeshComponent::Render(IRenderer& renderer) {
		renderer.Render(mesh, material, gameObject->TransformMatrix(), &renderData);
	}

	Box3 MeshComponent::GetBoundingBox() {
		auto box = mesh->GetBoundingBox();
		auto mat = gameObject->TransformMatrix();
		box.bounds[0] = mat * glm::vec4(box.bounds[0], 1);
		box.bounds[1] = mat * glm::vec4(box.bounds[1], 1);
		return box;
	}
}