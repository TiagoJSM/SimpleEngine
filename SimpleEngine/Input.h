#pragma once

#include <glm\glm.hpp>

namespace SimpleEngine {
	struct Input {
		static bool w;
		static bool s;
		static bool o;
		static bool l;

		static glm::ivec2 mousePointer;
		static bool leftMouse;
		static bool rightMouse;
	};
}