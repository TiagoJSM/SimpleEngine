#pragma once

namespace SimpleEngine {
	template<class Single>
	class Singleton {
	public:
		static Single* Instance() {
			if (_single == nullptr) {
				_single = new Single();
			}
			return _single;
		}
		static void Destroy() {
			delete _single;
			_single = nullptr;
		}
	private:
		static Single* _single;
	protected:
		Singleton() { }
	};

	template <class Single> Single* Singleton<Single>::_single = nullptr;
}