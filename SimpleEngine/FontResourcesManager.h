#pragma once

#include <Map>
#include <functional>
#include <tuple>
#include <memory>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "Singleton.h"
#include "Typedefs.h"
#include "Font.h"
#include "GraphicResourcesManager.h"
#include "MathUtils.h"
#include "ManagerHelpers.h"
#include "Screen.h"

namespace SimpleEngine {
	class FontResourcesManager : public Singleton<FontResourcesManager> {
	public:
		void Initialize();
		FontResource GetFont(std::string& path);
		void Destroy();
	private:
		std::function<void(FT_Face)> _fontFaceDelete;
		std::function<void(Font*)> _fontDelete;

		std::map<std::string, std::weak_ptr<FT_FaceRec_>> _fontFaces;
		std::map<std::string, std::weak_ptr<Font>> _fonts;
		FT_Library _library;   /* handle to library     */

		FT_Face LoadFontFace(std::string& path);
		Font* LoadFont(FontFaceResource fontFace);
		TextureResource LoadFont(FT_Bitmap bitmap);
		void Remove(FT_Face font);
		void Remove(Font* font);
	};
}