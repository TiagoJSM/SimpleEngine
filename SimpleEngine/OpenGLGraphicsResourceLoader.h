#pragma once

#include "IGraphicResourcesLoader.h"
#include "OpenGLMeshResource.h"
#include "Mesh.h"
#include "IShader.h"
#include "OpenGLTextureResource.h"

namespace SimpleEngine {
	class OpenGLGraphicsResourceLoader : public IGraphicResourcesLoader {
	public:
		OpenGLGraphicsResourceLoader(const string& common, const string& baseVertexShader, const string& baseFragmentShader);
		Mesh* GetMesh() override;
		ITextureResource* GetTextureResource(unsigned int width, unsigned int height) override;
		IShader* GetShader(const string& vertexShaderPath, const string& fragmentShaderPath) override;
		boost::any GetMaterialHandler() override;
	private:
		const string _commonShader;
		const string _baseVertexShader;
		const string _baseFragmentShader;
	};
}