#pragma once

#include "GameWorld.h"
#include "Input.h"
#include "Canvas.h"

namespace SimpleEngine {
	namespace UI {
		class UiHandler {
		public:
			static void Update(GameWorld& world);
		private:
			static bool _leftMouseDownLastUpdate;
			static bool _leftMouseUpLastUpdate;
			static GameObject _mouseLeftDownGo;
			static GameObject _mouseTargetGo;

			static void HandleUiEventsOnCanvas(GameObject go, glm::vec3 translation, Ray ray, std::function<void(GameObject)> action);
			static void HandleUiEventsOnGameObject(GameObject go);
			static void HandleMousePositionOnGameObject(GameObject go);
			static GameObject FindTargetGameObject(GameObject go, glm::vec3 translation, Ray ray);
		};
	}
}