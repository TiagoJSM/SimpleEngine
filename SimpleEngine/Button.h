#pragma once

#include "UiComponent.h"
#include "UiTransform.h"
#include "DefaultGraphicObjects.h"
#include "RenderData.h"
#include "FontResourcesManager.h"
#include "IMouseClick.h"
#include "Debug.h"
#include "Event.h"
#include "ColorTransitions.h"
#include "Image.h"

namespace SimpleEngine {
	namespace UI {
		class Button : public UiComponent, public IMouseClick, public IMouseEnter, public IMouseExit {
		public:
			Event<int> onClick;
			ColorTransitions colorTransitions;

			void Initialize() override;
			void Update(Timer timer) override;

			void Click() override;
			void Enter() override;
			void Exit() override;
		private:
			ComponentHandle<Image> _image;

			Color _originalColor;
			Color _targetColor;
			float _originalHighlight;
			float _targetHighlight;
			float _startTransitionTime;
			bool _inTransition;
			float _timeSinceLastUpdate;

			void StartTransition(Color color, float highlight);
		};
	}
}