#include "OpenGLLine.h"

namespace SimpleEngine {
	OpenGLLine::OpenGLLine() {}

	void OpenGLLine::Render(glm::vec3 start, glm::vec3 end) {
		LineSegmentData line { start, end };

		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(line), &line, GL_STATIC_DRAW);

		glBindVertexArray(_vao);
		glDrawArrays(GL_LINES, 0, 2);
	}

	OpenGLLine OpenGLLine::BuildGLLine() {
		const auto PositionAttributeIndex = 0;

		LineSegmentData line { glm::vec3(0, 0, 0), glm::vec3(0, 0, 0) };

		GLuint vao, vbo;
		glGenBuffers(1, &vbo);

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(line), &line, GL_STATIC_DRAW);
		glVertexAttribPointer(PositionAttributeIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(PositionAttributeIndex);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		return OpenGLLine(vao, vbo);
	}

	OpenGLLine::OpenGLLine(GLuint vao, GLuint vbo) {
		_vao = vao;
		_vbo = vbo;
	}
}
