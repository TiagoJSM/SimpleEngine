#pragma once

#include "../Box2D/Box2D.h"

namespace SimpleEngine {
    class Body2D;
    class Collider2D;

    class BodyContactListener : public b2ContactListener {
    public:
        void BeginContact(b2Contact* contact) override;
        void EndContact(b2Contact* contact) override;
    private:
        bool GetComponents(b2Contact* contact, Body2D** outBody, Collider2D** outCollider);
    };
}