#pragma once

#include "UiComponent.h"
#include "UiTransform.h"
#include "DefaultGraphicObjects.h"
#include "RenderData.h"

namespace SimpleEngine {
	namespace UI {
		class Image : public UiComponent {
		public:
			RenderData renderData;

			void Initialize() override;
			Box3 GetBoundingBox() override;
			void SetTexture(TextureResource texture);
		protected:
			void Render(glm::mat4 viewProjection, IRenderer& renderer) override;
		};
	}
}