#include "OpenGLMaterial.h"

namespace SimpleEngine {

	OpenGLMaterial::OpenGLMaterial(boost::any materialHandler) :Material(materialHandler) { }

	void OpenGLMaterial::SetMatrix(const std::string& name, glm::mat4 matrix) {
		SetParameter<glm::mat4>(name, matrix, _matrices);
		auto program = any_cast<GLuint>(GetHandler());
		glUseProgram(program);
		auto mvpId = glGetUniformLocation(program, name.c_str());
		glUniformMatrix4fv(mvpId, 1, GL_FALSE, &matrix[0][0]);
	}

	void OpenGLMaterial::SetTextures(const map<std::string, TextureResource>& textures) {
		_textures = textures;
		auto program = any_cast<GLuint>(GetHandler());
		glUseProgram(program);
		auto idx = 0;
		for (auto tex : textures) {
			auto textureName = tex.first;
			auto texLoc = glGetUniformLocation(program, (textureName + ".Sampler").c_str());
			glUniform1i(texLoc, idx);
			SetVec2(textureName + ".Offset", tex.second->GetOffset());

			idx++;

			if (idx == TEXTURE_POSITIONS) {
				break;
			}
		}
	}

	void OpenGLMaterial::SetVec3(const std::string& name, glm::vec3 vec) {
		SetParameter<glm::vec3>(name, vec, _vec3s);
		auto program = any_cast<GLuint>(GetHandler());
		glUseProgram(program);
		auto id = glGetUniformLocation(program, name.c_str());
		glUniform3f(id, vec.x, vec.y, vec.z);
	}

	void OpenGLMaterial::SetVec2(const std::string& name, glm::vec2 vec) {
		SetParameter<glm::vec2>(name, vec, _vec2s);
		auto program = any_cast<GLuint>(GetHandler());
		glUseProgram(program);
		auto id = glGetUniformLocation(program, name.c_str());
		glUniform2f(id, vec.x, vec.y);
	}

	void OpenGLMaterial::SetInt(const std::string& name, int value) {
		SetParameter<int>(name, value, _integers);
		auto program = any_cast<GLuint>(GetHandler());
		glUseProgram(program);
		auto id = glGetUniformLocation(program, name.c_str());
		glUniform1i(id, value);
	}

	OpenGLMaterial::~OpenGLMaterial() {
		auto program = any_cast<GLuint>(GetHandler());
		auto shader = GetShader();
		glUseProgram(0);
		glDetachShader(program, shader->GetVertexShader());
		glDetachShader(program, shader->GetFragmentShader());
		glDeleteProgram(program);
	}

	void OpenGLMaterial::OnSetShader(ShaderResource oldShader, ShaderResource newShader) {
		auto program = any_cast<GLuint>(GetHandler());
		auto shader = GetShader(); 
		
		if (oldShader) {
			glDetachShader(program, oldShader->GetVertexShader());
			glDetachShader(program, oldShader->GetFragmentShader());
		}

		glAttachShader(program, shader->GetVertexShader());
		glAttachShader(program, shader->GetFragmentShader());

		// Link. At this point, our shaders will be inspected/optized and the binary code generated
		// The binary code will then be uploaded to the GPU
		glLinkProgram(program);

		// Verify that the linking succeeded
		int isLinked;
		glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);

		if (isLinked == false)
			PrintShaderLinkingError(program);
	}

	void OpenGLMaterial::PrintShaderLinkingError(GLuint shaderProgram)
	{
		Logger::Log("=======================================\n");
		Logger::Log("Shader linking failed : ");

		// Find length of shader info log
		int maxLength;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);

		Logger::LogStream() << "Info Length : " << maxLength << std::endl;

		// Get shader info log
		char* shaderProgramInfoLog = new char[maxLength];
		glGetProgramInfoLog(shaderProgram, maxLength, &maxLength, shaderProgramInfoLog);

		Logger::LogStream() << "Linker error message : " << shaderProgramInfoLog << std::endl;

		// Handle the error in an appropriate way such as displaying a message or writing to a log file.
		// In this simple program, we'll just leave
		delete shaderProgramInfoLog;
		return;
	}
}