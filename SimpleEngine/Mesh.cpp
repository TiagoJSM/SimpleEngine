#include "Mesh.h"
#include "IRenderer.h"

namespace SimpleEngine {
	Mesh::Mesh(IMeshResource* meshResource) : _meshResource(meshResource){}

	void Mesh::ApplyDataToMesh() {
		_meshResource->BuildGraphicHandlers(
			_vertices,
			_colors,
			_uvs,
			_indices);
	}

	void Mesh::SetVertices(vector<glm::vec3>& vertices) {
		_vertices = vertices;
		_sphere = MathUtils::CalculateSphere(vertices);
		_box = MathUtils::CalculateBox(vertices);
	}

	const vector<glm::vec3>& Mesh::GetVertices() {
		return _vertices;
	}

	void Mesh::SetColors(vector<Color>& colors) {
		_colors = colors;
	}

	const vector<Color>& Mesh::GetColors() {
		return _colors;
	}

	void Mesh::SetUVs(vector<glm::vec2>& uvs) {
		_uvs = uvs;
	}

	const vector<glm::vec2>& Mesh::GetUVs() {
		return _uvs;
	}

	void Mesh::SetIndices(vector<unsigned int>& indices) {
		_indices = indices;
	}

	const vector<unsigned int>& Mesh::GetIndices() {
		return _indices;
	}

	Sphere Mesh::GetBoundingSphere() const {
		return _sphere;
	}

	Box3 Mesh::GetBoundingBox() const {
		return _box;
	}

	IMeshResource* Mesh::GetMeshResource() const {
		return _meshResource;
	}

	Mesh::~Mesh() {
		delete _meshResource;
	}
}