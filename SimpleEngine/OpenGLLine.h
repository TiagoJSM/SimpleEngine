#pragma once

#include <GL/glew.h>
#include <glm\glm.hpp>

namespace SimpleEngine {
	struct LineSegmentData
	{
		glm::vec3 start, end;
	};

	class OpenGLLine {
	public:
		OpenGLLine();
		void Render(glm::vec3 start, glm::vec3 end);
		static OpenGLLine BuildGLLine();
	private:
		OpenGLLine(GLuint vao, GLuint vbo);
		GLuint _vao, _vbo;
	};
}