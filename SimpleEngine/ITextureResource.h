#pragma once

#include <vector>
#include <glm\glm.hpp>

#include "Color.h"

using namespace std;

namespace SimpleEngine {
	enum class WrappingMode {
		Repeat,
		Clamp
	};

	class ITextureResource {
	public:
		virtual unsigned int GetWidth() = 0;
		virtual unsigned int GetHeight() = 0;
		virtual unsigned int GetHandler() = 0;
		virtual glm::vec2 GetOffset() = 0;
		virtual void SetOffset(glm::vec2 offset) = 0;

		virtual WrappingMode GetWrappingMode() = 0;
		virtual void SetWrappingMode(WrappingMode mode) = 0;

		virtual void GenerateGraphicHandlers() = 0;

        virtual vector<Color> GetPixels() = 0;
		virtual void SetPixels(const vector<Color>& colors) = 0;

		virtual ~ITextureResource() = 0;
	};
}