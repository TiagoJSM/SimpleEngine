#pragma once

#include <glm\glm.hpp>
#include "GraphicResourcesManager.h"

namespace SimpleEngine {
    class Screen {
    public:
        static glm::ivec2 Size();
    };
}