#pragma once

namespace SimpleEngine {
	struct Timer {
		float time;
		float deltaTime;
	};
}