#include "OpenGLShader.h"

namespace SimpleEngine {
	const unsigned int OpenGLShader::texturePositions[TEXTURE_POSITIONS] = {
		GL_TEXTURE0,
		GL_TEXTURE1,
		GL_TEXTURE2,
		GL_TEXTURE3,
		GL_TEXTURE4,
		GL_TEXTURE5,
		GL_TEXTURE6,
		GL_TEXTURE7
	};
}