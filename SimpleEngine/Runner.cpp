#include "Runner.h"

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <soil.h>

using namespace SimpleEngine;

Runner::Runner(IRenderer& renderer, IGraphicResourcesLoader& graphicsLoader, IScene& startingScene):
	Running(true),
	_renderer(renderer),
	_graphicsLoader(graphicsLoader),
	_startingScene(startingScene),
	_world(GameWorld(_componentManager)) { }

bool Runner::OnInit() {
	auto init = _renderer.OnInit(&_graphicsLoader);

	GraphicResourcesManager::Instance()->Initialize(&_graphicsLoader, &_renderer);
	Physics2DManager::Instance()->Initialize(1 / 60.f, 8, 3, b2Vec2(0, -0.05f));
	FontResourcesManager::Instance()->Initialize();
	
	Debug::Initialize(&_renderer);
	DefaultGraphicObjects::Initialize();

	_timer.time = ((float)SDL_GetTicks()) / 1000;

	_startingScene.BuildScene(_world);

	return init;
}

void Runner::OnEvent(SDL_Event* Event) {
	if (Event->type == SDL_QUIT) {
		Running = false;
	}

	auto keyDown = Event->type == SDL_KEYDOWN;
	Input::w = Event->key.keysym.sym == SDLK_w && keyDown;
	Input::s = Event->key.keysym.sym == SDLK_s && keyDown;
	Input::o = Event->key.keysym.sym == SDLK_o && keyDown;
	Input::l = Event->key.keysym.sym == SDLK_l && keyDown;

	auto buttonDown = Event->type == SDL_MOUSEBUTTONDOWN;
	Input::leftMouse = Event->button.button == SDL_BUTTON_LEFT && buttonDown;
	Input::rightMouse = Event->button.button == SDL_BUTTON_RIGHT && buttonDown;
}

void Runner::OnLoop() {
	auto ticks = ((float)SDL_GetTicks()) / 1000;
	_timer.deltaTime = ticks - _timer.time;
	_timer.time = ticks;

	UiHandler::Update(_world);
	SimpleEngine::Physics2DManager::Instance()->Update();

	//Logger::LogStream() << "Before Update: " << ((float)SDL_GetTicks()) / 1000 << std::endl;
	LoopGameWorld([&](SimpleEngine::GameObject go) { go->Update(_timer); });
	LoopGameWorld([&](SimpleEngine::GameObject go) { go->PhysicsUpdate(_timer); });
	//Logger::LogStream() << "After Update: " << ((float)SDL_GetTicks()) / 1000 << std::endl;
}


void Runner::OnRender() {
	auto viewProj = Camera::currentCamera->ProjectionMatrix() * Camera::currentCamera->ViewMatrix();
	_renderer.PreRender(viewProj);

	//auto topLevelGOs = _world.TopLevelGameObjects();
	auto frustum = _renderer.GetFrustum(viewProj);
	//Logger::LogStream() << "Before Render: " << ((float)SDL_GetTicks()) / 1000 << std::endl;

	for (auto go : _world) {
		if (!go->GetParent()) {
			TickGameObject(
				go,
				[&](SimpleEngine::GameObject go) {
				auto mesh = go->GetComponent<MeshComponent>();
				if (mesh) {
					mesh->Render(_renderer);
				}
			},
				[&](SimpleEngine::GameObject go) {
				return frustum.BoxInFrustum(go->GetBoundingBox()) != Intersection::Outside;
			});
		}
	}

	//Logger::LogStream() << "After Render: " << ((float)SDL_GetTicks()) / 1000 << std::endl;

	for (auto go : _world) {
		if (!go->GetParent()) {
			TickGameObject(
				go,
				[&](SimpleEngine::GameObject go) {
				auto uiComps = go->GetComponents<UI::UiComponent>();
				for (auto uiComp : uiComps) {
					uiComp->Render(_renderer);
				}
			});
		}
	}

	_renderer.Submit();
	Debug::Submit();

	_renderer.PostRender();
}

void Runner::OnCleanup() {
	_renderer.CleanUp();
    _world.CleanUp();
	DefaultGraphicObjects::CleanUp();
	Debug::Destroy();
	SimpleEngine::GraphicResourcesManager::Instance()->Destroy();
	SimpleEngine::FontResourcesManager::Instance()->Destroy();
}

int Runner::OnExecute() {
	if (OnInit() == false) {
		return -1;
	}

	SDL_Event Event;

	while (Running) {
		auto mouseButtons = SDL_GetMouseState(&Input::mousePointer.x, &Input::mousePointer.y);
		Input::mousePointer.y = SimpleEngine::Screen::Size().y - Input::mousePointer.y;

		while (SDL_PollEvent(&Event)) {
			OnEvent(&Event);
		}

		auto start = SDL_GetTicks();
		OnLoop();
		//Logger::LogStream() << "update: " << ((float)SDL_GetTicks() - start) << std::endl;
		start = SDL_GetTicks();
		OnRender();
		//Logger::LogStream() << "render: " << ((float)SDL_GetTicks() - start) << std::endl;
	}

	OnCleanup();

	return 0;
}

void Runner::LoopGameWorld(
	function<void(SimpleEngine::GameObject)> tickFunction,
	function<bool(SimpleEngine::GameObject)> validate) {
	for (auto go : _world) {
		TickGameObject(go, tickFunction);
	}
}
void Runner::TickGameObject(
	SimpleEngine::GameObject parent, 
	function<void(SimpleEngine::GameObject)> tickFunction,
	function<bool(SimpleEngine::GameObject)> validate) {
	if (!parent->IsEnabled()) {
		return;
	}
	if (validate != nullptr && !validate(parent)) {
		return;
	}
	tickFunction(parent);
	for (auto idx = 0; idx < parent->ChildCount(); idx++) {
		TickGameObject(parent->GetChildAt(idx), tickFunction, validate);
	}
	/*for (auto it = parent->begin(); it != parent->end(); ++it) {
		TickGameObject(*it, tickFunction, validate);
	}*/
}
