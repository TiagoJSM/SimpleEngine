#pragma once

#include "../Box2D/Box2D.h"
#include "Component.h"

namespace SimpleEngine {
    class Body2D;

    class Collider2D : public Component {
    public:
        Body2D* body;
        void Initialize() override;
        bool IsTrigger();
        void SetTrigger(bool trigger);
        ~Collider2D();
    protected:
        b2Fixture* _fixture;
    };
}