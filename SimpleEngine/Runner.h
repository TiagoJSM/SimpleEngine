#pragma once

#include <SDL.h>

// OpenGL / glew Headers
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

#include <vector>
#include <functional>

#include "SimpleEngine\Ui.h"
#include "Renderer.h"
#include "SimpleEngine.h"
#include "IScene.h"
#include "DefaultGraphicObjects.h"
#include "Frustum.h"
#include "ComponentManager.h"

using namespace std;

namespace SimpleEngine {
	using namespace UI;
	class Runner {
	private:
		bool Running;
		IScene& _startingScene;
		ComponentManager _componentManager;
		GameWorld _world;
		Timer _timer;

		IRenderer& _renderer;
		IGraphicResourcesLoader& _graphicsLoader;

		void LoopGameWorld(
			function<void(SimpleEngine::GameObject)> tickFunction,
			function<bool(SimpleEngine::GameObject)> validate = nullptr);
		void TickGameObject(
			GameObject parent, 
			function<void(SimpleEngine::GameObject)> tickFunction,
			function<bool(SimpleEngine::GameObject)> validate = nullptr);

	public:
		Runner(SimpleEngine::IRenderer& renderer, IGraphicResourcesLoader& graphicsLoader, IScene& startingScene);

		int OnExecute();
		bool OnInit();
		void OnEvent(SDL_Event* Event);
		void OnLoop();
		void OnRender();
		void OnCleanup();
	};
}