#include "Screen.h"

namespace SimpleEngine {
    glm::ivec2 Screen::Size() {
        return SimpleEngine::GraphicResourcesManager::Instance()->GetRenderer()->GetWindowSize();
    }
}