#pragma once

#include <GL/glew.h>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include "IShader.h"

namespace SimpleEngine {
	#define TEXTURE_POSITIONS 8
	
	class OpenGLShader : public IShader
	{
	public:
		bool CreateShader(
			const string& common,
			const string& baseVertexShaderPath, const string& baseFragmentShaderPath,
			const string& vertexShaderPath, const string& fragmentShaderPath)
		{
			// Generate our shader. This is similar to glGenBuffers() and glGenVertexArray(), except that this returns the ID

			if (!LoadVertexShader(common, baseVertexShaderPath, vertexShaderPath))
				return false;

			if (!LoadFragmentShader(common, baseFragmentShaderPath, fragmentShaderPath))
				return false;

			// All shaders has been create, now we must put them together into one large object
			return true;
		}

		unsigned int GetVertexShader() override {
			return vertexshader;
		}
		unsigned int GetFragmentShader() override {
			return fragmentShader;
		}

		void CleanUp()
		{
			/* Cleanup all the things we bound and allocated */
			glDeleteShader(vertexshader);
			glDeleteShader(fragmentShader);
		}

		~OpenGLShader() override {
			CleanUp();
		}
	private:
		// The handle to our shader program
		//GLuint shaderProgram;

		// The handles to the induvidual shader
		GLuint vertexshader, fragmentShader;

		const static unsigned int texturePositions[TEXTURE_POSITIONS];

		std::string ReadFile(const char* file)
		{
			// Open file
			std::ifstream t(file);

			// Read file into buffer
			std::stringstream buffer;
			buffer << t.rdbuf();

			// Make a std::string and fill it with the contents of buffer
			std::string fileContent = buffer.str();

			return fileContent;
		}

		bool LoadVertexShader(const string& common, const string& baseVertexShaderPath, const std::string &shaderPath)
		{
			std::cout << "Linking Vertex shader" << std::endl;

			// Read file as std::string 
			auto commonShader = ReadFile(common.c_str());
			auto commonShaderSrc = const_cast<char*>(commonShader.c_str());

			auto baseShader = ReadFile(baseVertexShaderPath.c_str());
			auto baseShaderSrc = const_cast<char*>(baseShader.c_str());

			// Read file as std::string 
			std::string shader = ReadFile(shaderPath.c_str());
			auto shaderSrc = const_cast<char*>(shader.c_str());

			// Create an empty vertex shader handle
			vertexshader = glCreateShader(GL_VERTEX_SHADER);

			char* shaderData[]{ baseShaderSrc, commonShaderSrc, shaderSrc };

			// Send the vertex shader source code to OpenGL
			glShaderSource(vertexshader, 3, shaderData, NULL);

			// Compile the vertex shader
			glCompileShader(vertexshader);

			int wasCompiled = 0;
			glGetShaderiv(vertexshader, GL_COMPILE_STATUS, &wasCompiled);

			if (wasCompiled == 0)
			{
				PrintShaderCompilationErrorInfo(vertexshader);
				return false;
			}

			return true;
		}

		bool LoadFragmentShader(const string& common, const string& baseFragmentShaderPath, const std::string &shaderPath)
		{
			std::cout << "Loading Fragment Shader" << std::endl;

			auto baseShader = ReadFile(baseFragmentShaderPath.c_str());
			auto baseShaderSrc = const_cast<char*>(baseShader.c_str());

			// Read file as std::string 
			auto commonShader = ReadFile(common.c_str());
			auto commonShaderSrc = const_cast<char*>(commonShader.c_str());

			auto str = ReadFile(shaderPath.c_str());
			auto src = const_cast<char*>(str.c_str());

			// Create an empty vertex shader handle
			fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

			char* shaderData[]{ baseShaderSrc, commonShaderSrc, src };

			// Send the vertex shader source code to OpenGL
			glShaderSource(fragmentShader, 3, shaderData, NULL);

			// Compile the vertex shader
			glCompileShader(fragmentShader);

			int wasCompiled = 0;
			glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &wasCompiled);

			if (wasCompiled == false)
			{
				PrintShaderCompilationErrorInfo(fragmentShader);
				return false;
			}

			return true;
		}

		// If something went wrong whil compiling the shaders, we'll use this function to find the error
		void PrintShaderCompilationErrorInfo(int32_t shaderId)
		{
			std::cout << "=======================================\n";
			std::cout << "Shader compilation failed : " << std::endl;

			// Find length of shader info log
			int maxLength;
			glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLength);

			// Get shader info log
			char* shaderInfoLog = new char[maxLength];
			glGetShaderInfoLog(shaderId, maxLength, &maxLength, shaderInfoLog);

			// Print shader info log
			std::cout << "\tError info : " << shaderInfoLog << std::endl;

			std::cout << "=======================================\n\n";
			delete shaderInfoLog;
		}
	};
}