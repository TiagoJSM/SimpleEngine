#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <boost\any.hpp>

#include "Color.h"

using namespace std;

namespace SimpleEngine {
	class IMeshResource {
	public:
		virtual void* GetHandler() = 0;
		virtual unsigned int IndicesCount() = 0;
		//virtual void Render() = 0;
		virtual void BuildGraphicHandlers(
			const vector<glm::vec3>& vertices, 
			const vector<Color>& colors,
			const vector<glm::vec2>& uvs,
			const vector<unsigned int>& indices) = 0;

		virtual ~IMeshResource() = 0;
	};
}