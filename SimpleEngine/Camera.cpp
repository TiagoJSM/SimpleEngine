#include "Camera.h"

namespace SimpleEngine {
	Camera* Camera::currentCamera;

	Camera::Camera() 
		:	scale(glm::vec3(1)),
			fieldOfView(45), aspectRatio(4.0f / 3.0f), nearClipping(0.1f), farClipping(100.0f),
			left(-1.f), right(1.f), bottom(-1.f), top(1.f),
			view(View::Perspective) {}

	glm::mat4 Camera::ViewMatrix() const {
		return MathUtils::CreateTransformationMatrix(-translation, rotation, scale);
	}

	glm::mat4 Camera::ProjectionMatrix() const {
		return view == View::Perspective
			? glm::perspective(glm::radians(fieldOfView), aspectRatio, nearClipping, farClipping)
			: glm::ortho(left, right, bottom, top, nearClipping, farClipping);
	}

	glm::vec2 Camera::WorldToScreen(glm::vec3 position) const {
        auto screenSize = Screen::Size();
		auto clipSpacePos = ProjectionMatrix() * (ViewMatrix() * glm::vec4(position, 1.0));
		auto ndcSpacePos = 
			glm::vec3(
				clipSpacePos.x / clipSpacePos.w, 
				clipSpacePos.y / clipSpacePos.w, 
				clipSpacePos.z / clipSpacePos.w);

		auto windowSpacePos =
			((glm::vec2(ndcSpacePos.x + 1.0f, ndcSpacePos.y + 1.0f) / 2.0f) * 
                glm::vec2(screenSize.x, screenSize.y));

		return glm::vec2(windowSpacePos.x, windowSpacePos.y);
	}

    glm::vec3 Camera::ScreenToWorld(glm::vec2 screenPosition, float z) const {
		return ScreenToWorld(screenPosition, z, ProjectionMatrix(), ViewMatrix());
    }

	glm::vec3 Camera::ScreenToWorld(glm::vec2 screenPosition, float z, glm::mat4 viewMatrix, glm::mat4 projectionMatrix) {
		auto screenSize = Screen::Size();
		auto x = 2.0 * screenPosition.x / screenSize.x - 1;
		auto y = 2.0 * screenPosition.y / screenSize.y - 1;

		auto viewProjectionInverse = glm::inverse(projectionMatrix * viewMatrix);

		auto point = glm::vec4(x, y, z, 1.0f);
		return viewProjectionInverse * point;
	}

	Ray Camera::ScreenPointToRay(glm::vec2 position) const {
		return ScreenPointToRay(position, translation, ViewMatrix(), ProjectionMatrix());
	}

	Ray Camera::ScreenPointToRay(glm::vec2 position, glm::vec3 translation, glm::mat4 viewMatrix, glm::mat4 projectionMatrix) {
		auto screenSize = Screen::Size();
		auto viewport = glm::vec4(0, 0, screenSize.x, screenSize.y);
		glm::vec3 v0 = glm::unProject(glm::vec3(float(position.x), float(position.y), 0.0f), viewMatrix, projectionMatrix, viewport);
		glm::vec3 v1 = glm::unProject(glm::vec3(float(position.x), float(position.y), 100.0f), viewMatrix, projectionMatrix, viewport);
		glm::vec3 dir = glm::normalize(v1 - v0);
		dir.z *= -1;	//ToDo: check if this is opengl mirroring, if yes move out of here
		return{ translation, dir };
	}
}