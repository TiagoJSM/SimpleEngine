#include "OpenGLMeshResource.h"

namespace SimpleEngine {
	OpenGLMeshResource::OpenGLMeshResource() { }

	void* OpenGLMeshResource::GetHandler() {
		return &_vao;
	}

	unsigned int OpenGLMeshResource::IndicesCount() {
		return _indicesCount;
	}

	void OpenGLMeshResource::BuildGraphicHandlers(
		const vector<glm::vec3>& vertices, 
		const vector<Color>& colors,
		const vector<glm::vec2>& uvs,
		const vector<unsigned int>& indices) {
		glGenBuffers(VboCount, _vbo);
		// Generate and assign a Vertex Array Object to our handle
		glGenVertexArrays(1, &_vao);
		// Bind our Vertex Array Object as the current used object
		glBindVertexArray(_vao);

		// Positions
		// ===================
		// Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[PositionAttributeIndex]);
		// Copy the vertex data to our buffer
		glBufferData(GL_ARRAY_BUFFER, (vertices.size() * sizeof(glm::vec3)), vertices.data(), GL_STATIC_DRAW);
		// Specify that our coordinate data is going into attribute index 0, and contains three floats per vertex
		glVertexAttribPointer(PositionAttributeIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);
		// Enable our attribute within the current VAO
		glEnableVertexAttribArray(PositionAttributeIndex);

		// Colors
		// =======================
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[ColorAttributeIndex]);
		// Copy the color data to our buffer
		glBufferData(GL_ARRAY_BUFFER, (colors.size() * sizeof(Color)), colors.data(), GL_STATIC_DRAW);
		// Specify that our color data is going into attribute index 1, and contains four floats per vertex
		glVertexAttribPointer(ColorAttributeIndex, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(ColorAttributeIndex);

		// UVs
		// =======================
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[UvAttributeIndex]);
		// Copy the uv data to our buffer
		glBufferData(GL_ARRAY_BUFFER, (uvs.size() * sizeof(glm::vec2)), uvs.data(), GL_STATIC_DRAW);
		// Specify that our uv data is going into attribute index 2, and contains two floats per vertex
		glVertexAttribPointer(UvAttributeIndex, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(UvAttributeIndex);

		// Indices
		// =======================
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[IndicesAttributeIndex]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

		_indicesCount = indices.size();

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	OpenGLMeshResource::~OpenGLMeshResource() {
		glDeleteVertexArrays(1, &_vao);
		glDeleteBuffers(VboCount, _vbo);
	}
}