#pragma once

#include "Component.h"
#include "Canvas.h"
#include "Rect.h"
#include "Screen.h"
#include "ComponentHandle.h"

namespace SimpleEngine{
	namespace UI {
		enum class VerticalAlignment {
			Top,
			Middle,
			Bottom,
			Stretch
		};
		enum class HorizontalAlignment {
			Left,
			Middle,
			Right,
			Stretch
		};
		struct Anchors {
			glm::vec2 min;
			glm::vec2 max;

			static const Anchors LeftTop;
			static const Anchors LeftMiddle;
			static const Anchors LeftBottom;
			static const Anchors LeftStretch;

			static const Anchors MiddleTop;
			static const Anchors MiddleMiddle;
			static const Anchors MiddleBottom;
			static const Anchors MiddleStretch;

			static const Anchors RightTop;
			static const Anchors RightMiddle;
			static const Anchors RightBottom;
			static const Anchors RightStretch;

			static const Anchors StretchTop;
			static const Anchors StretchMiddle;
			static const Anchors StretchBottom;
			static const Anchors StretchStretch;
		};

		class UiTransform : public Component {
		public:
			float width = 0, height = 0;
			glm::vec2 pivot = glm::vec2();
			Anchors anchors;

			UiTransform();
			Rect GetRect() const;
			ComponentHandle<Canvas> GetCanvas() const;
		private:
			void GetVerticalRectDimensions(ComponentHandle<UiTransform> canvasTransform, TransformComponent& transform, float* ry, float* height) const;
			void GetHorizontalRectDimensions(ComponentHandle<UiTransform> canvasTransform, TransformComponent& transform, float* rx, float* width) const;
		};
	}
}