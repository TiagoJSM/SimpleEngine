#pragma once

#include "IRenderer.h"
#include "Timer.h"
#include "BaseIncludes.h"
#include "Input.h"
#include "Box3.h"
#include "GameObject.h"
#include "Handle.h"

namespace SimpleEngine {
	class Component {
	public:
		GameObject gameObject;
		bool enabled = true;

        virtual void OnEnabled() {}
        virtual void OnDisabled() {}
		virtual void Initialize() {}
		virtual void Update(Timer timer) {}
		virtual void PhysicsUpdate(Timer timer) {}
		virtual Box3 GetBoundingBox() { return Box3(); }
		virtual ~Component() {}
	};
}