#pragma once

#include <string>
#include <iostream>

namespace SimpleEngine {
    class Logger {
    public:
        static void Log(const std::string& text);
		static std::ostream& LogStream();
    };
}