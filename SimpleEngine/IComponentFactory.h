#pragma once

#include "BaseIncludes.h"
#include "ComponentHandle.h"

using namespace std;

namespace SimpleEngine {
	class IComponentFactory {
	public:
		virtual ComponentHandle<Component> CreateComponent() = 0;
	};
}