#include "Color.h"
#include "MathUtils.h"

namespace SimpleEngine {
	const Color Color::White = Color(1.0f, 1.0f, 1.0f, 1.0f);

	Color::Color() : r(0), g(0), b(0), a(1) {}

	Color::Color(float r, float g, float b, float a)
		: r(r), g(g), b(b), a(a) {}
	Color::Color(glm::vec4 color) : Color(color.r, color.g, color.b, color.a) {}
	Color::Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
		: r((float)r / UCHAR_MAX), g((float)g / UCHAR_MAX), b((float)b / UCHAR_MAX), a((float)a / UCHAR_MAX) {}

	Color::operator glm::vec4() const {
		return glm::vec4(r, g, b, a);
	}

	Color Color::Lerp(Color a, Color b, float t) {
		return Color(
			MathUtils::Lerp(a.r, b.r, t),
			MathUtils::Lerp(a.g, b.g, t),
			MathUtils::Lerp(a.b, b.b, t),
			MathUtils::Lerp(a.a, b.a, t)
		);
	}
}