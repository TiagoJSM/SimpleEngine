#pragma once

#include <glm/glm.hpp>

namespace SimpleEngine {
	struct Rect final {
		glm::vec2 p1;

		float width;
		float height;

		Rect();
		Rect(float x, float y, float width, float height);

		glm::vec2 P2() const;
		glm::vec2 Center() const;
		bool Contains(const Rect other) const;
		Rect GetTranslated(const glm::vec2 translation) const;
	};
}