#include "BoxCollider2D.h"
#include "SimpleEngine.h"

namespace SimpleEngine {
    void BoxCollider2D::Initialize() {
        b2PolygonShape groundBox;
        groundBox.SetAsBox(1.0f, 1.0f);
        _fixture = body->GetPhysicsEngineBody()->CreateFixture(&groundBox, 1.0f);
        Collider2D::Initialize();
    }

    void BoxCollider2D::SetSize(float x, float y) {
        auto shape = static_cast<b2PolygonShape*>(_fixture->GetShape());
        shape->SetAsBox(x / 2, y / 2);
    }
}