#pragma once

namespace SimpleEngine {
	namespace UI {
		class IMouseClick {
		public:
			virtual void Click() = 0;
		};
		class IMouseEnter {
		public:
			virtual void Enter() = 0;
		};
		class IMouseExit {
		public:
			virtual void Exit() = 0;
		};
	}
}