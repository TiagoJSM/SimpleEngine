#include "BodyContactListener.h"
#include "Body2D.h"
#include "Collider2D.h"

namespace SimpleEngine {
    void BodyContactListener::BeginContact(b2Contact* contact) {
        Body2D* body;
        Collider2D* collider;
        if (GetComponents(contact, &body, &collider)) {
            body->RaiseOnColliderEnter(collider);
        }
    }

    void BodyContactListener::EndContact(b2Contact* contact) {
        Body2D* body;
        Collider2D* collider;
        if (GetComponents(contact, &body, &collider)) {
            body->RaiseOnColliderExit(collider);
        }
    }

    bool BodyContactListener::GetComponents(b2Contact* contact, Body2D** outBody, Collider2D** outCollider) {
        auto bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
        if (bodyUserData == nullptr) {
            return false;
        }
        auto fixtureUserData = contact->GetFixtureA()->GetUserData();
        if (fixtureUserData == nullptr) {
            return false;
        }
        auto body = static_cast<Body2D*>(bodyUserData);
        auto collider = static_cast<Collider2D*>(fixtureUserData);
        *outBody = body;
        *outCollider = collider;
        return true;
    }
}