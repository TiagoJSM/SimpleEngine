#include "DefaultGraphicObjects.h"

#include <string>

#include "GraphicResourcesManager.h"

using namespace std;

namespace SimpleEngine {
	ShaderResource DefaultGraphicObjects::DefaultShader;
	ShaderResource DefaultGraphicObjects::DefaultUiShader;
	MaterialResource DefaultGraphicObjects::UiDefaultMaterial;
	TextureResource DefaultGraphicObjects::DefaultTexture;

	void DefaultGraphicObjects::Initialize() {
		string defaultVertex = "Standard.vert", defaultFragment = "Standard.frag";
		auto defaultUiVertex = "UiDefault.vert"s, defaultUiFragment = "UiDefault.frag"s;
		auto gManager = GraphicResourcesManager::Instance();
		DefaultGraphicObjects::DefaultShader = gManager->GetShader(defaultVertex, defaultFragment);
		DefaultGraphicObjects::DefaultUiShader = gManager->GetShader(defaultUiVertex, defaultUiFragment);
		string defaultMat = "_UiDefaultMaterial_";
		DefaultGraphicObjects::UiDefaultMaterial = gManager->GetMaterial(defaultMat);
		DefaultGraphicObjects::UiDefaultMaterial->SetShader(DefaultGraphicObjects::DefaultUiShader);
		auto defaultTexture = "./Textures/DefaultTexture.bmp"s;
		DefaultGraphicObjects::DefaultTexture = gManager->LoadTextureResource(defaultTexture);
	}

	void DefaultGraphicObjects::CleanUp() {
		DefaultGraphicObjects::UiDefaultMaterial = nullptr;
		DefaultGraphicObjects::DefaultShader = nullptr;
		DefaultGraphicObjects::DefaultUiShader = nullptr;
		DefaultGraphicObjects::DefaultTexture = nullptr;
	}
}