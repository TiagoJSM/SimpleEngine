#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>

#include "IRenderer.h"
#include "GraphicResourcesManager.h"
#include "Box3.h"
#include "MathUtils.h"
#include "Screen.h"

namespace SimpleEngine {
	class Debug {
	public:
		static void Initialize(SimpleEngine::IRenderer* renderer);

		static void DrawLine(glm::vec3 start, glm::vec3 end);
		static void DrawBox3(Box3 box);
		static void Submit();
		static void Destroy();
	private:
		static IRenderer* _renderer;

		static std::vector<std::function<void()>> _renderCalls;
	};
}