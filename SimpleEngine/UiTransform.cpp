#include "UiTransform.h"

namespace SimpleEngine {
	namespace UI {
		const Anchors Anchors::LeftTop = { glm::vec2(0.f, 1.f), glm::vec2(0.f, 1.f) };
		const Anchors Anchors::LeftMiddle = { glm::vec2(0.f, 0.5f), glm::vec2(0.f, 0.5f) };
		const Anchors Anchors::LeftBottom = { glm::vec2(0.f, 0.f), glm::vec2(0.f, 0.f) };
		const Anchors Anchors::LeftStretch = { glm::vec2(0.f, 0.f), glm::vec2(0.f, 1.f) };
					  
		const Anchors Anchors::MiddleTop = { glm::vec2(0.5f, 1.f), glm::vec2(0.5f, 1.f) };
		const Anchors Anchors::MiddleMiddle = { glm::vec2(0.5f, 0.5f), glm::vec2(0.5f, 0.5f) };
		const Anchors Anchors::MiddleBottom = { glm::vec2(0.5f, 0.f), glm::vec2(0.5f, 0.f) };
		const Anchors Anchors::MiddleStretch = { glm::vec2(5.f, 0.f), glm::vec2(5.f, 1.f) };
					  
		const Anchors Anchors::RightTop = { glm::vec2(1.f, 1.f), glm::vec2(1.f, 1.f) };
		const Anchors Anchors::RightMiddle = { glm::vec2(1.f, 0.5f), glm::vec2(1.f, 0.5f) };
		const Anchors Anchors::RightBottom = { glm::vec2(1.f, 0.f), glm::vec2(1.f, 0.f) };
		const Anchors Anchors::RightStretch = { glm::vec2(1.f, 0.f), glm::vec2(1.f, 1.f) };
					  
		const Anchors Anchors::StretchTop = { glm::vec2(0.f, 1.f), glm::vec2(1.f, 1.f) };
		const Anchors Anchors::StretchMiddle = { glm::vec2(0.f, 0.5f), glm::vec2(1.f, 0.5f) };
		const Anchors Anchors::StretchBottom = { glm::vec2(0.f, 0.f), glm::vec2(1.f, 0.f) };
		const Anchors Anchors::StretchStretch = { glm::vec2(0.f, 0.f), glm::vec2(1.f, 1.f) };

		UiTransform::UiTransform() {
			anchors = Anchors::LeftTop;
		}

		Rect UiTransform::GetRect() const {
			auto canvasTransform = gameObject->GetComponentInParent<Canvas>()->GetUiTransform();
			auto& transform = gameObject->transform;

			float rx, ry, width, height;

			if (canvasTransform == this) {
				auto size = Screen::Size();
				auto position = gameObject->transform.translation;
				rx = position.x;
				ry = position.y;
				width = size.x;
				height = size.y;
			}
			else {
				auto parentUiTransform = gameObject->GetComponentInParent<UiTransform>(false);
				GetVerticalRectDimensions(parentUiTransform, transform, &ry, &height);
				GetHorizontalRectDimensions(parentUiTransform, transform, &rx, &width);
			}
			
			auto rect = Rect(rx, ry, width, height);
			auto translation = glm::vec2(rect.width * pivot.x, rect.height * pivot.y);
			return rect.GetTranslated(-translation);
		}

		ComponentHandle<Canvas> UiTransform::GetCanvas() const {
			return gameObject->GetComponentInParent<Canvas>();
		}

		void UiTransform::GetVerticalRectDimensions(ComponentHandle<UiTransform> parentUiTransform, TransformComponent& transform, float* ry, float* height) const {
			auto parentRect = parentUiTransform->GetRect();
			auto parentP1 = parentRect.p1;

			auto anchorMinY = parentP1.y + parentRect.height * anchors.min.y;
			auto anchorMaxY = parentP1.y + parentRect.height * anchors.max.y;

			*ry = anchorMinY + transform.translation.y;
			*height = this->height;

			if (anchors.min.y != anchors.max.y) {
				*height = anchorMaxY - anchorMinY + this->height;
			}
		}

		void UiTransform::GetHorizontalRectDimensions(ComponentHandle<UiTransform> parentUiTransform, TransformComponent& transform, float* rx, float* width) const {
			auto parentRect = parentUiTransform->GetRect();
			auto parentP1 = parentRect.p1;

			auto anchorMinX = parentP1.x + parentRect.width * anchors.min.x;
			auto anchorMaxX = parentP1.x + parentRect.width * anchors.max.x;

			*rx = anchorMinX + transform.translation.x;
			*width = this->width;

			if (anchors.min.x != anchors.max.x) {
				*width = anchorMaxX - anchorMinX + this->width;
			}
		}
	}
}