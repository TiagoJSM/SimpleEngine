#include "ColorTransitions.h"

namespace SimpleEngine {
	namespace UI {
		ColorTransitions::ColorTransitions() {
			standard = Color(1.0f, 1.0f, 1.0f, 1.0f);
			highlight = Color(1.0f, 1.0f, 1.0f, 1.0f);
			pressed = Color(0.8f, 0.8f, 0.8f, 1.0f);
			highlightMultiplier = 1.4f;
			transitionTime = 0.5f;
		}
	}
}