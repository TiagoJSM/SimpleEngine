#include "RendererUtils.h"

namespace SimpleEngine {
	bool MaterialRenderDataComp(RenderCommand& i, RenderCommand& j) {
		auto iMat = i.material, jMat = j.material;
		auto iMesh = i.mesh->GetMeshResource()->GetHandler(), jMesh = j.mesh->GetMeshResource()->GetHandler();
		return (iMat->renderQueue < jMat->renderQueue) &&
			iMat->GetHandler().type().hash_code() < jMat->GetHandler().type().hash_code() &&
			iMesh < jMesh;
	}
}