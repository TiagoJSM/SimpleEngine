#pragma once

#include "UiComponent.h"
#include "UiTransform.h"
#include "DefaultGraphicObjects.h"
#include "RenderData.h"
#include "FontResourcesManager.h"
#include "Debug.h"

namespace SimpleEngine {
	namespace UI {
		class Text : public UiComponent {
		public:
			std::string text;
			FontResource font;

			void Initialize() override;
		protected:
			void Render(glm::mat4 viewProjection, IRenderer& renderer) override;
		};
	}
}