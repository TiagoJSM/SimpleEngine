#include "TransformComponent.h"

namespace SimpleEngine {
	TransformComponent::TransformComponent(): scale(glm::vec3(1.f, 1.f, 1.f)) { }

	glm::mat4 TransformComponent::TransformMatrix() const {
		return MathUtils::CreateTransformationMatrix(translation, rotation, scale);
	}
}