#pragma once

#include <Map>
#include <functional>
#include <tuple>
#include <vector>
#include <soil.h>
#include <memory>
#include <iostream>

#include "IGraphicResourcesLoader.h"
#include "Mesh.h"
#include "ITextureResource.h"
#include "IShader.h"
#include "Color.h"

#include "Singleton.h"
#include "Typedefs.h"
#include "ManagerHelpers.h"
#include "OpenGLMaterial.h"

namespace SimpleEngine {
	struct ShaderKey {
		string vertexShader;
		string fragmentShader;

		ShaderKey(string vertexShader, string fragmentShader) {
			this->vertexShader = vertexShader;
			this->fragmentShader = fragmentShader;
		}

		bool operator==(const ShaderKey& other) const {
			return std::tie(vertexShader, fragmentShader) == std::tie(other.vertexShader, other.fragmentShader);
		}
		bool operator<(const ShaderKey& other) const {
			return std::tie(vertexShader, fragmentShader) < std::tie(other.vertexShader, other.fragmentShader);
		}
	};

	class GraphicResourcesManager : public Singleton<GraphicResourcesManager> {
	public:
		void Initialize(IGraphicResourcesLoader* loader, IRenderer* renderer);

		MeshResource GetMesh();
        MeshResource GetMesh(string& path);
		TextureResource GetTextureResource(
			unsigned int width = 1, unsigned int height = 1);
        TextureResource GetTextureResource(
			string& name, unsigned int width = 1, unsigned int height = 1);
        TextureResource LoadTextureResource(
			string& path);
        ShaderResource GetShader(
			string& vertexShaderPath, string& fragmentShaderPath);
		MaterialResource GetMaterial(string& name);
        IRenderer* GetRenderer();

	private:
		IGraphicResourcesLoader* _loader;
        IRenderer* _renderer;

		function<void(Mesh*)> _meshDelete;
		function<void(ITextureResource*)> _textureDelete;
		function<void(IShader*)> _shaderDelete;
		function<void(Material*)> _materialDelete;

		map<string, weak_ptr<Mesh>> _meshes;
		map<string, weak_ptr<ITextureResource>> _textures;
		map<ShaderKey, weak_ptr<IShader>> _shaders;
		map<string, weak_ptr<Material>> _materials;

		template <typename TKey, typename TResource>
		void Remove(TResource* mesh, std::map<TKey, std::weak_ptr<TResource>>& container);
	};
}