#include "Image.h"

namespace SimpleEngine {
	namespace UI {
		void Image::Initialize() {
			auto defaultTex = DefaultGraphicObjects::DefaultTexture;
			renderData.textures.push_back({ "tex", defaultTex });
			renderData.vec4s.push_back({ "_color", Color::White });
			renderData.floats.push_back({ "colorMultiplier", 1.0f });
		}
		Box3 Image::GetBoundingBox() {
			auto rect = GetUiTransform()->GetRect();
			return Box3(glm::vec3(rect.p1, -0.1f), glm::vec3(rect.P2(), 0.1f));
		}
		void Image::Render(glm::mat4 viewProjection, IRenderer& renderer) {
			renderer.DrawQuad(viewProjection, GetUiTransform()->GetRect(), DefaultGraphicObjects::UiDefaultMaterial, &renderData);
		}

		void Image::SetTexture(TextureResource texture) {
			renderData.SetTexture("tex", texture);
		}
	}
}