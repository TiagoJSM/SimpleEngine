#include "FontResourcesManager.h"

namespace SimpleEngine {
	void FontResourcesManager::Initialize() {
		_fontFaceDelete = [this](FT_Face font) { Remove(font); };
		_fontDelete = [this](Font* font) { Remove(font); };

		auto error = FT_Init_FreeType(&_library);
		if (error) {
		}
	}

	FontResource FontResourcesManager::GetFont(std::string& path) {
		auto search = _fonts.find(path);
		if (search != _fonts.end()) {
			return search->second.lock();
		}
		auto face = 
			GetResource<FT_FaceRec_, std::string>(
				path, _fontFaces, [&, this]() { return LoadFontFace(path); }, _fontFaceDelete);

		return
			GetResource<Font, std::string>(
				path, _fonts, [&, this]() { return LoadFont(face); }, _fontDelete);
	}

	void FontResourcesManager::Destroy() {
		FT_Done_FreeType(_library);
		Singleton<FontResourcesManager>::Destroy();
	}

	FT_Face FontResourcesManager::LoadFontFace(std::string& path) {
		FT_Face face;
		auto error = FT_New_Face(_library,
			path.c_str(),
			0,
			&face);
		if (error == FT_Err_Unknown_File_Format)
		{

		}
		else if (error)
		{

		}

		auto s = Screen::Size();
		error = FT_Set_Char_Size(
			face,    /* handle to face object           */
			0,       /* char_width in 1/64th of points  */
			12 * 64,   /* char_height in 1/64th of points */
			s.x,     /* horizontal device resolution    */
			s.y);   /* vertical device resolution      */

		return face;
	}

	Font* FontResourcesManager::LoadFont(FontFaceResource fontFace) {
		FT_Error error;
		vector<FontCharacterInfo> textures;
		for (auto start = 'a', end = 'z'; start <= end; start++) {
			error = FT_Load_Char(fontFace.get(), start, FT_LOAD_RENDER);
			auto slot = fontFace->glyph;
			
			FontCharacterInfo info = {
				LoadFont(slot->bitmap),
				slot->bitmap_left,
				slot->bitmap_top,
				slot->advance
			};
			textures.push_back(info);
		}
		
		return new Font(fontFace, textures);
	}

	TextureResource FontResourcesManager::LoadFont(FT_Bitmap bitmap) {
		auto width = (unsigned int)MathUtils::NextPowerOf2(bitmap.width);
		auto height = (unsigned int)MathUtils::NextPowerOf2(bitmap.rows);

		auto texture = GraphicResourcesManager::Instance()->GetTextureResource(width, height);
		std::vector<Color> colors;
		for (auto j = 0u; j < height; j++) {
			for (auto i = 0u; i < width; i++) {
				unsigned char val = (i >= bitmap.width || j >= bitmap.rows) ? 0 : bitmap.buffer[i + bitmap.width*j];
				colors.push_back(Color(val, val, val, val));
			}
		}
		texture->SetPixels(colors);
		texture->SetOffset(glm::vec2(-(1.0f - (float)bitmap.width / width), -(1.0f - (float)bitmap.rows / height)));
		return texture;
	}

	void FontResourcesManager::Remove(FT_Face font) {
		for (auto iter = _fontFaces.begin(), endIter = _fontFaces.end(); iter != endIter; ) {
			if (iter->second.expired()) {
				_fontFaces.erase(iter++);
			}
			else {
				++iter;
			}
		}
		FT_Done_Face(font);
	}

	void FontResourcesManager::Remove(Font* font) {
		for (auto iter = _fonts.begin(), endIter = _fonts.end(); iter != endIter; ) {
			if (iter->second.expired()) {
				_fonts.erase(iter++);
			}
			else {
				++iter;
			}
		}
		delete font;
	}
}