#include "Collider2D.h"
#include "SimpleEngine.h"

namespace SimpleEngine {
    void Collider2D::Initialize() {
        _fixture->SetUserData(this);
    }

    bool Collider2D::IsTrigger() {
        return _fixture->IsSensor();
    }

    void Collider2D::SetTrigger(bool trigger) {
        _fixture->SetSensor(trigger);
    }

    Collider2D::~Collider2D() {
        body->GetPhysicsEngineBody()->DestroyFixture(_fixture);
        _fixture = nullptr;
    }
}