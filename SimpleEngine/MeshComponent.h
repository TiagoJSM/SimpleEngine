#pragma once

#include "Component.h"
#include "Mesh.h"
#include "IShader.h"

namespace SimpleEngine {
	class MeshComponent : public Component {
	public:
		MeshResource mesh;
		MaterialResource material;
		RenderData renderData;

		void Render(IRenderer& renderer);
		Box3 GetBoundingBox() override;
	};
}