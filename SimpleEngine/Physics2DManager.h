#pragma once

#include "../Box2D/Box2D.h"
#include "Singleton.h"
#include "BodyContactListener.h"
#include <glm\glm.hpp>

namespace SimpleEngine {
	class Physics2DManager : public Singleton<Physics2DManager> {
	public:
		Physics2DManager();
		void Initialize(
			float step, 
			int velocityIterations = 8, 
			int positionIterations = 3, 
			b2Vec2 gravity = Physics2DManager::DefaultGravity);
		void Update();

		b2Body* CreateBody(glm::vec2 position);
	private:
		static b2Vec2 DefaultGravity;

		b2World _world;
		float _step;
		int _velocityIterations;
		int _positionIterations;
        BodyContactListener _contactListener;
	};
}